
from script import Script, ScriptException
import pandas

import os


class DataScript(Script):

    default_settings = {
        "report": False,
        "report.table": 'measurements',
        "report.dir": None,
        "report.path": None,
        "report.label": None,
        "report.sqlite": False,
    }

    def __init__(self, label=None, **kwargs):
        super(DataScript, self).__init__(**kwargs)
        self.setLabel(label)
        table = self.getReportTable()
        self._measurements = {table: pandas.DataFrame()}

    def getReportTable(self):
        return self.getSetting("report.table")

    def setReportTable(self, value):
        if not isinstance(value, str) or value == "":
            raise ValueError("Invalid Table Name")
        self.setSetting('report.table', value)

    def getMeasurements(self, table=None):
        if table is None:
            table = self.getReportTable()
        return self._measurements.get(table, pandas.DataFrame())

    def hasMeasurements(self, table=None):
        return len(self.getMeasurements(table=table)) > 0

    def writesReport(self):
        return self.getSetting("report") is True

    def enableReporting(self, state):
        self.setSetting("report", state is True)

    def writesSQLite(self):
        return self.getSetting("report.sqlite") is True

    def enableSQLite(self, state):
        self.setSetting("report.sqlite", state is True)

    def getDataDir(self):
        result = self.getSetting("report.dir")
        if result is None:
            result = os.path.join(self.getRootDir(), "data")
        return result

    def getLabel(self):
        return self.getSetting("report.label")

    def setLabel(self, label):
        self.setSetting("report.label", label)

    def getReportPath(self, generate=True):
        result = self.getSetting("report.path")
        if result is None and generate:
            result = self.generateReportPath()
        return result

    def setReportPath(self, fp=None):
        """Set the path to report to.

        - Disables reporting when set to False.
        - Writes log to default location when set to None.

        :param path: Location to write to log to
        :type path: bool | None | str
        """
        # check if path exists
        if isinstance(fp, str):
            if fp == "":
                fp = None
            else:
                fd = os.path.dirname(fp)
                if not os.path.exists(fd):
                    fp = None
        else:
            fp = None
        self.setSetting("report.path", fp)

    def generateReportPath(
            self, directory=None, date=None, version=None, name=None, label=None, ext=None, overwrite=False
    ):
        if directory is None:
            directory = self.getDataDir()
        if ext is None:
            ext = "csv"
        if label is None:
            label = self.getLabel()
        return self.generatePath(
            directory=directory, date=date, version=version, name=name, label=label, ext=ext, overwrite=overwrite
        )

    def run(self):
        result = super(DataScript, self).run()
        if self.writesReport() and self.hasMeasurements():
            self.report()
        return result

    def _prepare(self):
        result = super(DataScript, self)._prepare()
        self.clear()
        return result

    def execute(self):
        return self.collect()

    def collect(self):
        return False    # overload

    def clear(self, table=None):
        if table is None:
            table = self.getReportTable()
        self._measurements[table] = pandas.DataFrame()

    def record(self, measurements, table=None):
        result = False
        if table is None:
            table = self.getReportTable()
        # if not a data frame then convert into data frame
        if not isinstance(measurements, pandas.DataFrame):
            measurements = pandas.DataFrame(measurements)
        # record them
        if isinstance(measurements, pandas.DataFrame):
            current_measurements = self.getMeasurements(table=table)
            # store new data set
            self._measurements[table] = current_measurements.append(measurements)
            result = True
        return result

    def record_annotated(self, values, table=None, **kwargs):
        self.record(
            self.annotate(values, **kwargs), table=table
        )

    def report(self, results=None, path=None, label=None, table=None):
        result = False
        if results is None:
            results = self.getMeasurements(table=table)
        if label is None:
            label = self.getLabel()
        if path is None:
            path = self.getReportPath(generate=False)
        if self.writesSQLite():
            if isinstance(path, str) and os.path.isdir(path):
                path = self.generateReportPath(directory=path, label=label, ext="db", overwrite=True)
            if path is None:
                path = self.generateReportPath(label=label, ext="db", overwrite=True)
            result = self.report_sqlite(path, results, table=table)
        else:
            if table is not None:
                label = "{}_{}".format(label ,table)
            if isinstance(path, str) and os.path.isdir(path):
                path = self.generateReportPath(directory=path, label=label)
            if path is None:
                path = self.generateReportPath(label=label)
            result = self.report_csv(path, results)
        self.getLog().info("Write report to {}: {}".format(path, result))
        return result

    def report_csv(self, fp, results):
        if isinstance(results, pandas.DataFrame):
            results.to_csv(fp, index=False)
        else:
            with open(fp, "wb") as dest:
                # get header list
                headers = results[0].keys()
                import csv
                writer = csv.DictWriter(dest, headers)
                writer.writeheader()
                writer.writerows(results)
        return True

    def report_sqlite(self, fp, results, table=None):
        result = False
        import sqlite3
        con = None
        if table is None:
            table = self.getReportTable()
        try:
            con = sqlite3.connect(fp)
        except sqlite3.OperationalError:
            self.getLog().error("Unable to connect to {}".format(fp))
        if con is not None:
            if isinstance(results, pandas.DataFrame):
                results.to_sql(table, con, if_exists='append', index=False)
                con.close()
                result = True
            else:
                self.getLog().error("Can only write DataFrame objects to SQLite")
        return result

    #
    #   Argument processing
    #

    def parse_arguments(self, arguments):
        """Process the arguments from argparse

        :param arguments: Arguments from argparse
        :type arguments: dict[str, str | float | int]
        """
        super(DataScript, self).parse_arguments(arguments)
        report = arguments.get("report")
        if report is None or isinstance(report, str):
            self.enableReporting(True)
            self.setReportPath(report)
        use_sqlite = arguments.get("use_sqlite")
        if isinstance(use_sqlite, bool):
            self.enableSQLite(use_sqlite)
        label = arguments.get("label")
        if isinstance(label, str) and label != "":
            self.setLabel(label)

    def define_arguments(self, parser=None):
        """Adds arguments to the parser

        :param parser: argparse.ArgumentParser
        :return: The new parser
        :rtype: argparse.ArgumentParser
        """
        parser = super(DataScript, self).define_arguments(parser)
        parser.add_argument(
            '-r', '--report', dest="report", nargs="?", const=None, default=False,
            help="Report the measurements in a CSV File"
        )
        parser.add_argument(
            '--sqlite', dest="use_sqlite", action="store_true",
            help="Whether to use SQLite instead of CSV to report data"
        )
        parser.add_argument(
            '--label', dest="label",
            help="An extra label to put in the name when reporting"
        )
        return parser

    def default_arguments(self):
        defaults = super(DataScript, self).default_arguments()
        defaults["label"] = None
        return defaults


class DataScriptException(ScriptException):

    def __init__(self, msg):
        super(DataScriptException, self).__init__(msg=msg)
