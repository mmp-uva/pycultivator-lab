"""
The Script Module provides a basic front-end script facilities.

Providing you with a standardized class that can be called from a terminal program.

Upon running the script via the run() method, it will prepare itself, execute the script and clean-up.

Example usages:
 - Prepare: connect to device
 - execute: measure OD
 - cleanup: disconnect from device

The script class comes with features:
 - Write messages to standard output.
 - Write a pause message to standard output (updated every second).
 - ABSTRACT load() method for loading configuration objects from a configuration source.

"""

from pycultivator.core import ConfigurableObject, pcException, pcLogger
from extras import logger
from datetime import datetime as dt
import os, logging, argparse

__all__ = [
    "Script", "DeviceScript",
]


class Script(
    ConfigurableObject,
    logger.ConsoleLoggerMixin
):
    """ABSTRACT Basic Script"""

    _name = "script"
    namespace = "script"
    default_settings = {
        "ini.file": None,
        "root.dir": None,
        "log.file.state": False,
        "log.file.dir": None,
    }

    @staticmethod
    def annotate(values, parameters=None, **kwargs):
        """Annotates a list of values by storing each value in a dictionary.

        :param values: List of values to annotate
        :type values: list[float | int]
        :param parameters: Annotation to add to each value
        :type parameters: dict[str, str | int | float]
        :param kwargs: extra keyword arguments to add to annotation
        :type kwargs: str | int | float
        :return: List of annotated values
        :rtype: list[dict[str, int | str| float]]
        """
        results = []
        if parameters is None:
            parameters = {}
        parameters.update(kwargs)
        if values is None:
            values = []
        if not isinstance(values, (tuple, list)):
            values = [values]
        for idx, value in enumerate(values):
            parameters["replicate"] = idx
            parameters["value"] = value
            results.append(parameters.copy())
        return results

    @staticmethod
    def average(values):
        """Calculates the average value"""
        result = 0.0
        if len(values) > 0:
            result = sum(values) / float(len(values))
        return result

    @classmethod
    def median(cls, values):
        """Calculates the median (middle value)"""
        result = 0.0
        if len(values) > 0:
            # from stack overflow: http://stackoverflow.com/a/20730918/6336991
            even = (0 if len(values) % 2 else 1) + 1
            half = (len(values) - 1) / 2
            result = sum(sorted(values)[half:half + even]) / float(even)
        return result

    @classmethod
    def getName(cls):
        return cls._name

    @classmethod
    def generatePath(cls, directory=None, date=None, version=None, name=None, label=None, ext=None, overwrite=False):
        """Generates a path to a file, adding a version indicator if the file already exists"""
        if directory is None:
            directory = os.getcwd()
        if date is None:
            date = dt.now()
        if name is None:
            name = cls.getName()
        if ext is None:
            ext = ""
        if isinstance(ext, str):
            ext.lstrip(".")
        result = os.path.join(
            directory, "{}{}_{}{}{}".format(
                date.strftime("%Y%m%d") if isinstance(date, dt) else date,
                version if version is not None else "",
                name,
                "_{}".format(label) if label is not None else "",
                ".{}".format(ext) if ext is not None else ""
            )
        )
        if os.path.exists(result) and not overwrite:
            version = cls._addVersion(version)
            cls.getLog().log(1, "{} exists, try adding {} behind date.".format(result, version))
            result = cls.generatePath(
                directory=directory, date=date, version=version, name=name, ext=ext, label=label
            )
        return result

    @staticmethod
    def _addVersion(version=None):
        if version in (None, ""):
            version = "a"
        else:
            last = version[-1]
            if ord(last) <= ord("z"):
                last = ord(last) + 1
            else:
                last = "{}a".format(last)
            if isinstance(last, int):
                last = chr(last)
            version = "{}{}".format(version[0:-1], last)
        return version

    def __init__(self, cwd=None, **kwargs):
        """Initialises the script"""
        super(Script, self).__init__(**kwargs)
        if cwd is None:
            cwd = __file__
        if os.path.isfile(cwd):
            cwd = os.path.dirname(cwd)
        self._cwd = cwd

    def getWorkingDir(self):
        return self._cwd

    @property
    def cwd(self):
        return self.getWorkingDir()

    def setWorkingDir(self, path):
        if not os.path.exists(path) or not os.path.isdir(path):
            raise ScriptException("Received invalid Path")
        self._cwd = path

    @cwd.setter
    def cwd(self, path):
        self.setWorkingDir(path)

    def getRootDir(self):
        result = self.getSetting("root.dir")
        if result is None:
            result = self.getWorkingDir()
        return result

    @property
    def is_quiet(self):
        return self.isQuiet()

    @is_quiet.setter
    def is_quiet(self, state):
        self.setQuiet(state)

    def isQuiet(self):
        return self._quiet is True

    def setQuiet(self, state):
        self._quiet = state is True

    def writesLogFile(self):
        return self.getSetting("log.file.state") is True

    def getLogFileDir(self):
        result = self.getSetting("log.file.dir")
        if result is None:
            result = os.path.join(self.getRootDir(), "log")
        return result

    def getLogLevel(self):
        return self.getRootLog().level

    @property
    def log_level(self):
        return self.getLogLevel()

    def setLogLevel(self, level=0):
        """Sets the verbose detail level of the logging, level is in 0 - 6

        :param level: Level of verbosity (0-6):
            1 = CRITICAL, 2 = ERROR, 3 = WARNING, 4 = INFO, 5 = DEBUG, 6 = EVERYTHING
        :type level: int
        """
        if level > 0:
            level = 60 - (level * 10)
            self.getRootLog().setLevel(level)

    @log_level.setter
    def log_level(self, level):
        self.setLogLevel(level)

    def generateLogPath(self, directory=None, date=None, version=None, name=None, ext=None, overwrite=False):
        if directory is None:
            directory = self.getLogFileDir()
        if ext is None:
            ext = "log"
        return self.generatePath(
            directory=directory, date=date, version=version, name=name, ext=ext, overwrite=True
        )

    def addStreamHandler(self):
        """Connects a stream logging handler to the root logger.
        """
        self.addLogHandler(pcLogger.UVAStreamHandler())

    def addFileHandler(self, log_path=None):
        if log_path is None:
            log_path = self.generateLogPath()
        if not os.path.exists(os.path.dirname(log_path)):
            raise ScriptException("Invalid log path: {}".format(log_path))
        handler = pcLogger.UVARotatingFileHandler(filename=log_path)
        self.addLogHandler(handler)

    def addLogHandler(self, handler):
        """Adds a logging handler to the root logger."""
        if not isinstance(handler, logging.Handler):
            raise ScriptException("Received invalid handler: {}".format(type(handler)))
        self.getRootLog().addHandler(handler)
        return self

    @classmethod
    def bootstrap(cls, cwd=None, **kwargs):
        script = cls(cwd=cwd, **kwargs)
        # define argument parser
        p = script.define_arguments()
        # load defaults
        a = script.default_arguments()
        # load arguments and update defaults
        a.update(vars(p.parse_args()))
        # parse arguments
        script.parse_arguments(a)
        # run script
        return script.run()

    def run(self):
        """Starting point for the script. After loading, use this to start"""
        result = self._prepare()
        try:
            result = result and self.execute()
        except KeyboardInterrupt:
            self.warn("\nUser interrupt caught, quit gracefully")
            result = False
        except Exception as e:
            self.log.error("Unexpected exception occurred", exc_info=1)
            result = False
        finally:
            self._clean()
        return result

    def _prepare(self):
        """Prepares the script. Should return True when successful.

        :rtype: bool
        """
        self.getLog().info("Prepare script for execution")
        return True

    def execute(self):
        """Does the actual work of the script. Should return True when successful.

        :rtype: bool
        """
        raise NotImplementedError

    def _clean(self):
        """Cleans the environment of the script. Should return True when successful.

        :rtype: bool
        """
        self.getLog().info("Clean-up after execution")
        return True

    @staticmethod
    def process_argument(name, arguments, default=None):
        """Read the argument from the given dictionary, if present"""
        result = default
        if name in arguments.keys():
            result = arguments[name]
        return result

    def parse_arguments(self, arguments):
        """Process the arguments from argparse

        :param arguments: Arguments from argparse
        :type arguments: dict[str, str | float | int]
        """
        # load ini
        ini_file = arguments.get("ini_file")
        settings = self.load_ini(ini_file)
        self.settings.update(settings)
        verbose = self.process_argument("verbose", arguments, 0)
        if verbose > 0:
            self.setLogLevel(verbose)
            self.addStreamHandler()
        log_file = self.process_argument("log_file", arguments, False)
        if log_file is None or isinstance(log_file, str):
            self.addFileHandler(log_file)
        is_quiet = arguments.get("is_quiet")
        if is_quiet is not None:
            self.setQuiet(is_quiet)

    def load_ini(self, ini_file):
        results = {}
        from pycultivator_lab.scripts.extras import ini_config
        if isinstance(ini_file, str) and not os.path.exists(ini_file):
            ini_file = None
        # did we find anything?
        if ini_file is None:
            ini_files = ini_config.INIConfig.find_ini()
            if len(ini_files) > 0:
                ini_file = ini_files[0]
        if ini_file is not None:
            self.inform("Load ini settings from: {}".format(ini_file))
            results = ini_config.INIConfig().load_ini(ini_file)
        return results

    def define_arguments(self, parser=None):
        """Adds arguments to the parser

        :param parser: argparse.ArgumentParser
        :return: The new parser
        :rtype: argparse.ArgumentParser
        """
        if parser is None:
            parser = argparse.ArgumentParser(description="A simple script")
        parser.add_argument(
            '-v', '--verbose', action="count",
            help="Whether to print verbose messages and at what level."
            "Add multiple flags to increase detail"
        )
        parser.add_argument(
            '-q', '--quiet', action="store_true", dest="is_quiet",
            help="Do not ask the user any questions (Assumes yes on all questions)"
        )
        parser.add_argument(
            '-l', '--log-file', dest="log_file", nargs="?", const=None, default=False,
            help="If set (with or without path) log messages are also written to a file"
        )
        parser.add_argument(
            '--ini', dest="ini_file", help="Location of pycultivator.ini with settings"
        )
        return parser

    def default_arguments(self):
        return {"log_file": False}


class DeviceScript(Script):
    """A script with device registration"""

    DEVICE_TYPES = set([])

    default_settings = {
        "config.dir": None
    }

    def __init__(self, **kwargs):
        super(DeviceScript, self).__init__(**kwargs)
        self._auto_load = True          # whether to load the plate upon argument processing
        self._config_locations = {}     # store config location using name as ID
        self._configs = {}              # store config using name as ID
        self._devices = {}              # store device using name as ID

    def doAutoLoad(self):
        return self._auto_load is True

    def setAutoLoad(self, state):
        self._auto_load = state is True

    def getConfigDir(self):
        result = self.getSetting("config.dir")
        if result is None:
            result = self.getRootDir()
        return result

    def getConfigLocations(self):
        return self._config_locations

    def getConfigLocation(self, name):
        if not self.hasConfigLocation(name):
            raise KeyError("No configuration path under the name {}".format(name))
        return self.getConfigLocations()[name]

    def hasConfigLocation(self, name):
        return name in self.getConfigLocations().keys()

    def setConfigLocation(self, name, location):
        self._config_locations[name] = location

    def getConfigs(self):
        """Returns all the configuration objects registered to this script

        :rtype: dict[str, pycultivator.config.xmlConfig.XMLConfig]
        """
        return self._configs

    def getConfig(self, name):
        """ Returns the configuration object registered under the given name

        :param name: Name of the configuration
        :rtype: None or pycultivator.config.xmlConfig.XMLConfig
        """
        if not self.hasConfigName(name):
            raise KeyError("No configuration under the name {}".format(name))
        return self.getConfigs()[name]

    def hasConfigName(self, name):
        """Returns whether this name is registered in the configurations dictionary

        :rtype: bool
        """
        return name in self.getConfigs().keys()

    def hasConfig(self, name):
        """Returns whether a configuration object is registered under this name

        Checks both existence of the name and whether the registered name is not None

        :rtype: bool
        """
        return self.hasConfigName(name) and self.getConfig(name) is not None

    def setConfig(self, name, c):
        from pycultivator.config import baseConfig
        if c is not None and not isinstance(c, baseConfig.Config):
            raise ValueError("Invalid configuration file: {}".format(type(c)))
        self._configs[name] = c

    def getDevices(self):
        """ Return all device objects registered to this script

        :rtype: dict[str, pycultivator.device.device.Device]
        """
        return self._devices

    def getDevice(self, name):
        """ Return the device object registered under the given name

        :param name: Name of the device
        :type name: str
        :rtype: None or pycultivator.device.device.Device
        """
        if not self.hasDeviceName(name):
            raise KeyError("Unknown device type")
        return self.getDevices()[name]

    def hasDeviceName(self, name):
        return name in self.getDevices().keys()

    def hasDevice(self, name):
        return self.hasDeviceName(name) and self.getDevice(name) is not None

    def setDevice(self, name, d):
        from pycultivator.device import BaseDevice
        if d is not None and not isinstance(d, BaseDevice):
            raise ValueError("Invalid device object: {}".format(type(d)))
        self._devices[name] = d

    # behaviour

    def findConfig(self, config_path):
        result = None
        if os.path.exists(config_path):
            result = config_path
        else:
            # add config dir
            config_path = os.path.join(self.getConfigDir(), config_path)
            if os.path.exists(config_path):
                result = config_path
        return result

    def loadConfig(self, name, config_path, settings=None, **kwargs):
        """Load an configuration object from a configuration path"""
        from pycultivator.config import xmlConfig
        config = xmlConfig.XMLConfig.load(config_path, settings=settings, **kwargs)
        if config is not None:
            self.setConfig(name, config)
        return self.hasConfig(name)

    def loadDevice(self, name, config=None, config_path=None, settings=None, **kwargs):
        """ Loads an device from a configuration path

        :param name:
        :param config_path:
        :param settings:
        :param kwargs:
        :return:
        """
        device = None
        if config is None and self.hasConfig(name):
            config = self.getConfig(name)
        if config is not None:
            xdc = config.getHelperFor("device")
            device = xdc.load()
        if device is not None:
            self.setDevice(name, device)
        return self.hasDevice(name)

    def execute(self):
        raise NotImplementedError


class ScriptException(pcException.PCException):
    """An Exception thrown by a pycultivator_plate Script"""

    def __init__(self, msg):
        super(ScriptException, self).__init__(msg)
