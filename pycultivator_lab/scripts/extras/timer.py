"""Mix-in class to provide a timing and time estimating capabilities"""

from datetime import datetime as dt
import logger
from pycultivator.core import pcException

__author__ = "Joeri Jongbloets"


class TimerMixin(logger.ConsoleLoggerMixin):
    """ The timer script mixin will help you to inform the user about
    how long the script will take and how much longer to wait.


    """

    TIME_UNITS = ["sec.", "min.", "hrs.", "days", "weeks", "months", "years"]

    TIME_UNIT_SIZES = {
        "sec.": 60,
        "min.": 60,
        "hrs.": 24,
        "days": 7,
        "weeks": 4,
        "months": 12,
        "years": 1,
    }

    def __init__(self):
        self.__t_start = None
        self.__t_stop = None

    def _to_seconds(self, t, unit="sec."):
        """Changes a value from the current unit into seconds"""
        if unit not in self.TIME_UNITS:
            raise TimerMixinException("Unknown time unit: {}".format(unit))
        idx = self.TIME_UNITS.index(unit) - 1
        result = t
        # as long we are not to seconds: convert
        while idx >= 0:
            new_unit = self.TIME_UNITS[idx]
            result *= float(self.TIME_UNIT_SIZES[new_unit])
            idx -= 1
        return result

    def _time_unit(self, t, unit="sec."):
        """Changes the time from seconds to largest unit for which the value >= 1"""
        # first turn to standard unit
        t = self._to_seconds(t)
        for unit in self.TIME_UNITS:
            unit_size = self.TIME_UNIT_SIZES[unit]
            if t > unit_size:
                if unit_size > 0:
                    t /= float(unit_size)
            else:
                break
        return t, unit

    def _inform_time(self, actual_msg, actual, estimate=None):
        estimate_msg = ""
        if estimate is not None:
            error_t, error_unit = self._time_unit(actual - estimate)
            estimate_msg = " [{:+6.2f} {}]".format(
                error_t, error_unit
            )
        actual_t, unit = self._time_unit(actual)
        self.inform(
            "## {:>25}: {:5.2f} {}{}".format(
                actual_msg, actual_t, unit, estimate_msg
            )
        )

    def get_estimate(self):
        raise NotImplementedError

    def get_time_start(self):
        return self.__t_start

    def get_time_stop(self):
        return self.__t_stop

    def get_time_spent(self):
        t_start = self.get_time_start()
        if t_start is None:
            raise TimerMixinException("Start timer before getting time spent")
        t_stop = self.get_time_stop()
        if t_stop is None:
            t_stop = dt.now()
        return (t_stop - t_start).total_seconds()

    def inform_estimate(self):
        self._inform_time(
            "Estimated time", self.get_estimate()
        )

    def inform_actual(self):
        self._inform_time(
            "Actual time", self.get_time_spent(), self.get_estimate()
        )

    def reset_timer(self):
        self.__t_start = None
        self.__t_stop = None

    def start_timer(self):
        self.__t_start = dt.now()

    def stop_timer(self):
        self.__t_stop = dt.now()

    def time_this(f):
        """ Decorator to time a function

        :param f:
        :return:
        """
        def magic(self, *args, **kwargs):
            self.reset_timer()
            self.inform_estimate()
            self.start_timer()
            try:
                result = f(self, *args, **kwargs)
            except KeyboardInterrupt:
                raise
            finally:
                self.stop_timer()
                self.inform_actual()
            # pass result
            return result
        return magic

    time_this = staticmethod(time_this)


class LapTimerMixin(TimerMixin):
    """ Collects the time spent per lap, and updates it's estimate

    Provides the @time_lap decorator, to time a function as one lap.

    """

    def __init__(self):
        super(LapTimerMixin, self).__init__()
        self.__lap_time = 0.0
        self.__lap = 0
        self.__increment = 0

    def get_estimated_laps(self):
        raise NotImplementedError

    def get_initial_lap_estimate(self):
        raise NotImplementedError

    def get_lap_count(self):
        return self.__lap

    def get_laps_left(self):
        result = self.get_estimated_laps() - self.get_lap_count()
        # if below 0, show negative number
        if result < 0:
            result = -1
        return result

    def get_current_estimate(self, laps_left=None):
        """ The current estimated time for this script"""
        if laps_left is None:
            laps_left = self.get_laps_left()
        return self.get_lap_estimate() * float(laps_left)

    def get_lap_estimate(self):
        result = self.get_initial_lap_estimate()
        if self.get_lap_time() > 0:
            result = self.get_lap_time() / self.get_lap_count()
        return result

    def get_estimate(self):
        return self.get_initial_lap_estimate() * float(self.get_estimated_laps())

    def get_lap_time(self):
        return self.__lap_time

    def add_lap_time(self, t_lap):
        self.__lap_time += t_lap
        self.__lap += 1
        # add percentage increment: if above 1 show message
        self.__increment += (1.0 / self.get_estimated_laps()) * 100.0

    def reset_timer(self):
        super(LapTimerMixin, self).reset_timer()
        self.__lap_time = 0.0
        self.__lap = 0
        self.__increment = 0

    def time_lap(f):
        def magic(self, *args, **kwargs):
            t_start = dt.now()
            result = f(self, *args, **kwargs)
            t_spent = (dt.now() - t_start).total_seconds()
            old_estimate = self.get_current_estimate(self.get_laps_left() - 1)
            if old_estimate < 0:
                old_estimate = 0
            self.add_lap_time(t_spent)
            self.inform_update(self.get_current_estimate(), old_estimate)
            # pass result
            return result
        return magic

    time_lap = staticmethod(time_lap)

    def inform_lap(self, t_lap, t_estimated=None):
        if t_estimated is None:
            t_estimated = self.get_lap_estimate()
        self._inform_time(
            "Lap time", t_lap, t_estimated
        )

    def inform_update(self, estimate, old_estimate=None):
        count = self.get_lap_count()
        total = self.get_estimated_laps()
        percentage = 0.0
        if total > 0:
            percentage = (count / float(total) * 100.0)
        if self.__increment > 1:
            self._inform_time(
                "{:6.2f}% | Estimated time left".format(percentage),
                estimate, old_estimate
            )
            # reset increment
            self.__increment = 0


class TimerMixinException(pcException.PCException):

    def __init__(self, msg):
        super(TimerMixinException, self).__init__(msg=msg)
