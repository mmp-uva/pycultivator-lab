"""Loads ini configurations files into the script object"""

from mixin import Mixin
from pycultivator.core import pcUtility
from pycultivator.core.objects import PCObject
import sys, os

if sys.version_info[0] < 3:
    import ConfigParser as configparser
else:
    import configparser


class INIConfig(Mixin, PCObject):

    COMPATIBLE_VERSIONS = {
        # 2 = fully supported
        # 1 = deprecated
        # 0 = not supported
        "1.0": 2
    }

    @staticmethod
    def find_ini(directories=None, names=None):
        """Search for ini files

        :param directories: A list of directories to search into
            If None or empty list: Set directories to current work directory.
        :type directories: list[str] | str
        :param names: A list of names (without extension!) to search for
            If None or empty list: Set names to "pycultivator"
        :type names: list[str] | str
        :return: A list of matching files
        :rtype: list[str]
        """
        # parse directories
        if directories is None:
            directories = []
        if not isinstance(directories, (list, tuple, set)):
            directories = [directories]
        # cast to list if not already a list
        directories = list(directories)
        # load defaults
        if len(directories) == 0:
            directories = [os.getcwd()]
        # parse names
        if names is None:
            names = []
        if not isinstance(names, (list, tuple, set)):
            names = [names]
        names = list(names)
        if len(names) == 0:
            names = ["pycultivator"]
        return pcUtility.find_files(directories, name=names, extension="ini")

    @classmethod
    def load_ini(cls, location, section="pycultivator", namespace=None):
        results = {}
        if namespace is None:
            namespace = ""
        if cls.check_ini(location):
            try:
                cfg = configparser.SafeConfigParser()
                cfg.read(location)
                config_items = cfg.items(section)
                for item, value in config_items:
                    name = item
                    if namespace != "":
                        name = "{}.{}".format(namespace, name)
                    results[name] = cls.parse_ini_value(value)
            except configparser.Error as e:
                pass
        return results

    @classmethod
    def check_ini(cls, location, section="pycultivator"):
        result = os.path.exists(location)
        if result:
            try:
                cfg = configparser.SafeConfigParser()
                # read ini
                cfg.read(location)
                # check version
                ini_version = cls.get_ini_option(cfg, section, "version", default="0.0")
                version_level = -1
                if ini_version in cls.COMPATIBLE_VERSIONS.keys():
                    version_level = cls.COMPATIBLE_VERSIONS[ini_version]
                if version_level == 1:
                    cls.getLog().warning("INi File with version {} is deprecated, consider upgrading!".format(
                        ini_version
                    ))
                result = version_level > 0
            except configparser.Error:
                cls.getLog().warning("Unable to read version information from {}".format(
                    os.path.basename(location))
                )
                result = False
        return result

    @classmethod
    def get_ini_option(cls, cfg, section, option, default=None):
        result = default
        try:
            if isinstance(default, float):
                result = cfg.getfloat(section, option)
            elif isinstance(default, int):
                result = cfg.getint(section, option)
            elif isinstance(default, bool):
                result = cfg.getboolean(section, option)
            else:
                result = cfg.get(section, option)
        except configparser.NoSectionError:
            pass
        except configparser.NoOptionError:
            pass
        except configparser.Error:
            pass
        return result

    @classmethod
    def parse_ini_value(cls, value):
        result = value
        if value == "False":
            result = False
        if value == "True":
            result = True
        return result
