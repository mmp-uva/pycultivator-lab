"""Basic Placeholder for a general mixin object"""


__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class Mixin(object):
    """Basic Mixin object

     To be extended by other objects
     """

    pass
