""" A mixin module providing plotting capabilities to the scripts"""


import mixin
from pandas import DataFrame

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class PlotterMixin(mixin.Mixin):
    """Provides a basic plotter mixin"""

    def plot(self, x, y, data=None):
        """Creates and outputs a plot"""
        if isinstance(data, (list, dict)):
            data = DataFrame(data)
        if not isinstance(data, DataFrame):
            raise ValueError("Invalid data source")
