
from pycultivator_lab.scripts.extras import console


class TableScript(console.ConsoleTableMixin):

    def execute(self):
        self.writeln("Write a table from a list of dictionaries (keys as fields):")
        d = [
            {"Column 1": "a", "Column 2": "b"},
            {"Column 1": "e", "Column 2": "f"}
        ]
        self.write_table(d)
        self.writeln("Write a table from a list of dictionaries (custom a fields):")
        self.write_table(d, fields=["Column 1", "Column 3"])
        self.writeln("Write a table from a list of lists (without fields):")
        d = [
            ["a", "b"],
            ["e", "f"]
        ]
        self.write_table(d)
        self.writeln("Write a table from a list of lists (with fields):")
        self.write_table(d, ["Column 1", "Column 2"])


if __name__ == "__main__":

    ts = TableScript()
    ts.execute()
