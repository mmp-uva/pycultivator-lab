
from pycultivator_lab.scripts.extras import timer
from time import sleep
from numpy.random import normal as nrandom


class SlowScript(timer.TimerMixin):

    def get_estimate(self):
        return 5

    @timer.TimerMixin.time_this
    def execute(self):
        sleep(4)


class SlowRepeatingScript(timer.LapTimerMixin):

    def get_initial_lap_estimate(self):
        return 0.5

    def get_estimated_laps(self):
        return 300

    @timer.LapTimerMixin.time_this
    def execute(self):
        for __ in range(self.get_estimated_laps() +1):
            self.execute_lap()

    @timer.LapTimerMixin.time_lap
    def execute_lap(self):
        sleep(nrandom(self.get_initial_lap_estimate(), 0.01))

if __name__ == "__main__":
    s = SlowRepeatingScript()
    s.execute()
