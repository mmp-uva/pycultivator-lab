"""Input related mixin classes"""

from mixin import Mixin
import sys

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class ConsoleMixin(Mixin):
    """General Console output mixin"""

    @staticmethod
    def write(msg, start="", end=""):
        """Write a message to the console"""
        sys.stdout.write("{}{}{}".format(start, msg, end))
        sys.stdout.flush()

    @classmethod
    def writeln(cls, msg, start=""):
        cls.write(msg, start=start, end="\n")

    @staticmethod
    def write_error(msg, start="", end=""):
        """Writes a error message to the console"""
        sys.stderr.write("{}{}{}".format(start, msg, end))
        sys.stderr.flush()

    @classmethod
    def writeln_error(cls, msg, start=""):
        cls.write_error(msg, start=start, end="\n")


class ConsoleInputMixin(ConsoleMixin):

    @classmethod
    def read_input(cls, question, vtype=str, default=None, max_attempts=3):
        result, attempt = None, 0
        while result is None or attempt > max_attempts:
            if sys.version_info[0] < 3:
                v = raw_input(question)
            else:
                v = input(question)
            # convert to desired type
            try:
                result = vtype(v)
            except ValueError:
                msg = "Sorry, I didn't catch that! Can you repeat? ({} attempts left)".format(
                    max_attempts - attempt
                )
                cls.write(msg)
                attempt += 1
        if attempt > max_attempts:
            result = default
        return result

    @classmethod
    def read_confirmation(cls, question, is_strict=False, max_attempts=3):
        result = False
        if is_strict:
            question = "{} (Y/n)".format(question)
        else:
            question = "{} (yes/no)".format(question)
        response = cls.read_input(question, max_attempts=max_attempts)
        if (is_strict and response == "Y") or (not is_strict and response.lower() in ("y", "yes")):
            result = True
        return result


class ConsoleTableMixin(ConsoleMixin):
    """Prints restructured tables"""

    def write_table(self, data, fields=None, capitalize=False):
        """Writes a table from a list of dictionaries

        :type data: list[dict[str, str | int | float]] | list[list[str | int | float]]
        :type fields: list[str] | None
        """
        if fields is None:
            fields = []
            if len(data) > 0:
                row = data[0]
                if isinstance(row, dict):
                    fields = row.keys()
                if isinstance(row, list):
                    fields = ["Column {}".format(i) for i in range(len(row))]
        self.write_table_header(fields, capitalize=capitalize)
        if len(data) > 0:
            for row in data:
                if isinstance(row, dict):
                    self.write_dict_table_row(row, fields=fields)
                if isinstance(row, list):
                    self.write_table_row(row)
        # done

    def write_dict_table(self, data, fields=None, capitalize=None):
        """Writes a table from a dictionary

        :type data: dict[str, list[str | int | float]]
        :type fields: list[str]
        """
        if fields is None:
            fields = data.keys()
        rows = 0 if len(fields) == 0 else len(data[fields[0]])
        if rows > 0:
            self.write_table_header(fields, capitalize=capitalize)
            for row_idx in range(rows):
                values = []
                for f in fields:
                    v = data[f][row_idx] if f in data.keys() else ""
                    values.append(v)
                self.write_table_row(values)
        # done

    def write_pandas_table(self, data, fields=None, capitalize=False):
        """Writes a table based on a pandas Data Frame

        :type data: pandas.DataFrame
        """
        if fields is None:
            fields = data.columns
        if len(data) > 0:
            self.write_table_header(fields, capitalize=capitalize)
            for idx, row in data.iterrows():
                self.write_pandas_table_row(row, fields)
        # done

    def write_pandas_table_row(self, data, fields=None):
        """Write a pandas Data Series row to the console

        :type data: pandas.Series
        """
        if fields is None:
            fields = data.keys()
        self.write_dict_table_row(data, fields=fields)

    def write_dict_table_row(self, data, fields=None):
        """Write a dictionary a

        >>> ConsoleTableMixin().write_dict_table_row({'a': 1}, ['a'])
        | 1                |
        +------------------+
        >>> ConsoleTableMixin().write_dict_table_row({'a': 1}, ['A'])
        | 1                |
        +------------------+
        >>> ConsoleTableMixin().write_dict_table_row({'a': 1}, ['b'])
        |                  |
        +------------------+

        :type data: dict[str, str | int | float]
        :type fields: list[str]
        """
        values = []
        if fields is None:
            fields = data.keys()
        for spec in fields:
            field = None
            if isinstance(spec, str):
                field = spec
            if isinstance(spec, (tuple, list)):
                field, name = spec
            if field is not None:
                keys = filter(lambda k: isinstance(k, str) and k.lower() == field.lower(), data.keys())
                v = data[keys[0]] if len(keys) > 0 else ""
                values.append(v)
        self.write_table_row(values)

    def write_table_header(self, names, capitalize=False):
        """Print a table header with column names from the list `names`"""
        self.write_table_edge(len(names))
        result = ""
        for idx, spec in enumerate(names):
            name = None
            if isinstance(spec, str):
                name = spec
            if isinstance(spec, (tuple, list)):
                field, name = spec
            if name is not None:
                lsep = "|" if idx == 0 else ""
                if not isinstance(name, str):
                    name = str(name)
                if capitalize:
                    name = name.title()
                result = "{}{} {:>16s} |".format(result, lsep, name)
        self.write(result, start="", end="\n")
        self.write_table_edge(len(names))
        return result

    def write_table_row(self, values):
        """Print a list of values as a row in a table

        :type values: list[str | int | float]
        """
        result = ""
        for idx, value in enumerate(values):
            lsep = "|" if idx == 0 else ""
            if isinstance(value, int):
                value = "{:<16d}".format(value)
            if isinstance(value, float):
                value = "{:<16.8f}".format(value)
            result = "{}{} {:<16s} |".format(result, lsep, value)
        self.write(result, start="", end="\n")
        self.write_table_edge(len(values))
        return result

    def write_table_edge(self, n):
        """Print a table edge spanning `n` number of columns"""
        result = ""
        for idx in range(n):
            lsep = "+" if idx == 0 else ""
            result = "{}{}-{:-<16s}-+".format(result, lsep, "")
        self.write(result, start="", end="\n")
        return result
