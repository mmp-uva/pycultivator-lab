
"""
The Script Module provides a basic front-end script facilities.

Providing you with a standardized class that can be called from a terminal program.

Upon running the script via the start() method, it will prepare itself, execute the script and clean-up.

Example usages:
 - Prepare: connect to device
 - execute: measure OD
 - cleanup: disconnect from device

The script class comes with features:
 - Write messages to standard output.
 - Write a pause message to standard output (updated every second).
 - ABSTRACT load() method for loading configuration objects from a configuration source.

"""

from scripts import *
from attributes import *

__all__ = [
    "MetaScript", "MetaScriptException", "ConfigurableMetaScript",
    "ScriptAttribute", "ScriptListAttribute", "ScriptCountingAttribute", "ScriptAttributeException"
]



