
from pycultivator.core.objects import PCObject
from pycultivator.core.objects.configurable import ConfigurableMeta, ConfigurableObject
from pycultivator.core import pcException, pcTemplate
from attributes import *
import sys

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = [
    "MetaScript", "MetaScriptException",
    "ConfigurableMetaScript"
]


class MetaScriptMeta(type):

    def __init__(cls, name, bases=None, attrs=None):
        super(MetaScriptMeta, cls).__init__(name, bases, attrs)
        if bases is None:
            bases = []
        if attrs is None:
            attrs = {}
        # create class
        cls.defined_attributes = {}
        # fill meta with meta from parents
        for base in bases:
            if isinstance(base, MetaScriptMeta):
                cls.defined_attributes.update(base.getDefinedAttributes())
        # process variables
        for obj_name, obj in attrs.items():
            if isinstance(obj, ScriptAttribute):
                cls._registerAttribute(obj_name, obj)

    def _registerAttribute(cls, name, obj):
        # remove _ from obj_name
        pub_name = name.lstrip("_")
        # store variable under public name in meta
        cls.defined_attributes[pub_name] = obj
        # if variable is public: create public property
        setattr(cls, pub_name, pcTemplate.DeferredAttribute(pub_name, "_attributes"))

    def getDefinedAttributes(cls):
        return cls.defined_attributes


class MetaScript(PCObject):

    __metaclass__ = MetaScriptMeta

    def __init__(self, *args, **kwargs):
        """Initialize the Template with it's variables

        To prevent sharing of variable values between template instances, a deepcopy is made of each variable.
        The __init__ method should also be used to register methods to the event hooks of the variables.
        """
        super(MetaScript, self).__init__(*args, **kwargs)
        self._attributes = {}
        # populate variables dictionary from the _meta
        import copy
        for k in self.defined_attributes.keys():
            variable = copy.deepcopy(self.defined_attributes[k])
            variable.set_name(k)
            self._attributes[k] = variable
            # store variable object under private name in class
            setattr(self, "_{}".format(k), variable)
        # initialize the attributes with values given as keywords
        for kwarg in kwargs.keys():
            if kwarg in self.attributes.keys():
                self.attributes[kwarg].set(kwargs[kwarg])
                # remove keyword from kwargs
                kwargs.pop(kwarg)
                # done

    @classmethod
    def get_defined_attributes(cls):
        return cls.defined_attributes

    @property
    def attributes(self):
        """Dictionary of the variables used in this script instance

        :return: All variables in this object
        :rtype: dict[str, pycultivator_lab.scripts.meta.attributes.ScriptAttribute]
        """
        return self._attributes

    # actions

    def define_parser(self):
        """Creates an Argument Parser with all the attributes necessary for this script"""
        import argparse
        parser = argparse.ArgumentParser()
        return self.add_arguments(parser)

    def add_arguments(self, parser, _attributes=None):
        """Creates an argument parser

        :type parser: argparse.ArgumentParser
        :param _attributes: Attributes to create arguments from (None = self.attributes)
        :type _attributes: None | dict[str, pycultivator_lab.scripts.meta.attributes.ScriptAttribute]
        :rtype: argparse.ArgumentParser
        """
        # sort by weight from high to low
        if _attributes is None:
            _attributes = self.attributes
        names = sorted(_attributes.keys(), key=lambda k: _attributes[k].weight, reverse=True)
        for name in names:
            parser = _attributes[name].add_argument(parser)
        return parser

    def parse_arguments(self, arguments=None, _attributes=None):
        """Read and store values obtained from argparse

        :type arguments: dict[str | str | float | int | bool | list]
        :param _attributes: Attributes to read values into (None = self.attributes)
        :type _attributes: None | dict[str, pycultivator_lab.scripts.meta.attributes.ScriptAttribute]
        """
        result = True
        if arguments is None:
            arguments = {}
        if _attributes is None:
            _attributes = self.attributes
        for name, attribute in _attributes.items():
            result = attribute.parse_argument(arguments) and result
        return result

    def read_arguments(self, arguments=None):
        results = {}
        if arguments is None:
            arguments = {}
        for name in self.attributes.keys():
            attribute = self.attributes[name]
            public_name = attribute.get_argument_name()
            value = attribute.read_argument(arguments)
            results[public_name] = value if value is not None else attribute.default
        return results

    def reset(self):
        for attribute in self.attributes.values():
            attribute.reset()

    def load_arguments(self):
        """Load the command-line arguments given to this script"""
        # reset all attributes to default values
        self.reset()
        p = self.define_parser()
        # load arguments
        a = vars(p.parse_args())
        # parse arguments
        result = self.parse_arguments(a)
        return result

    def run(self, load_arguments=True):
        """Run the script. Main entry point"""
        result = False
        try:
            if load_arguments:
                self.load_arguments()
            try:
                self.info("Script started...")
                result = self.prepare()
                if result:
                    result = self.execute()
            finally:
                result = self.clean() and result
                self.info("Script finished...")
        except KeyboardInterrupt:
            self.interrupted()
            result = False
        return result

    def prepare(self):
        return True

    def execute(self):
        raise NotImplementedError

    def clean(self):
        return True

    # console information
    # these methods print to console (can be overloaded by a logging script)

    def interrupted(self):
        self.info("Interrupted by user, quit gracefully. Please wait!")

    def error(self, msg, start="", end="\n"):
        if self.getLog().getEffectiveLevel() >= 50:
            sys.stderr.write("{}[ERROR] {}{}".format(start, msg, end))
            sys.stderr.flush()
        else:
            self.getLog().error(msg)

    def warning(self, msg, start="", end="\n"):
        if self.getLog().getEffectiveLevel() >= 30:
            sys.stderr.write("{}[WARNING] {}{}".format(start, msg, end))
            sys.stdout.flush()
        else:
            self.getLog().warning(msg)

    def info(self, msg, start="", end="\n"):
        if self.getLog().getEffectiveLevel() >= 20:
            sys.stderr.write("{}[INFO] {}{}".format(start, msg, end))
            sys.stdout.flush()
        else:
            self.getLog().info(msg)

    def debug(self, msg, start="", end="\n"):
        self.getLog().debug(msg)


class MetaScriptException(pcException.PCException):
    """Exception raised by a MetaScript"""

    def __init__(self, msg):
        super(MetaScriptException, self).__init__(msg=msg)


class ConfigurableMetaScriptMeta(ConfigurableMeta, MetaScriptMeta):
    """A configurable Meta Script"""

    def __init__(cls, name, bases, attrs):
        super(MetaScriptMeta, cls).__init__(name, bases, attrs)
        # create class
        cls.defined_attributes = {}
        # fill meta with meta from parents
        for base in bases:
            if isinstance(base, MetaScriptMeta):
                cls.defined_attributes.update(base.getDefinedAttributes())
        # process variables
        for obj_name, obj in attrs.items():
            if isinstance(obj, ScriptAttribute):
                cls._registerAttribute(obj_name, obj)


class ConfigurableMetaScript(MetaScript, ConfigurableObject):

    __metaclass__ = ConfigurableMetaScriptMeta

    def execute(self):
        raise NotImplementedError
