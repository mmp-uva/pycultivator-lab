"""Extra classes for  meta scripts"""

from pycultivator_lab.scripts.meta import *
from pycultivator_lab.scripts.meta.validators import *


class ConfigurableScript(MetaScript):
    """Looks for pycultivator.ini files to load attributes from"""

    ini = ScriptAttribute(str, )

    def run(self, load_arguments=True, load_ini=True):
        try:
            if load_arguments:
                self.load_arguments()
            if load_ini:
                self.load_ini()
        except KeyboardInterrupt:
            self.interrupted()
        # do rest of start, but now do not load arguments
        super(ConfigurableScript, self).run(load_arguments=False)

    def find_ini(self):
        """Searches for ini files"""

    def load_ini(self):
        pass

    def execute(self):
        raise NotImplementedError


class LoggingScript(MetaScript):
    """Writes messages to log files or console"""

    verbose = ScriptCountingAttribute(
        default=10, validator=RangeValidator(minimum=0, include_minimum=True, maximum=100, include_maximum=True)
    )

    def execute(self):
        raise NotImplementedError
