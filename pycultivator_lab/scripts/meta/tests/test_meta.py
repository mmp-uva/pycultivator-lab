# coding=utf-8
"""Test cases for the __init__ module in pycultivator_lab.scripts.meta"""

from pycultivator_lab.scripts.meta import *
from pycultivator_lab.scripts.meta.attributes import *
from pycultivator_lab.scripts.meta.validators import *
from pycultivator.core.tests import UvaSubjectTestCase
import argparse

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class ExampleMetaScript(MetaScript):

    signal = ScriptAttribute(int)
    intensity = ScriptAttribute(
        int, validator=RangeValidator(minimum=0, maximum=None, include_minimum=True)
    )
    well = ScriptListAttribute(int, list_all=range(24), allow_duplicates=False)

    def execute(self):
        return True


class TestExampleMetaScript(UvaSubjectTestCase):
    """Test the ScriptAttrbute class"""

    _subject_cls = ExampleMetaScript
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """ Returns the class of the subject of the tests case

        :return:
        :rtype: type[T <= ExampleMetaScript]
        """
        return super(TestExampleMetaScript, cls).getSubjectClass()

    def getSubject(self):
        """ Returns the instance of the subject of the tests case

        :return:
        :rtype: ExampleMetaScript
        """
        return super(TestExampleMetaScript, self).getSubject()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()()

    def test_attributes(self):
        self.assertTrue("well" in self.getSubject().attributes)
        self.assertEqual(self.getSubject().well, [None])
        self.assertTrue("intensity" in self.getSubject().attributes)
        self.assertEqual(self.getSubject().intensity, None)

    def test_define_parser(self):
        self.assertIsInstance(self.getSubject().define_parser(), argparse.ArgumentParser)

    def test_add_arguments(self):
        parser = argparse.ArgumentParser()
        parser = self.getSubject().add_arguments(parser)
        arguments = [a.dest for a in parser._actions]
        self.assertIn("intensity", arguments)
        self.assertNotIn("intensity_range", arguments)
        self.assertIn("wells", arguments)
        self.assertIn("well_range", arguments)
        self.assertIn("all_wells", arguments)

    def test_parse_arguments(self):
        arguments = {'wells': [1], 'intensity': 300}
        self.getSubject().parse_arguments(arguments)
        self.assertEqual(self.getSubject().well, [1])
        self.assertEqual(self.getSubject().intensity, 300)
        arguments = {'intensity': -1}
        self.getSubject().reset()
        self.getSubject().parse_arguments(arguments)
        self.assertEqual(self.getSubject().intensity, None)

    def test_read_arguments(self):
        arguments = {'wells': [1], 'intensity': 300}
        results = self.getSubject().read_arguments(arguments)
        self.assertIn("wells", results.keys())
        self.assertEqual(results["wells"], [1])
        self.assertIn("intensity", results.keys())
        self.assertEqual(results["intensity"], 300)
        arguments = {'intensity': -1}
        self.getSubject().reset()
        results = self.getSubject().read_arguments(arguments)
        self.assertIn("intensity", results.keys())
        self.assertEqual(results["intensity"], -1)
