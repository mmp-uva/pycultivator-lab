# coding=utf-8
"""Test cases for the attributes module in pycultivator_lab.scripts.meta"""

from pycultivator_lab.scripts.meta.attributes import *
from pycultivator.core.tests import UvaSubjectTestCase
import argparse

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TestScriptAttribute(UvaSubjectTestCase):
    """Test the ScriptAttrbute class"""

    _subject_cls = ScriptAttribute
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """ Returns the class of the subject of the tests case

        :return:
        :rtype: type[T <= pycultivator_lab.scripts.meta.attributes.TaskAttribute]
        """
        return super(TestScriptAttribute, cls).getSubjectClass()

    def getSubject(self):
        """ Returns the instance of the subject of the tests case

        :return:
        :rtype: pycultivator_lab.scripts.meta.attributes.TaskAttribute
        """
        return super(TestScriptAttribute, self).getSubject()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(str, default="default", name="a")

    def test_get_name(self):
        self.assertEqual(self.getSubject().get_name(), "a")
        self.getSubject().set_name("a_b_c")
        self.assertEqual(self.getSubject().get_name(), "a_b_c")

    def test_get_argument_name(self):
        self.assertEqual(self.getSubject().get_argument_name(), "a")
        self.getSubject().set_name("abc")
        self.assertEqual(self.getSubject().get_argument_name(), "abc")
        self.getSubject().set_required(False)
        self.assertEqual(self.getSubject().get_argument_name(), "--abc")
        self.getSubject().set_name("a_b_c")
        self.getSubject().set_required(True)
        self.assertEqual(self.getSubject().get_argument_name(), "a-b-c")
        self.getSubject().set_required(False)
        self.assertEqual(self.getSubject().get_argument_name(), "--a-b-c")

    def test_add_argument(self):
        parser = argparse.ArgumentParser()
        # required attribute
        parser = self.getSubject().add_argument(parser)
        self.assertIn("a", [a.dest for a in parser._actions])
        self.assertNotIn("-a", parser._option_string_actions)
        self.assertNotIn("a", parser._option_string_actions)
        # optional attribute
        self.getSubject().set_required(False)
        parser = self.getSubject().add_argument(parser)
        self.assertIn("a", [a.dest for a in parser._actions])
        self.assertIn("-a", parser._option_string_actions)
        self.getSubject().add_alias("b")
        parser = argparse.ArgumentParser()
        parser = self.getSubject().add_argument(parser)
        self.assertIn("-b", parser._option_string_actions)

    def test_parse_argument(self):
        self.assertEqual(self.getSubject().get(), 'default')
        self.getSubject().parse_argument({'a': 1})
        self.assertEqual(self.getSubject().get(), 1)
        self.getSubject().reset()
        self.getSubject().parse_argument({'b': 1})
        self.assertEqual(self.getSubject().get(), 'default')
        self.getSubject().set_name("a_b_c")
        self.getSubject().reset()
        self.getSubject().parse_argument({'a-b-c': 1})
        self.assertEqual(self.getSubject().get(), 1)
        self.getSubject().reset()
        self.getSubject().parse_argument({'a_b_c': 1})
        self.assertEqual(self.getSubject().get(), 'default')

    def test_read_argument(self):
        v = self.getSubject().read_argument({'a': 1})
        self.assertEqual(v, 1)
        v = self.getSubject().read_argument({'b': 1})
        self.assertEqual(v, None)

    def test_copy(self):
        s = self.getSubjectClass()(int, name="a", default=1, weight=3, help="bla bla", validator="x")
        """:type: pycultivator_lab.scripts.meta.attributes.Attribute"""
        s1 = s.copy()
        self.assertEqual(s._name, s1._name)
        self.assertEqual(s.get_type(), s1.get_type())
        self.assertEqual(s.get_default(), s1.get_default())
        self.assertEqual(s.get_weight(), s1.get_weight())
        self.assertEqual(s.get_help(), s1.get_help())
        self.assertEqual(s.get_validators(), s1.get_validators())


class TestScriptListAttribute(TestScriptAttribute):

    _subject_cls = ScriptListAttribute
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """ Returns the class of the subject of the tests case

        :return:
        :rtype: type[T <= pycultivator_lab.scripts.meta.attributes.TaskListAttribute]
        """
        return super(TestScriptListAttribute, cls).getSubjectClass()

    def getSubject(self):
        """ Returns the instance of the subject of the tests case

        :return:
        :rtype: pycultivator_lab.scripts.meta.attributes.TaskListAttribute
        """
        return super(TestScriptListAttribute, self).getSubject()

    def initSubject(self, *args, **kwargs):
        obj = self.getSubjectClass()(
            int, default=[1,2,3], name="a", list_all=range(99), allow_range=True
        )
        return obj

    def test_init(self):
        s = self.getSubjectClass()(int, 1)
        self.assertEqual(s.get(), [1])
        s = self.getSubjectClass()(int, [1])
        self.assertEqual(s.get(), [1])
        s = self.getSubjectClass()(int, (1,))
        self.assertEqual(s.get(), [1])

    def test_get_name(self):
        self.assertEqual(self.getSubject().get_name(), "as")
        self.getSubject().set_name("a_b_c")
        self.assertEqual(self.getSubject().get_name(), "a_b_cs")
        self.getSubject().set_name("wells")
        self.assertEqual(self.getSubject().get_name(), "wells")

    def test_get_argument_name(self):
        self.assertEqual(self.getSubject().get_argument_name(), "as")
        self.getSubject().set_name("well")
        self.assertEqual(self.getSubject().get_argument_name(), "wells")
        self.getSubject().set_name("a_b_c")
        self.assertEqual(self.getSubject().get_argument_name(), "a-b-cs")
        self.getSubject().set_name("a")
        self.getSubject().set_required(False)
        self.assertEqual(self.getSubject().get_argument_name(), "--as")
        self.getSubject().set_name("well")
        self.assertEqual(self.getSubject().get_argument_name(), "--wells")
        self.getSubject().set_name("a_b_c")
        self.assertEqual(self.getSubject().get_argument_name(), "--a-b-cs")

    def test_get_range_argument_name(self):
        self.assertEqual(self.getSubject().get_range_argument_name(), "--a-range")
        self.getSubject().set_name("well")
        self.assertEqual(self.getSubject().get_range_argument_name(), "--well-range")
        self.getSubject().set_name("a_b_c")
        self.assertEqual(self.getSubject().get_range_argument_name(), "--a-b-c-range")

    def test_get_all_argument_name(self):
        self.assertEqual(self.getSubject().get_all_argument_name(), "--all-as")
        self.getSubject().set_name("well")
        self.assertEqual(self.getSubject().get_all_argument_name(), "--all-wells")
        self.getSubject().set_name("a_b_c")
        self.assertEqual(self.getSubject().get_all_argument_name(), "--all-a-b-cs")

    def test_add_argument(self):
        parser = argparse.ArgumentParser()
        parser = self.getSubject().add_argument(parser)
        arguments = filter(lambda a: a.dest == "as", parser._actions)
        self.assertTrue(len(arguments) > 0)
        self.assertTrue(type(arguments[0]), argparse._AppendAction)
        # tests for range
        arguments = filter(lambda a: a.dest == "a_range", parser._actions)
        self.assertTrue(len(arguments) > 0)
        self.assertTrue(type(arguments[0]), argparse._StoreAction)
        self.getSubject().allow_all(range(99))
        parser = argparse.ArgumentParser()
        parser = self.getSubject().add_argument(parser)
        arguments = filter(lambda a: a.dest == "a_range", parser._actions)
        self.assertTrue(len(arguments) > 0)
        self.assertTrue(type(arguments[0]), argparse._StoreTrueAction)

    def test_parse_argument(self):
        self.assertEqual(self.getSubject().get(), [1, 2, 3])
        self.getSubject().parse_argument({'as': 1})
        self.assertEqual(self.getSubject().get(), [1])
        self.getSubject().reset()
        self.getSubject().parse_argument({'b': 1})
        self.assertEqual(self.getSubject().get(), [1, 2, 3])
        self.getSubject().reset()
        self.getSubject().parse_argument({'as': 1, 'a_range': [4]})
        self.assertEqual(self.getSubject().get(), [1, 0, 1, 2, 3])
        self.getSubject().allow_duplicates(False)
        self.getSubject().parse_argument({'as': 1, 'a_range': [4]})
        self.assertListEqual(
            sorted(self.getSubject().get()),
            sorted([1, 0, 2, 3])
        )
        self.getSubject().allow_all(range(10))
        self.getSubject().parse_argument({'as': -1, 'all_as': True})
        self.assertListEqual(
            sorted(self.getSubject().get()),
            sorted(range(10))
        )

    def test_read_argument(self):
        v = self.getSubject().read_argument({'as': 1})
        self.assertEqual(v, [1])
        v = self.getSubject().read_argument({'b': 1})
        self.assertEqual(v, [])

    def test_read_range_argument(self):
        v = self.getSubject().read_range_argument({'a_range': []})
        self.assertEqual(v, [])
        v = self.getSubject().read_range_argument({'as': []})
        self.assertEqual(v, [])
        v = self.getSubject().read_range_argument({'as': [1]})
        self.assertEqual(v, [])
        v = self.getSubject().read_range_argument({'a_range': [1]})
        self.assertEqual(v, range(1))
        v = self.getSubject().read_range_argument({'a_range': [1, 10]})
        self.assertEqual(v, range(1, 10))
        v = self.getSubject().read_range_argument({'a_range': [1, 10]})
        self.assertEqual(v, range(1, 10))

    def test_read_all_argument(self):
        v = self.getSubject().read_all_argument({'all_as': True})
        self.assertEqual(v, range(99))
        v = self.getSubject().read_all_argument({'as': True})
        self.assertEqual(v, [])


class TestScriptCountingAttribute(TestScriptAttribute):

    _subject_cls = ScriptCountingAttribute
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """ Returns the class of the subject of the tests case

        :return:
        :rtype: type[T <= pycultivator_lab.scripts.meta.attributes.TaskCountingAttribute]
        """
        return super(TestScriptCountingAttribute, cls).getSubjectClass()

    def getSubject(self):
        """ Returns the instance of the subject of the tests case

        :return:
        :rtype: pycultivator_lab.scripts.meta.attributes.TaskCountingAttribute
        """
        return super(TestScriptCountingAttribute, self).getSubject()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(default=0, name="a")

    def test_add_argument(self):
        parser = argparse.ArgumentParser()
        # required attribute
        parser = self.getSubject().add_argument(parser)
        self.assertIn("a", [a.dest for a in parser._actions])
        self.assertIn("-a", parser._option_string_actions)
        self.assertNotIn("a", parser._option_string_actions)
        # optional attribute
        self.getSubject().set_required(False)
        parser = argparse.ArgumentParser()
        parser = self.getSubject().add_argument(parser)
        self.assertIn("a", [a.dest for a in parser._actions])
        self.assertIn("-a", parser._option_string_actions)
        self.getSubject().add_alias("b")
        parser = argparse.ArgumentParser()
        parser = self.getSubject().add_argument(parser)
        self.assertIn("-b", parser._option_string_actions)

    def test_get_argument_name(self):
        self.assertEqual(self.getSubject().get_argument_name(), "-a")
        self.getSubject().set_name("abc")
        self.assertEqual(self.getSubject().get_argument_name(), "--abc")
        self.getSubject().set_required(False)
        self.assertEqual(self.getSubject().get_argument_name(), "--abc")
        self.getSubject().set_name("a_b_c")
        self.getSubject().set_required(True)
        self.assertEqual(self.getSubject().get_argument_name(), "--a-b-c")
        self.getSubject().set_required(False)
        self.assertEqual(self.getSubject().get_argument_name(), "--a-b-c")

    def test_parse_argument(self):
        self.assertEqual(self.getSubject().get(), 0)
        self.getSubject().parse_argument({'a': 1})
        self.assertEqual(self.getSubject().get(), 1)
        self.getSubject().reset()
        self.getSubject().parse_argument({'b': 1})
        self.assertEqual(self.getSubject().get(), 0)
        self.getSubject().reset()
        self.getSubject().multiplier = 10
        self.getSubject().parse_argument({'a': 1})
        self.assertEqual(self.getSubject().get(), 10)

    def test_read_argument(self):
        v = self.getSubject().read_argument({'a': 3})
        self.assertEqual(v, 3)
        self.getSubject().multiplier = 10
        v = self.getSubject().read_argument({'a': 3})
        self.assertEqual(v, 30)
