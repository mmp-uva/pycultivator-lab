
from pycultivator.core.objects import PCObject
from pycultivator.core import pcException
# from extras import logger
# from datetime import datetime as dt
# import os, logging, argparse


__all__ = [
    "ScriptAttribute", "ScriptAttributeException",
    "ScriptListAttribute", "ScriptCountingAttribute"
]


class ScriptAttribute(PCObject):

    def __init__(
            self, type_, default=None, name=None, aliases=None, weight=0,
            is_required=True, help=None, validators=None, **kwargs
    ):
        """Initialize a Script Attribute

        Script Attributes are used in MetaScript to define it's public attributes.
        The attributes are then used to automatically create the arguments of it's argument parser

        """
        super(ScriptAttribute, self).__init__()
        self._name = None
        self.set_name(name)
        self._type = type_
        self._default = default
        self._value = None
        self._is_required = is_required is True
        # extra
        if aliases is None:
            aliases = []
        if not isinstance(aliases, (tuple, list, set)):
            aliases = [aliases]
        self._aliases = set(aliases)
        self._weight = weight
        self._help = help
        # register validators to class
        if validators is None:
            validators = []
        if not isinstance(validators, (tuple, list, set)):
            validators = [validators]
        self._validators = validators
        self.reset()
        # ignore kwargs

    def get_name(self):
        return self._name

    def get_argument_name(self):
        if not self.has_name():
            raise ScriptAttributeException("Please define a name")
        name = self.get_name()
        # replace underscores with dashes
        name = name.replace("_", "-")
        prefix = ""
        # we put a prefix if we are optional or if we also have aliases
        if not self.is_required() or len(self.aliases) > 0:
            prefix = "-" if len(name) == 1 else "--"
        return "{}{}".format(prefix, name)

    @property
    def name(self):
        return self.get_name()

    def has_name(self):
        return self.get_name() is not None

    def set_name(self, v):
        self._name = v

    def get_type(self):
        return self._type

    def get_default(self):
        return self._default

    @property
    def default(self):
        return self.get_default()

    def get(self):
        return self._value

    @property
    def value(self):
        return self.get()

    def set(self, v):
        self._value = v

    @value.setter
    def value(self, v):
        self.set(v)

    def is_set(self):
        return self.value is not None

    def is_required(self):
        return self._is_required is True

    def set_required(self, state):
        self._is_required = state is True

    def get_aliases(self):
        """Extra name arguments of script

        :rtype: set[str]
        """
        return self._aliases

    @property
    def aliases(self):
        return self.get_aliases()

    def get_argument_aliases(self):
        """Returns parsed alias names"""
        results = []
        for alias in self.aliases:
            name = alias.rstrip("-")
            prefix = "--" if len(name) > 1 else "-"
            results.append("{}{}".format(prefix, name))
        return results

    def add_alias(self, name):
        if name == self.get_name():
            raise ValueError("Alias cannot be the same as the name")
        return self._aliases.add(name)

    def get_weight(self):
        return self._weight

    @property
    def weight(self):
        return self.get_weight()

    def get_help(self):
        return self._help

    @property
    def help(self):
        return self.get_help()

    def get_validators(self):
        return self._validators

    @property
    def validators(self):
        return self.get_validators()

    # actions

    def reset(self):
        """Reset the value of the attribute to the default value"""
        self.set(self.get_default())

    def get_argument_names(self):
        """Returns a set with the argument name and it's aliases

        :rtype: set[str]
        """
        names = {self.get_argument_name()}
        return names.union(self.get_argument_aliases())

    def add_argument(self, parser, names=None, preload=True, **kwargs):
        """Adds itself as an argument to the given parser

        :type parser: argparse.ArgumentParser
        :type settings: None | dict[str, float | str | int | bool]
        :rtype: argparse.ArgumentParser
        """
        settings = {}
        if names is None:
            names = self.get_argument_names()
        if preload:
            settings = {
                'type': self.get_type(),
                'help': self.get_help(),
            }
            if len(names) > 1 or not self.is_required():
                settings["dest"] = self.get_name()
                settings["required"] = self.is_required()
        # add kwargs
        settings.update(kwargs)
        # prevent required for positionals
        if len(names) == 1 and "required" in settings:
            settings.pop("required")
        # create argument
        return self._add_argument(parser, names=names, settings=settings)

    def _add_argument(self, parser, names, settings=None):
        if settings is None:
            settings = {}
        parser.add_argument(*names, **settings)
        return parser

    def parse_argument(self, arguments):
        """Read from the dictionary and store value as current value

        :type arguments: dict[str, None | int | float | str]
        """
        result = True
        value = self.read_argument(arguments)
        if value is not None:
            result = self.validate(value)
            if result:
                self.set(value)
        return result

    def read_argument(self, arguments):
        """Reads the argument value from the dictionary and returns it's parsed value

        :type arguments: dict[str, None | int | float | str]
        """
        local_name = self.get_name()
        public_name = self.get_argument_name()
        value = None
        # check if argument is set
        if public_name in arguments.keys():
            value = arguments[public_name]
        elif local_name in arguments.keys():
            value = arguments[local_name]
        return value

    def validate(self, value):
        result = True
        for validator in self.validators:
            result = validator.test(value)
        if not result:
            self.getLog().warning(
                "Value for {} attribute is not valid: {}, ignoring".format(
                    self.get_name(), value
                )
            )
        return result

    def copy(self, memo=None):
        """Create a deepcopy of the object

        :param memo: Extra memo for deepcopy
        :return: A deep copy of this object
        :rtype: pycultivator_lab.scripts.meta.attributes.ScriptAttribute
        """
        import copy
        result = type(self)(
            self._type, name=self._name,
            default=copy.deepcopy(self.default, memo),
            is_required=self.is_required(),
            aliases=copy.deepcopy(self.aliases, memo),
            weight=copy.deepcopy(self.weight, memo),
            help=copy.deepcopy(self.help, memo),
            validators=self.validators[:]
        )
        result.set(copy.deepcopy(self._value, memo))
        return result

    def __str__(self):
        return self.short_string()

    def short_string(self):
        return "{required}<{type}> {name} (default={default}){value}".format(
            required="required " if self.is_required() else "",
            type=self.get_type().__name__,
            name=self.name, default=self.default,
            value=" {}".format(self.value) if self.is_set() else ""
        )

    def full_string(self):
        return "- {required} <{type}> {name}\n\tdefault={default}\n\tvalue={value}\n\n\t{help}".format(
            required="required" if self.is_required() else "",
            type=self.get_type().__name__,
            name=self.name, default=self.default,
            value=" {}".format(self.value) if self.is_set() else "",
            help=self.get_help()
        )

class ScriptAttributeException(pcException.PCException):
    """Exception raised by script attributes"""

    def __init__(self, msg):
        super(ScriptAttributeException, self).__init__(msg=msg)


class ScriptListAttribute(ScriptAttribute):
    """A Script Attribute that is a list"""

    def __init__(self, type_, default=None, list_all=None, allow_range=True, allow_duplicates=True, **kwargs):
        """Initialze a List Attribute for MetaScripts

        :param list_all: Allow users to write "--all-[variable]", and then set the value to the list given here.
        :param allow_range: Allow users to write "--[variable]-range", and then set the value to the given range.
        :param allow_duplicates: Whether to allow duplicate values in the list
        """
        if not isinstance(default, (tuple, list)):
            default = [default]
        super(ScriptListAttribute, self).__init__(type_=type_, default=list(default), **kwargs)
        self._list_all = list_all
        self._allow_range = allow_range
        self._allow_duplicates = allow_duplicates

    def get_name(self):
        """Get the local name of the name argument

        >>> a = ScriptListAttribute(int, name="well")
        ... a.get_name()
        wells

        :rtype: str
        """
        name = super(ScriptListAttribute, self).get_name()
        if name is None:
            raise ScriptAttributeException("Please define a name")
        # replace dashes with underscores
        name = name.replace("-", "_")
        # remove tailing s
        name = name.rstrip("s")
        return "{}s".format(name)

    def get_all(self):
        return self._list_all

    def allows_all(self):
        return self.get_all() is not None

    def allow_all(self, all_values):
        self._list_all = all_values

    def get_all_name(self):
        """Get the local name of the all argument

        >>> a = ScriptListAttribute(int, name="well")
        ... a.get_all_name()
        all_wells

        :rtype: str
        """
        name = self.get_name()
        # remove tailing s
        name = name.rstrip("s")
        return "all_{}s".format(name)

    def get_all_argument_name(self, prefix="--"):
        """Get the public name of the range argument

        >>> a = ScriptListAttribute(int, name='well')
        ... a.get_all_argument_name()
        --all-wells

        :rtype: str
        """
        name = self.get_argument_name()
        # remove prefixing dashes
        name = name.lstrip("-")
        # remove postfixing s
        name = name.rstrip("s")
        return "{}all-{}s".format(prefix, name)

    def allows_range(self):
        return self._allow_range

    def allow_range(self, state):
        self._allow_range = state is True

    def allows_duplicates(self):
        return self._allow_duplicates is True

    def allow_duplicates(self, state):
        self._allow_duplicates = state is True

    def get_range_name(self):
        """Get the local name of the range argument

        >>> a = ScriptListAttribute(int, name='well')
        ... a.get_range_name()
        well_range

        :rtype: str
        """
        name = self.get_name()
        # remove tailing s
        name = name.strip("s")
        return "{}_range".format(name)

    def get_range_argument_name(self, prefix="--"):
        """Get the public name of the range argument

        >>> a = ScriptListAttribute(int, name='well')
        ... a.get_range_argument_name()
        --well-range

        :rtype: str
        """
        name = self.get_argument_name()
        # remove prefixing dashes
        name = name.lstrip("-")
        # remove tailing s
        name = name.rstrip("s")
        return "{}{}-range".format(prefix, name)

    def add_argument(self, parser, names=None, preload=True, **kwargs):
        """Adds itself as an argument to the given parser

        :type parser: argparse.ArgumentParser
        :rtype: argparse.ArgumentParser
        """
        # create argument for each value
        parser = super(ScriptListAttribute, self).add_argument(
            parser, preload=preload, action="append"
        )
        if self.allows_range():
            parser = self.add_range_argument(parser)
        if self.allows_all():
            parser = self.add_all_argument(parser)
        return parser

    def add_range_argument(self, parser, names=None, preload=True, **kwargs):
        settings = {}
        if names is None:
            names = {self.get_range_argument_name()}
        if preload:
            settings = {
                "dest": self.get_range_name(),
                "nargs": "+",
                "type": int,
                "help": "Set {public} to a range of value. Overrules: {public}".format(
                    public=self.get_argument_name()
                )
            }
        settings.update(kwargs)
        parser = super(ScriptListAttribute, self)._add_argument(
            parser, names=names, settings=settings
        )
        return parser

    def add_all_argument(self, parser, names=None, preload=True, **kwargs):
        settings = {}
        if names is None:
            names = {self.get_all_argument_name()}
        if preload:
            settings = {
                "dest": self.get_all_name(),
                "action": "store_true",
                "help": "Set {public} to a all possible values. Overrules: {public} and {public_range}".format(
                    public=self.get_argument_name(), public_range=self.get_range_argument_name()
                )
            }
        settings.update(kwargs)
        parser = super(ScriptListAttribute, self)._add_argument(
            parser, names=names, settings=settings
        )
        return parser

    def parse_argument(self, arguments):
        """Read from the dictionary and store value as current value

        :type arguments: dict[str, None | int | float | str]
        """
        values = self.read_argument(arguments)
        range_values = []
        if self.allows_range():
            # check for the range argument
            range_values = self.read_range_argument(arguments)
        # combine both
        values += range_values
        if self.allows_all():
            # check for all argument
            all_values = self.read_all_argument(arguments)
            if len(all_values) > 0:
                values = all_values
        result = len(values) > 0
        if not self.allows_duplicates():
            values = list(set(values))
        for value in values:
            self.validate(value)
        if result:
            self.set(values)
        return result

    def read_argument(self, arguments):
        """Read from the dictionary and store value as current value

        :param arguments: Dictionary of all given command-line arguments
        :type arguments: dict[str, None | int | float | str]
        """
        values = []
        local_name = self.get_name()
        # check if argument is set
        if local_name in arguments.keys():
            value = arguments[local_name]
            if not isinstance(value, (tuple, list)):
                value = [value]
            values = list(value)
        return values

    def read_range_argument(self, arguments):
        """Read the <variable>_range parameter

        :param arguments: Dictionary of all given command-line arguments
        :type arguments: dict[str, None | int | float | str | bool | list[int | float | str]]
        :return: The values in the range
        :rtype: list[None | int | float | str]
        """
        result = []
        range_value = []
        range_local_name = self.get_range_name()
        if range_local_name in arguments.keys():
            values = arguments[range_local_name]
            if isinstance(values, (tuple, list)):
                range_value = values
        if 0 < len(range_value) <= 3:
            result = range(*range_value[:3])
        return result

    def read_all_argument(self, arguments):
        """Read the all_<variable> parameter

        :param arguments: Dictionary of all given command-line arguments
        :type arguments: dict[str, None | int | float | str | bool]
        :return: An empty list or with all the possible values
        :rtype: list[None | int | float | str]
        """
        result = []
        all_local_name = self.get_all_name()
        if all_local_name in arguments.keys():
            if arguments[all_local_name] is True:
                result = self.get_all()
        return result

    def copy(self, memo=None):
        result = super(ScriptListAttribute, self).copy(memo=memo)
        """:type: pycultivator_lab.scripts.meta.attributes.ScriptListAttribute"""
        import copy
        result.allow_all(
            copy.deepcopy(self.allows_all())
        )
        result.allow_range(self.allows_range())
        result.allow_duplicates(self.allows_duplicates())
        return result


class ScriptCountingAttribute(ScriptAttribute):
    """Attribute counting the number of times the attribute is given"""

    def __init__(self, type_=int, multiplier=1, **kwargs):
        if type_ is not int:
            self.getLog().warning("Counting Attribute type is always integer, ignoring other values")
            type_ = int
        super(ScriptCountingAttribute, self).__init__(type_=type_, **kwargs)
        self._multiplier = multiplier

    def get_argument_name(self):
        name = super(ScriptCountingAttribute, self).get_argument_name().lstrip("-")
        # always prefix with dashes
        prefix = "-" if len(name) == 1 else "--"
        return "{}{}".format(prefix, name)

    def get_multiplier(self):
        return self._multiplier

    @property
    def multiplier(self):
        return self.get_multiplier()

    def set_multiplier(self, v):
        if not isinstance(v, (int, float)):
            raise ValueError("Invalid value for multiplier")
        self._multiplier = v

    @multiplier.setter
    def multiplier(self, v):
        self.set_multiplier(v)

    def add_argument(self, parser, names=None, preload=True, **kwargs):
        """Adds itself as an argument to the given parser

        :type parser: argparse.ArgumentParser
        :rtype: argparse.ArgumentParser
        """
        settings = {}
        if names is None:
            names = self.get_argument_names()
        if preload:
            settings = {
                "help": self.get_help(),
                "action": "count",
                "dest": self.get_name()
            }
        settings.update(**kwargs)
        return super(ScriptCountingAttribute, self).add_argument(
            parser, names=names, preload=False, **settings
        )

    def read_argument(self, arguments):
        """Reads the argument value from the dictionary and returns it's parsed value

        :type arguments: dict[str, None | int | float | str]
        """
        local_name = self.get_name()
        value = None
        # check if argument is set
        if local_name in arguments.keys():
            value = arguments[local_name]
        if value is not None:
            # multiply
            value *= self.multiplier
        return value

    def copy(self, memo=None):
        result = super(ScriptCountingAttribute, self).copy(memo=memo)
        result.multiplier = self.multiplier
        return result
