
from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator_lab import protocol
import types


class ProtocolA(protocol.BaseProtocol):

    @protocol.ProtocolStep()
    def execute(self):
        pass


class TestProtocolStep(UvaSubjectTestCase):

    _abstract = False
    _subject_cls = protocol.ProtocolStep

    def test_init(self):
        ps = protocol.ProtocolStep()
        self.assertTrue(ps.name.startswith(protocol.ProtocolStep.__name__), msg="init without name")
        ps = protocol.ProtocolStep(name="a")
        self.assertEqual(ps.name, "a", msg="init with name")
        ps = protocol.ProtocolStep(name="b", magic=ProtocolA)
        self.assertEqual(ps.name, "b", msg="init with name and magic")
        ps = protocol.ProtocolStep(name="c", required=False, dependent=False, weight=10)
        self.assertEqual(ps.name, "c")
        self.assertFalse(ps.is_required)
        self.assertFalse(ps.is_dependent)
        self.assertEqual(ps.weight, 10)

    def test_decorator(self):
        p = ProtocolA()
        ps = p.steps[0]
        self.assertIsInstance(ps, protocol.ProtocolStep)
        self.assertEqual(ps.name, "execute")
        p.start()

