
from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator_lab.task import TaskContext
from pycultivator_lab.protocol import ProtocolStep, BaseProtocol, Protocol


class ContextA(TaskContext):

    def __init__(self, value=0):
        super(ContextA, self).__init__()
        self._value = value

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, v):
        self._value = v


class ProtocolA(BaseProtocol):

    @ProtocolStep()
    def add(self):
        self.context.value += 1


class ProtocolB(ProtocolA):

    def add(self):
        self.context.value += 1

    @ProtocolStep(weight=1)
    def multiply(self):
        self.context.value *= 2


class ProtocolC(ProtocolB):

    # set to None to remove Step
    multiply = None


class ProtocolC2(ProtocolB):

    @ProtocolStep(weight=-1)
    def multiply(self):
        self.context.value *= 2


class ProtocolD(ProtocolC2):

    @ProtocolStep(weight=1)
    def subtract(self):
        self.context.value -= 1


class ProtocolE(ProtocolA):

    multiply = ProtocolStep(weight=1, magic=ProtocolA)


class TestBaseProtocol(UvaSubjectTestCase):

    _abstract = False
    _subject_cls = BaseProtocol

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()("test")

    def test_init(self):
        self.assertEqual(self.subject.name, "test")
        p = self.getSubjectClass()()
        self.assertTrue(p.name.startswith("Protocol"))

    def test_steps(self):
        self.assertEqual(
            len(self.subject.steps), 0
        )

    def test_independence(self):
        ca = ContextA()
        cb = ContextA()
        a = ProtocolA("a")
        b = ProtocolA("b")
        a.start(ca)
        self.assertTrue(all([s.is_finished for s in a.steps]))
        self.assertTrue(all([not s.is_finished for s in b.steps]))
        self.assertEqual(ca.value, 1)
        self.assertEqual(cb.value, 0)
        b.start(cb)
        self.assertTrue(all([s.is_finished for s in b.steps]))
        self.assertEqual(ca.value, 1)
        self.assertEqual(cb.value, 1)
        a.start(cb)
        self.assertEqual(ca.value, 1)
        self.assertEqual(cb.value, 2)

    def test_is_finished(self):
        c = ContextA()
        p = ProtocolA("a")
        self.assertTrue(all([not s.is_finished for s in p.steps]))
        self.assertFalse(p.is_finished)
        p.start(c)
        self.assertTrue(all([s.is_finished for s in p.steps]))
        self.assertTrue(p.is_finished)

    def test_weight(self):
        c = ContextA()
        p = ProtocolB("b")
        self.assertFalse(p.is_finished)
        p.start(c)
        self.assertTrue(p.is_finished)
        self.assertEqual(c.value, 2)
        c = ContextA()
        p = ProtocolC2("c2")
        p.start(c)
        self.assertEqual(c.value, 1)

    def test_inheritance(self):
        p = ProtocolB("b")
        self.assertEqual(len(p.steps), 2)
        p = ProtocolC("c")
        self.assertEqual(len(p.steps), 1)
        p = ProtocolC2("c2")
        self.assertEqual(len(p.steps), 2)
        p = ProtocolD("d")
        self.assertEqual(len(p.steps), 3)

    def test_nesting(self):
        c = ContextA()
        e = ProtocolE("e")
        self.assertEqual(c.value, 0)
        e.start(c)
        self.assertEqual(c.value, 2)


class TestProtocol(TestBaseProtocol):

    _subject_cls = Protocol

    def test_steps(self):
        self.assertEqual(
            len(self.subject.steps), 3
        )
