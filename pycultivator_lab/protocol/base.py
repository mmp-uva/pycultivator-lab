
from pycultivator.core.objects.configurable import ConfigurableMeta
from pycultivator_lab.task import BaseTask
from step import ProtocolStep

from itertools import count as _count


class MetaProtocol(ConfigurableMeta):

    @staticmethod
    def __new__(mcs, name, bases=None, attrs=None):
        return super(MetaProtocol, mcs).__new__(mcs, name, bases, attrs)

    def __init__(cls, name, bases=None, attrs=None):
        protocol_steps = {}
        if bases is not None:
            for base in bases:
                if isinstance(base, MetaProtocol):
                    protocol_steps.update(base.protocol_steps)
        for k, v in attrs.items():
            # when a step is added as attribute
            if isinstance(v, ProtocolStep):
                protocol_steps[k] = v
                v._name = k
            # add to protocol steps if is a protocol step function
            elif hasattr(v, "_protocol_step"):
                protocol_steps[k] = getattr(v, "_protocol_step")
            # remove from protocol steps if it is not
            elif v is None and k in protocol_steps.keys():
                protocol_steps.pop(k)
        cls._protocol_steps = protocol_steps
        """:type: dict[str, ProtocolStep]"""
        super(MetaProtocol, cls).__init__(name, bases, attrs)

    @property
    def protocol_steps(cls):
        return cls._protocol_steps


_counter = _count().next
_counter()


def _new_name():
    return "Protocol{}".format(_counter())


class BaseProtocol(BaseTask):
    """Base implementation of a protocol

        Protocols execute steps which are methods of the object decorated with the ProtocolStep decorator.
        Steps are executed in order of weight (smallest, including negative first).
        Interdependence between steps can be configured using the `required` and `depedent` attributes.

        - A `required` step will cause the protocol to fail, when the step fails.
        - A `dependent` step will not be executed if a previous required step has failed.

        Steps can be configured using the ProtocolStep decorator::

            @ProtocolStep("name", required=True, dependent=False, weight=10)
            def f(self):
                print("Hello")
        """

    __metaclass__ = MetaProtocol

    def __init__(self, name=None, active=True, weight=0, **kwargs):
        super(BaseProtocol, self).__init__(active=active, weight=weight, **kwargs)
        if name is None:
            name = _new_name()
        self._name = name
        self._steps = []
        import copy
        for step in self._protocol_steps.values():
            self._steps.append(copy.deepcopy(step))

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def steps(self):
        """Reference to all the steps in this protocol

        :rtype: list[pycultivator_lab.protocol.step.ProtocolStep]
        """
        with self._object_lock:
            result = self._steps
        return result

    @property
    def is_finished(self):
        """Whether all steps are executed"""
        with self._object_lock:
            result = all([s.is_finished for s in self.steps])
        return result

    @property
    def progress(self):
        """Percentage of steps executed

        :return: Percentage of steps executed ([0-1])
        :rtype: float
        """
        with self._object_lock:
            result = round(sum([s.is_finished for s in self.steps]) / float(len(self.steps)), 4)
        return result

    def run(self, *args, **kwargs):
        success = True
        sequence = sorted(self.steps, key=lambda x: x.weight)
        """:type: list[ProtocolStep]"""
        for step in sequence:
            result = False
            # execute if we do not dependent on previous results
            # or if all previous results are successful
            if step.is_active and (not step.is_dependent or success):
                # we provide context as first argument for step
                # then we provide the self argument in case the step decorates a function
                result = step.start(self.context, self)
            # register progress
            success = (not step.is_required or result) and success
            # register result
        # print overview of performance
        from pycultivator.core.pcUtility import seconds_to_str
        self.getLog().info(
            "#### Protocol Performance:\n{}".format(
                "\n".join([
                    "\t{:3}. {:36} : {:6} ({})".format(
                        idx, step.uuid, step.is_successful, seconds_to_str(step.runtime)
                    ) for idx, step in enumerate(sequence)
                ])
            )
        )
        return success


class Protocol(BaseProtocol):
    """A Protocol class with 3 basic steps.

    - The `prepare` step is meant for all preparatory work.
    - The `execute` step is meant for the main work (depending on success of prepare)
    - The `clean` step is meant to wrap all loose ends and does not depend on the previous steps.

    Step order is managed using weights where prepare will be executed first (-99),
    then execute (0 weight), then clean (99).

    The protocol executes the steps by calling the private methods first which in turn delegate to the public methods.
    This allows developers to extend the class and overload the public methods without the need to
    reapply the ProtocolStep decorator.

    """

    def prepare(self):
        return True  # overload

    @ProtocolStep("prepare", weight=-99)
    def _prepare(self):
        return self.prepare()

    def execute(self):
        return True  # overload

    @ProtocolStep("execute", weight=0)
    def _execute(self):
        return self.execute()

    def clean(self):
        return True  # overload

    @ProtocolStep("clean", dependent=False, weight=99)
    def _clean(self):
        return self.clean()
