
from pycultivator_lab.task import BaseTask

from itertools import count as _count
import functools
import types

_counter = _count().next
_counter()


def _newname():
    return "ProtocolStep{}".format(_counter())


class ProtocolStep(BaseTask):
    """A single step in a protocol

    Steps allow users to create more complex protocols

    Use as decorator to execute protocol methods as steps ::
        class P(Protocol):

            @ProtocolStep()
            def execute(self):
                print "a"

    Or define step as class ::
        class S1(ProtocolStep):
            def run(self):
                print "a"

        class P(Protocol):
            execute = S1

    Or use the magic attribute to redirect execution ::
        def execute(p):
            print "a"

        class P(Protocol):
            execute = ProtocolStep(magic=execute)
    """

    def __init__(self, name=None, required=True, dependent=True, active=True, weight=0, magic=None, **kwargs):
        super(ProtocolStep, self).__init__(active=active, weight=weight, **kwargs)
        self._magic = magic
        if name is None:
            if magic is not None:
                name = magic.__name__
            else:
                name = _newname()
        self._name = name
        self._required = required is True
        self._dependent = dependent is True

    @property
    def name(self):
        return self._name

    @property
    def is_required(self):
        return self._required

    @property
    def is_dependent(self):
        return self._dependent

    def run(self, *args, **kwargs):
        result = False
        # if not task class -> make task object
        if isinstance(self._magic, (type, types.ClassType)) and issubclass(self._magic, BaseTask):
            self._magic = self._magic()
        # if task object use run
        if isinstance(self._magic, BaseTask):
            result = self._magic.start(self.context)
        # otherwise if function execute function
        elif isinstance(self._magic, types.FunctionType):
            result = self._magic(*args, **kwargs)
        # if any method did not return anything assume it went fine
        if result is None:
            result = True
        return result

    def __call__(self, f=None):
        magic = self
        # when used as decorator we set the magic attribute to point to function
        if f is not None:
            @functools.wraps(f)
            def magic(*args, **kwargs):
                result = f(*args, **kwargs)
                return result
            # reference step class in decorated function
            # allows classes to detect which methods are decorated
            setattr(magic, "_protocol_step", self)
            # store function in this step so it will execute the step
            self._magic = magic
            self._name = f.__name__
        return magic

    def copy(self, memo=None):
        result = super(ProtocolStep, self).copy(memo=memo)
        result._name = self.name
        result._required = self.is_required
        result._dependent = self.is_dependent
        result._magic = self._magic
        return result
