
from dummyProtocol import DummyProtocol
from odProtocol import ODProtocol

__all__ = [
    "DummyProtocol", "ODProtocol"
]


def load_classes():
    return {
        'dummy': DummyProtocol,
        'od': ODProtocol
    }