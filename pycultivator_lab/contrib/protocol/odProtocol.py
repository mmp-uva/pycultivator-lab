
from pycultivator_lab.protocol import Protocol


class ODProtocol(Protocol):
    """Protocol for measuring optical density"""

    def _prepare(self):
        return True

    def _execute(self):
        return True

    def _clean(self):
        return True

