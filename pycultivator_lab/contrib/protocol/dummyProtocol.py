
from pycultivator_lab.protocol import Protocol


class DummyProtocol(Protocol):
    """Protocol for measuring optical density"""

    def prepare(self):
        return True

    def execute(self):
        print(">>> {} says hello!".format(self.name))
        return True

    def clean(self):
        return True

