"""Task Server with threaded Workers"""

from pycultivator.core import PCObject
from base import Server
import threading


class WorkerServer(Server):

    _default_settings = {
        "worker.pool.size": 1
    }

    def __init__(self, settings=None, **kwargs):
        super(WorkerServer, self).__init__(settings=settings, **kwargs)
        self._workers = []

    def getWorkerPoolSize(self):
        """Size of worker pool

        :return:
        :rtype: int
        """
        return self.getSetting("worker.pool.size")

    def setWorkerPoolSize(self, v):
        self.constrain(v, lwr=0, upr=None, name="Worker Pool Size")
        self.setSetting("worker.pool.size", v)
        self.update()

    @property
    def workers(self):
        """Workers used by this server

        :return:
        :rtype: list[pycultivator_lab.server.worker.TaskWorker]
        """
        return self._workers

    def execute(self):
        if not self.shouldStop():
            self.manage_workers()
        self.manage_tasks()
        self.manage_resources()

    def manage_workers(self):
        with self._object_lock:
            while len(self.workers) != self.getWorkerPoolSize():
                if len(self.workers) > self.getWorkerPoolSize():
                    worker = self.workers.pop()
                    self.getLog().info("Pool size too large ({} > {}), remove worker: {}".format(
                        len(self.workers), self.getWorkerPoolSize(), worker.name
                    ))
                    worker.stop()
                if len(self.workers) < self.getWorkerPoolSize():
                    name = "Worker-{}".format(len(self.workers))
                    worker = TaskWorker(name, self)
                    self.getLog().info("Pool size too small ({} < {}), add worker: {}".format(
                        len(self.workers), self.getWorkerPoolSize(), worker.name
                    ))
                    worker.start()
                    self._workers.append(worker)

    def remove_worker(self, worker):
        with self._object_lock:
            if worker in self.workers:
                worker.stop()
                self.workers.remove(worker)

    def clean(self):
        for worker in self.workers:
            worker.stop()

    def stop(self):
        super(WorkerServer, self).stop()

    def terminate(self):
        super(WorkerServer, self).terminate()
        for worker in self.workers:
            worker.terminate()


class TaskWorker(PCObject, threading.Thread):
    """Executes tasks"""

    def __init__(self, name, parent):
        PCObject.__init__(self)
        threading.Thread.__init__(self, name=name)
        self._parent = parent
        self._stop_event = threading.Event()
        self._terminate_event = threading.Event()

    def getParent(self):
        return self._parent

    @property
    def parent(self):
        """The parent of this worker

        :return:
        :rtype: pycultivator_lab.server.worker.WorkerServer
        """
        return self.getParent()

    def run(self):
        self.getLog().info("Task Worker {} says Hello!".format(self.name))
        while self.canRun():
            # obtain task
            self._execute_task()
        self.clean()
        self.getLog().info("Task Worker {} says bye bye!".format(self.name))

    def _execute_task(self):
        # get a task
        task = self.parent.tasks.retrieve()
        if task is not None:
            try:
                task.start()
            except KeyboardInterrupt:
                self.stop()
                raise
            finally:
                self.parent.tasks.finish(task)
        # return to loop

    def clean(self):
        if self.parent is not None:
            self.parent.remove_worker(self)

    def canRun(self):
        return not self.mustStop() and not self.shouldStop()

    def shouldStop(self):
        return self._stop_event.isSet()

    def mustStop(self):
        return self._terminate_event.isSet()

    def stop(self):
        self._stop_event.set()

    def terminate(self):
        self._terminate_event.set()
