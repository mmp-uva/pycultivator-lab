"""The Server Class implements a basic standalone host

* Servers: have a configuration manager to manage experiment configurations

"""

from pycultivator_lab.host import Host
from pycultivator_lab.experiment import ExperimentManager


class Server(Host):
    """A light-weight server"""

    _namespace = "task.server"
    _default_settings = {
        "root.dir": None,
        "config.dir": None,
        "log.file.state": False,
        "log.file.dir": None,
        "data.dir": None,
    }

    def __init__(self, settings=None, **kwargs):
        super(Server, self).__init__(settings=settings, **kwargs)
        self._experiment_manager = ExperimentManager(self)

    def load_experiment(self, location, settings=None, **kwargs):
        self._experiment_manager.load(location, settings=settings, **kwargs)

    def run(self):
        self.getLog().info("Press Ctrl+C to stop the server.")
        return super(Server, self).run()

    def canRun(self):
        """Whether the host should continue running

        * not terminating
        * not stopping
        * if stopping -> still have work

        :rtype: bool
        """
        result = not self.mustStop()
        if result:
            result = not self.shouldStop() or self.hasWork()
        return result
