
from __future__ import print_function
from pycultivator.core import pcLogger
from pycultivator_lab.server.worker import WorkerServer
from pycultivator_lab.task.base import Task
from time import sleep


class HelloWorldTask(Task):

    def run(self, *args, **kwargs):
        print("Hello World!")
        # delay execution
        sleep(1)
        return True


if __name__ == "__main__":
    log = pcLogger.getLogger()
    log.setLevel(10)
    pcLogger.connectStreamHandler(log, level=10)
    log.info("Start WorkerServer Demo")
    server = WorkerServer()
    # update pool size
    server.setWorkerPoolSize(3)
    # add tasks
    for i in range(10):
        server.schedule_task(HelloWorldTask())
    server.start()
