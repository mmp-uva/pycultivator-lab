"""Base Implementation of an Experiment

* Experiments are sequences of protocols.
* Experiment manage the execution of the protocols it contains
* An experiment is executed by an host/manager
* Experiments run protocols

* Experiments export data to a specific destination
* Experiments import data from a specific location

"""

from pycultivator_lab.core.schedule import Schedule
from pycultivator_lab.task import BaseTask, TaskException
from pycultivator_lab.protocol import BaseProtocol


from itertools import count as _count
from datetime import datetime as dt

_counter = _count().next
_counter()


def _new_name():
    return "Experiment{}".format(_counter())


class Experiment(BaseTask):

    _namespace = "experiment"
    _default_settings = {

    }

    def __init__(self, name=None, parent=None, active=True, settings=None, **kwargs):
        super(Experiment, self).__init__(active=active, settings=settings, **kwargs)
        if name is None:
            name = _new_name()
        self._name = name
        self._parent = parent
        self._schedule = Schedule(self)
        from pycultivator_lab.resource import ResourceManager
        self._resources = ResourceManager(self)
        self._protocols = []

    @property
    def name(self):
        with self._object_lock:
            result = self._name
        return result

    @name.setter
    def name(self, name):
        with self._object_lock:
            self._name = name

    @property
    def parent(self):
        with self._object_lock:
            result = self._parent
        return result

    def hasParent(self):
        return self.parent is not None

    @property
    def schedule(self):
        with self._object_lock:
            result = self._schedule
        return result

    @schedule.setter
    def schedule(self, s):
        if not isinstance(s, Schedule):
            raise TypeError("The schedule has to be a Schedule object")
        s.parent = self
        self._schedule = s

    @property
    def is_ready(self):
        with self._object_lock:
            result = self.is_active and not self.is_running
            if result:
                result = self.schedule.should_run()
        return result

    @property
    def protocols(self):
        """All the protocols by this experiment when it is executed

        :return:
        :rtype: list[pycultivator_lab.task.protocol.Protocol]
        """
        with self._object_lock:
            result = self._protocols
        return result

    @property
    def resources(self):
        """ All the resources used by this experiment

        :rtype: pycultivator_lab.resource.manager.ResourceManager
        """
        result = self._resources
        with self._object_lock:
            from pycultivator_lab.host import Host
            if isinstance(self.parent, Host):
                result = self.parent.resources
        return result

    def add(self, protocol, weight=None):
        """Add Protocol"""
        if not isinstance(protocol, BaseProtocol):
            raise TypeError("Expected a protocol instance, not {}".format(type(protocol)))
        if weight is not None:
            protocol.weight = weight
        with self._object_lock:
            self._protocols.append(protocol)

    def remove(self, protocol):
        with self._object_lock:
            if protocol in self._protocols:
                self._protocols.remove(protocol)

    def start(self, context=None, *args, **kwargs):
        if context is None:
            pass
        return super(Experiment, self).start(context=context, *args, **kwargs)

    def run(self, *args, **kwargs):
        """Run the experiment"""
        result = True
        # notify schedule we are executing the experiment
        self.schedule.update()
        sequence = sorted(self.protocols, key=lambda x: x.weight)
        for protocol in sequence:
            protocol.start(context=self.context)
        # inform about performance
        from pycultivator.core.pcUtility import seconds_to_str
        self.getLog().info(
            "#### Experiment Performance:\n{}".format(
                "\n".join([
                    "\t{:3}. {:36} : {:6} ({})".format(
                        idx, protocol.uuid, protocol.is_successful, seconds_to_str(protocol.runtime)
                    ) for idx, protocol in enumerate(sequence)
                ])
            )
        )
        return result


class ExperimentException(TaskException):
    """Exception raised by a Experiment"""

    def __init__(self, msg):
        super(ExperimentException, self).__init__(msg=msg)
