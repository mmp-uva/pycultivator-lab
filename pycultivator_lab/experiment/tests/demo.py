"""Demonstrate experiment execution from the example configuration file"""

from pycultivator.core import pcLogger
from pycultivator_lab.config.xmlConfig import XMLConfig
from experiment import ExperimentManager

import os, sys

SCHEMA_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "..", "config", "schema")
CONFIG_PATH = os.path.join(SCHEMA_DIR, "configuration.xml")
SCHEMA_PATH = os.path.join(SCHEMA_DIR, "pycultivator_lab.xsd")


def inform(msg, value, end="\n"):
    sys.stdout.write("{:40s}{:<20s}{}".format(msg, str(value), end))
    sys.stdout.flush()


if __name__ == "__main__":
    log = pcLogger.getLogger()
    log.level = pcLogger.levels("info")
    pcLogger.connectStreamHandler(log, level=pcLogger.levels("info"))

    log.info("Loading configuration.....")
    manager = ExperimentManager()
    name = manager.load(CONFIG_PATH)
    if name is not None:
        e = manager.get(name)
        inform("Experiment Name:", e.name)
        inform("Experiment State:", e.is_active)
        # info on schedule
        inform("Experiment last run:", e.schedule.last_run)
        inform("Experiment next run:", e.schedule.next_run)
        # info on protocols
        inform("Number of Protocols:", len(e.protocols))
        # info on resources
        inform("Number of Resources:", e.resources.size)
        # execute experiment
        result = e.start()
        inform("Experiment result:", result)
        inform("Experiment last run:", e.schedule.last_run)
    else:
        print("Unable to load config!!")
        print(XMLConfig.assert_config(CONFIG_PATH))
