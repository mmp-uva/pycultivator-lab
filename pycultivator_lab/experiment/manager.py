"""Manages Experiments

* Watches a folder to load configuration files
* Loads Experiment from configuration file
* Executes Experiment on time

"""

from pycultivator_lab.core.manager import NamedManager
from base import Experiment


class ExperimentManager(NamedManager):

    _namespace = "experiment.manager"
    _default_settings = {
        'configuration.location': None
    }

    def __init__(self, parent=None, settings=None, **kwargs):
        super(ExperimentManager, self).__init__(parent=parent, settings=settings, **kwargs)

    def load(self, location, settings=None, **kwargs):
        """Load an experiment from the configuration source into the manager

        :param location:
        :param settings:
        :param kwargs:
        :return:
        """
        result = None
        from pycultivator.config import load_config
        config = load_config(location, settings=settings, **kwargs)
        if config is not None:
            xce = config.getHelperFor("experiment")
            experiment = xce.load(parent=self.parent)
            """:type: pycultivator_lab.experiment.base.Experiment"""
            if experiment is not None:
                result = self.add(experiment)
        return result

    def add(self, experiment):
        """Add an experiment to the manager

        >>> m = ExperimentManager()
        >>> e = Experiment(1, None)

        >>> m.add(e)     #doctest: +ELLIPSIS
        1
        >>> e in m._items.values()
        True

        :param experiment: Resource to register
        :type experiment: pycultivator_lab.experiment.base.Experiment
        :return: The resource that was added
        :rtype: pycultivator_lab.resource.base.Resource
        """
        # register
        self._object_lock.acquire()
        if experiment.name not in self._items.keys():
            self._items[experiment.name] = experiment
        self._object_lock.release()
        return experiment.name

    def has(self, item):
        """Whether this manager has the name or item

        >>> m = ExperimentManager()
        >>> e = Experiment(1, None)

        >>> m.add(e)
        1
        >>> m.has(e)
        True
        >>> m.has(1)
        True

        :param item: Name or item to search for
        :return:
        """
        with self._object_lock:
            if isinstance(item, Experiment):
                result = item in self._items.values()
            else:
                result = item in self._items.keys()
        return result
