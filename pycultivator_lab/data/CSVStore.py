# coding=utf-8

"""Module providing data access interface to CSV Files"""

import DataStore
import csv
import os

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class CSVDataStore(DataStore.DataStore):
    """Implements a CSV DataStore"""

    _default_settings = DataStore.DataStore.mergeDefaultSettings({
        "csv.delimiter": ",",
        "csv.separator": ".",
    })

    def __init__(self, location, settings=None, **kwargs):
        DataStore.DataStore.__init__(self, location, settings=settings, **kwargs)
        self._fh = None
        self._reader = None
        self._writer = None

    # Getters and setters

    def getDelimiter(self):
        return self.getSetting("csv.delimiter")

    def getDecimalSeparator(self):
        return self.getSetting("csv.separator")

    def hasFileHandler(self):
        return self.getFileHandler() is not None

    def getFileHandler(self):
        """Returns the file Handler of the open csv file

        :return: the file handler of the csv file
        :rtype: None or file
        """
        return self._fh

    def _setFileHandler(self, fh):
        self._fh = fh
        return self.getFileHandler()

    def _getReader(self):
        """Returns a CSV Reader instance

        :rtype: csv.DictReader
        """
        if self._reader is None:
            self._reader = csv.DictReader(self.getFileHandler(), delimiter=self.getDelimiter())
        return self._reader

    def _clearReader(self):
        self._reader = None

    def _resetReader(self):
        self._clearReader()
        return self._getReader()

    def _getWriter(self):
        """

        :return:
        :rtype: csv.DictWriter
        """
        if self._writer is None:
            self._writer = csv.DictWriter(self.getFileHandler(), delimiter=self.getDelimiter())
        return self._writer

    def _clearWriter(self):
        self._writer = None

    def _resetWriter(self):
        self._clearWriter()
        return self._getWriter()

    # Behaviour

    @classmethod
    def exists(cls, location):
        """ Returns whether there is a datastore at the given location

        :param location: The location to look at
        :type location: str or file
        :return: Whether there already exists a DataStore at the location
        :rtype: bool
        """
        result = isinstance(location, file)
        if isinstance(location, str):
            result = os.path.exists(location)
        return result

    @classmethod
    def canOpen(cls, location):
        """ Determines whether this

        :param location: The location of the data source
        :type location: str | file
        :return:
        :rtype: bool
        """
        result = False
        if isinstance(location, str):
            result = cls._canOpen(location)
        if isinstance(location, file):
            result = "+" in location.mode and "b" in location.mode and (
                "w" in location.mode or "r" in location.mode or "a" in location.mode
            )
        return result

    @classmethod
    def _canOpen(cls, location):
        # check if directory exists
        result = os.path.exists(os.path.dirname(location))
        # if location exists, make sure it is a file
        if os.path.exists(location):
            result = os.path.isfile(location)
        # the location name has to end with .csv
        result = result and os.path.splitext(location)[1].lower() == ".csv"
        return result

    def isConnected(self):
        """Returns whether the datastore is connected to the data source

        Returns if the DataStore has a FileHandler and if the intern connection flag is True

        :rtype: bool
        """
        return self.hasFileHandler() and super(CSVDataStore, self).isConnected()

    def _close(self):
        self._setLocation(None)
        self.getFileHandler().close()
        self._setFileHandler(None)
        self._setConnected(False)
        return not self.isConnected()

    def canStore(self, record_class):
        """Returns if this DataStore can store the record_class

        This depends on whether the CSV file exists already and if the scheme matches with the given scheme.

        :type record_class: pycultivator.data.DataModel.Record
        :rtype: bool
        """
        return self.hasRecordSchema(record_class, allow_empty=True)

    def hasRecordSchema(self, record, allow_empty=False):
        """Determines if Scheme of the record is defined in the DataStore.

        :type record: pycultivator.data.DataModel.Record
        :param record: The record (class) from which the scheme comes.
        :type allow_empty: bool
        :param allow_empty: Whether to treat an empty scheme as failure.
        :rtype: bool
        """
        result = False
        if self.isConnected():
            # collect variable names from record
            variables = record.getVariables()
            # collect variable names from csv
            fields = []
            if self._getReader().line_num > 0:
                fields = self._getReader().fieldnames
            # success: if no fields have been declared (i.e. file is empty)
            result = len(fields) == 0
            if allow_empty or not result:
                # set result to True if we have at least 1 variable
                result = len(variables) > 0
                # check if each variable name is in the csv scheme
                for variable in variables:
                    result = variable in fields and result
        self._clearReader()
        return result

    def createRecordSchema(self, record):
        """Creates the scheme for the record in the DataStore

        Note some DataStore cannot handle multiple scheme's or do not allow their schemes to update.
        These will return False.

        :type record: pycultivator.data.DataModel.Record
        :param record: The record (class) that has the scheme to be written.
        :rtype: bool
        """
        result = False
        # only create if connected and if no data is present
        if self.isConnected() and self.isEmpty():
            # collect variable names
            variables = record.getVariables()
            # create writer object
            writer = csv.DictWriter(self.getFileHandler(), fieldnames=variables)
            # write header
            writer.writeheader()
        return result

    def isEmpty(self):
        """Returns whether the opened file is empty

        :rtype: bool
        """
        return self.isConnected() and self._getReader().line_num == 0

    def _open(self):
        try:
            if isinstance(self.getLocation(), str):
                location = open(self.getLocation(), "rb+")
            self._setFileHandler(self.getLocation())
            self._setConnected(True)
        except IOError:
            self.getLog().warning("Unable to open {}".format(self.getLocation()))
        return self.isConnected()

    def _write(self, o):
        """Write the record to the CSV"""


    def _read(self, criteria=None):
        pass



