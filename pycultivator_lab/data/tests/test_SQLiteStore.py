"""
Module to test the DataStore module
"""

from pycultivator.data import SQLiteStore, DataModel
from pycultivator.data.tests.test_SQLStore import TestSQLStore
from pycultivator_lab.data import DataModel

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TestSQLiteStore(TestSQLStore):

    _location = ":memory:"

    _record_cls = DataModel.TimeRecord
    _subject_cls = SQLiteStore.SQLiteStore
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """

        :return:
        :rtype: pycultivator.data.SQLiteStore.SQLiteStore
        """
        return super(TestSQLiteStore, cls).getSubjectClass()

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator.data.SQLiteStore.SQLiteStore
        """
        return super(TestSQLiteStore, self).getSubject()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass().create(self.getLocation(), settings={"storage.type": ":memory:"})

    def test_buildWhereFromCriteria(self):
        query, values = self.getSubjectClass()._buildWhereFromCriteria("")
        self.assertEqual(query, "")
        self.assertEqual(values, {})
        criteria = [("a", "=", "b"), ("c", "=", 1)]
        query, values = self.getSubjectClass()._buildWhereFromCriteria("", criteria)
        self.assertEqual(query, " WHERE a = :c_a AND c = :c_c")
        self.assertEqual(values, {"c_a": "b", "c_c": 1})

    def test_canStore(self):
        pass

    def test_read(self):
        record = self.getRecord()
        self.assertTrue(self.getSubject().write(record))
        # now read
        criteria = self.getSubject()._buildCriteriaList(time_record_id=1)
        results = self.getSubject().read(self.getRecordClass(), criteria)
        self.assertTrue(len(results) == 1)
        self.assertEqual(results[0], record)

    def test_write(self):
        record = self.getRecord()
        self.assertTrue(self.getSubject().write(record))
        # get table name
        table = self.getSubject()._parseRecordNameToSQL(record.getName())
        # create criteria
        criteria = self.getSubject()._buildCriteriaList(time_record_id=1)
        # create select query
        query, values = self.getSubject()._buildSelect(table, criteria=criteria)
        records = self.getSubject().getCursor().execute(query, values).fetchall()
        self.assertEqual(len(records), 1)
        self.assertEqual(
            self.getSubjectClass()._parseSQLToValue(records[0]["time_point"], "datetime"),
            record.getTimePoint()
        )

    def test_writeAll(self):
        records = [self.getRecord() for i in range(5)]
        self.assertEqual(self.getSubject().writeAll(records), 5)
        # now read all records
        results = self.getSubject().read(self.getRecordClass())
        # check if we got 5 back
        self.assertEqual(len(results), 5)
