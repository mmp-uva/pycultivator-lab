"""
Module to test the DataStore module
"""

from pycultivator.data import SQLStore, DataModel
from pycultivator.data.tests.test_DataStore import SubjectTestDataStore
from pycultivator_lab.data import DataModel

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TestSQLStore(SubjectTestDataStore):

    _location = ":memory:"

    _record_cls = DataModel.TimeRecord
    _subject_cls = SQLStore.SQLStore
    _abstract = True

    @classmethod
    def getSubjectClass(cls):
        """

        :return:
        :rtype: pycultivator.data.SQLStore.SQLStore
        """
        return super(TestSQLStore, cls).getSubjectClass()

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator.data.SQLStore.SQLStore
        """
        return super(TestSQLStore, self).getSubject()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass().create(self.getLocation(), settings={"storage.type": ":memory:"})

    def setUp(self):
        super(TestSQLStore, self).setUp()
        # initialize database
        self._buildDatabase()

    def _buildDatabase(self):
        """Initializes an empty database with mock data"""
        self.getSubject().getCursor().execute(
            "CREATE TABLE IF NOT EXISTS measurement(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            "od_led INTEGER NOT NULL," +
            "od_value REAL"
            ")"
        )
        self.getSubject().getCursor().execute("INSERT INTO measurement(od_led, od_value) VALUES (720, 0.5)")
        self.getSubject().getCursor().execute("INSERT INTO measurement(od_led, od_value) VALUES (680, 1.0)")
        self.getSubject().getCursor().execute("INSERT INTO measurement(od_led, od_value) VALUES (720, 3.0)")
        self.getSubject().getConnection().commit()

    def test_CamelCaseToUnderScores(self):
        self.assertEqual(self.getSubjectClass().camelToUnder("abc"), "abc")
        self.assertEqual(self.getSubjectClass().camelToUnder("aBc"), "a_bc")
        self.assertEqual(self.getSubjectClass().camelToUnder("Abc"), "abc")
        self.assertEqual(self.getSubjectClass().camelToUnder("abC"), "ab_c")
        self.assertEqual(self.getSubjectClass().camelToUnder("ABigAnimal"), "a_big_animal")
        self.assertEqual(self.getSubjectClass().camelToUnder("ACBigAnimal"), "ac_big_animal")
        self.assertEqual(self.getSubjectClass().camelToUnder("ABigAnimalSTD"), "a_big_animal_std")

    def test_UnderscoreToCamelCase(self):
        self.assertEqual(self.getSubjectClass().underToCamel("abc"), "Abc")
        self.assertEqual(self.getSubjectClass().underToCamel("a_bc"), "ABc")
        self.assertEqual(self.getSubjectClass().underToCamel("ab_c"), "AbC")
        self.assertEqual(self.getSubjectClass().underToCamel("a_big_animal"), "ABigAnimal")
        self.assertEqual(self.getSubjectClass().underToCamel("ac_big_animal"), "AcBigAnimal")
        self.assertEqual(self.getSubjectClass().underToCamel("a_big_animal_std"), "ABigAnimalStd")

    def test_parseNameToSQL(self):
        self.assertEqual(self.getSubjectClass()._parseNameToSQL("a.b"), "a_b")
        self.assertEqual(self.getSubjectClass()._parseNameToSQL("ab."), "ab_")
        self.assertEqual(self.getSubjectClass()._parseNameToSQL("a_b"), "a_b")

    def test_parseSQLToName(self):
        self.assertEqual(self.getSubjectClass()._parseSQLToName("a_b"), "a.b")
        self.assertEqual(self.getSubjectClass()._parseSQLToName("ab_"), "ab.")
        self.assertEqual(self.getSubjectClass()._parseSQLToName("a.b"), "a.b")

    def test_parseRecordNameToSQL(self):
        table = self.getSubjectClass()._parseRecordNameToSQL(DataModel.TimeRecord.getName())
        self.assertEqual(table, "time_record")
        table = self.getSubjectClass()._parseRecordNameToSQL(DataModel.Record.getName())
        self.assertEqual(table, "record")
        table = self.getSubjectClass()._parseRecordNameToSQL(DataModel.ODMeasurement.getName())
        self.assertEqual(table, "od_measurement")

    def test_parseSQLToRecordName(self):
        name = self.getSubjectClass()._parseSQLToRecordName("time_record")
        self.assertEqual(name.lower(), DataModel.TimeRecord.getName().lower())
        self.assertEqual(name, DataModel.TimeRecord.getName())
        name = self.getSubjectClass()._parseSQLToRecordName("record")
        self.assertEqual(name.lower(), DataModel.Record.getName().lower())
        self.assertEqual(name, DataModel.Record.getName())
        name = self.getSubjectClass()._parseSQLToRecordName("od_measurement")
        self.assertEqual(name.lower(), DataModel.ODMeasurement.getName().lower())
        self.assertEqual(name, DataModel.ODMeasurement.getName())

    def test_parseTypeToSQL(self):
        self.assertEqual(self.getSubjectClass()._parseTypeToSQL(int), "INTEGER")
        self.assertEqual(self.getSubjectClass()._parseTypeToSQL(str), "TEXT")
        self.assertEqual(self.getSubjectClass()._parseTypeToSQL(float), "REAL")
        with self.assertRaises(SQLStore.SQLStoreException):
            self.getSubjectClass()._parseTypeToSQL(None)
        with self.assertRaises(SQLStore.SQLStoreException):
            self.getSubjectClass()._parseTypeToSQL(self.getRecordClass())

    def test_parseValueToSQL(self):
        self.assertEqual(self.getSubjectClass()._parseValueToSQL(True), 1)
        self.assertEqual(self.getSubjectClass()._parseValueToSQL(False), 0)
        self.assertEqual(self.getSubjectClass()._parseValueToSQL("a"), "a")
        self.assertEqual(self.getSubjectClass()._parseValueToSQL(1), 1)
        self.assertEqual(self.getSubjectClass()._parseValueToSQL(1.0), 1.0)

    def test_buildCriteriaList(self):
        criteria = {"a": "b", "c": 1}
        self.assertEqual(
            self.getSubjectClass()._buildCriteriaList(**criteria), [("a", "=", "b"), ("c", "=", 1)]
        )
        criteria = {"a": "b", "c": 1}
        self.assertEqual(
            self.getSubjectClass()._buildCriteriaList(">", **criteria), [("a", ">", "b"), ("c", ">", 1)]
        )
        operators = {"a": "=", "c": ">="}
        self.assertEqual(
            self.getSubjectClass()._buildCriteriaList(operators, **criteria), [("a", "=", "b"), ("c", ">=", 1)]
        )
        operators = {"a": "=", "d": ">="}
        self.assertEqual(
            self.getSubjectClass()._buildCriteriaList(operators, **criteria), [("a", "=", "b")]
        )

    def test_buildWhereFromCriteria(self):
        raise NotImplementedError

    def test_buildDatabase(self):
        self.assertTrue("measurement" in self.getSubject()._getTables())

    def test_hasRecordSchema(self):
        self.assertFalse(self.getSubject().hasRecordSchema(self.getRecordClass()))
        self.assertFalse(self.getSubject()._hasTable("time_record"))

    def test_createRecordSchema(self):
        self.assertFalse(self.getSubject()._hasTable("time_record"))
        self.assertTrue(self.getSubject().createRecordSchema(self.getRecordClass()))
        self.assertTrue(self.getSubject().hasRecordSchema(self.getRecordClass(), False))
        self.assertTrue(self.getSubject()._hasTable("time_record"))

    def test_canStore(self):
        self.assertTrue(self.getSubject().canStore(None))
        self.assertTrue(self.getSubject().canStore(self.getRecordClass()))

    def test_read(self):
        raise NotImplementedError

    def test_write(self):
        raise NotImplementedError

    def test_hasTable(self):
        self.assertTrue(self.getSubject()._hasTable("measurement"))
        self.assertFalse(self.getSubject()._hasTable("time_record"))
