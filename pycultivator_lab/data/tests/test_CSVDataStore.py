"""
Module to test the CSVDataStore module
"""

import os

from pycultivator.data import CSVStore, DataModel
from pycultivator.data.tests.test_DataStore import SubjectTestDataStore
from pycultivator_lab.data import DataModel

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TestCSVDataStore(SubjectTestDataStore):

    _subject_cls = CSVStore.CSVDataStore
    _abstract = False

    def __init__(self, methodName='runTest'):
        super(TestCSVDataStore, self).__init__(methodName=methodName)
        self._tmp_file = None

    def getLocation(self):
        return self._tmp_file

    def setUp(self):
        self._tmp_file = os.tmpfile()
        result = super(TestCSVDataStore, self).setUp()
        return result

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator.data.CSVStore.CSVDataStore
        """
        return super(TestCSVDataStore, self).getSubject()

    def test_canStore(self):
        self.assertTrue(self.getSubject().isConnected())
        self.assertTrue(self.getSubject().canStore(DataModel.TimeRecord))

    def test_read(self):
        pass

    def test_write(self):
        pass
