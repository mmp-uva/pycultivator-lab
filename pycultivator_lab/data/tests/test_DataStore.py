"""
Module to test the DataStore module
"""

from pycultivator.data import DataStore, DataModel
from pycultivator.foundation.tests._test import UvaSubjectTestCase
from pycultivator_lab.data import DataModel

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class SubjectTestDataStore(UvaSubjectTestCase):

    _location = ":memory:"

    _record_cls = DataModel.TimeRecord
    _subject_cls = DataStore.DataStore
    _abstract = True

    def __init__(self, methodName='runTest'):
        super(SubjectTestDataStore, self).__init__(methodName=methodName)
        self._record = None

    def getLocation(self):
        """Returns the l"""
        return self._location

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator.data.DataStore.DataStore
        """
        return super(SubjectTestDataStore, self).getSubject()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass().create(self.getLocation())

    def tearDown(self):
        if self.getSubject().isConnected():
            self.getSubject().close()
        self.clearRecord()
        super(SubjectTestDataStore, self).tearDown()

    @classmethod
    def getRecordClass(cls):
        return cls._record_cls

    def initRecord(self, *args, **kwargs):
        return self.getRecordClass()(*args, **kwargs)

    def getRecord(self):
        """Returns a initialized record object for testing

        :return:
        :rtype: pycultivator.data.DataModel.TimeRecord
        """
        return self._record

    def setRecord(self, record):
        self._record = record
        return self.getRecord()

    def clearRecord(self):
        self.setRecord(None)

    def setUp(self):
        result = super(SubjectTestDataStore, self).setUp()
        self.setRecord(self.initRecord())
        return result

    def test_canOpen(self):
        self.assertTrue(self.getSubjectClass().canOpen(self.getLocation()))
        self.assertFalse(self.getSubjectClass().canOpen("notafile.txt"))
        self.assertFalse(self.getSubjectClass().canOpen("notafile.csv"))

    def test_create(self):
        subject = self.getSubjectClass().create(self.getLocation())
        self.assertIsInstance(subject, DataStore.DataStore)
        self.assertTrue(subject.isConnected())
        subject.close()
        subject = self.getSubjectClass().create("notafile.txt")
        self.assertIsNone(subject, DataStore.DataStore)

    def test_open(self):
        self.assertTrue(self.getSubject().isConnected())
        self.assertFalse(self.getSubject().open())
        self.assertFalse(self.getSubject().open("notafile.txt"))
        self.assertTrue(self.getSubject().close())
        self.assertFalse(self.getSubject().open("notafile.txt"))

    def test_isConnected(self):
        self.assertTrue(self.getSubject().isConnected())
        self.assertTrue(self.getSubject().close())
        self.assertFalse(self.getSubject().isConnected())

    def test_close(self):
        self.assertTrue(self.getSubject().isConnected())
        self.assertTrue(self.getSubject().close())
        self.assertFalse(self.getSubject().isConnected())
        self.assertFalse(self.getSubject().close())

    def test_canStore(self):
        pass

    def test_read(self):
        pass

    def test_hasRecord(self):
        self.assertFalse(self.getSubject().hasRecord(self.getRecord()))
        self.assertFalse(None)

    def test_write(self):
        pass
