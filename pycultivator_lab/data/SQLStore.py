# coding=utf-8

"""Module providing data access interface to SQL Databases"""

from abc import abstractmethod
from datetime import datetime as dt

import DataStore
from pycultivator.data import DataModel

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class SQLStore(DataStore.DataStore):
    """ABSTRACT Class for working with SQL Databases"""

    PYTHON_TO_SQL_TYPES = {
        str: "TEXT",
        int: "INTEGER",
        float: "REAL",
        dt: "TIMESTAMP",
        bool: "INTEGER",
    }

    SQL_TO_PYTHON_TYPES = {
        "INTEGER": int,
        "TEXT": str,
        "REAL": float,
        "TIMESTAMP": dt,
    }

    @staticmethod
    def dotsToUnder(name):
        """Replaces dots in a name with underscores

        :param name: Name to convert
        :type name: str
        :return: New name where '.' -> '_'
        :rtype: str
        """
        return name.replace(".", "_")

    @staticmethod
    def underToDots(name):
        """Replaces underscores with dots

        :param name: Name to convert
        :type name: str
        :return: New name where '_' -> '.'
        :rtype: str
        """
        return name.replace("_", ".")

    @staticmethod
    def camelToUnder(name):
        """Converts CamelCase to underscores: BigAnimal -> big_animal

        :param name: The name to convert
        :type name: str
        :rtype: str
        """
        result = ""
        for idx in range(len(name)):
            pl = None if idx == 0 else name[idx-1]  # previous letter
            cl = name[idx]  # current letter
            nl = None if idx + 1 == len(name) else name[idx + 1]  # next letter
            if cl.isupper() and idx > 0 and (
                        (pl is not None and pl.islower()) or
                        (nl is not None and nl.islower())
            ):
                result += "_"
            result += cl.lower()
        return result

    @staticmethod
    def underToCamel(name):
        """Converts a name with underscores to CamelCase: big_animal -> BigAnimal

        CAUTION: Is not able to convert from od_measurement to ODMeasurement
        Best to use this along with `.tolower()` for whole string.
        """
        result = ""
        for idx in range(len(name)):
            pl = None if idx == 0 else name[idx-1]  # previous letetr
            cl = name[idx]  # current letter
            if idx == 0 or pl == "_":
                cl = cl.upper()
            if cl != "_":
                result += cl
        return result

    @staticmethod
    def _buildWhereFromCriteria(query, criteria=None):
        """Build where statement from a list of criteria.

        :param criteria:
        :type criteria:
        :return:
        :rtype:
        """
        raise NotImplementedError

    # Getters / Setters

    # Behaviour

    @classmethod
    def _canOpen(cls, location):
        """Whether this store can open the location

        In this case: directory exists and extension is '.db'

        :param location: Path to the data source location
        :type location: str
        :return: Whether ths store can open the location
        :rtype: bool
        """
        raise NotImplementedError

    @classmethod
    def _exists(cls, location):
        """Whether the location points to an already existing data source.

        Also checks if the location can be open (`canOpen`)

        :param location:
        :type location:
        :return: Whether the location to an already existing data source
        :rtype: bool
        """
        raise NotImplementedError

    @abstractmethod
    def _open(self):
        """Internal command for creating a connection to the data source"""
        raise NotImplementedError

    @abstractmethod
    def _close(self):
        raise NotImplementedError

    def canStore(self, record):
        """Returns whether this record (class) can be stored by this store.

        This is determined by:
        - schema is not defined in store: canStore == True
        - schema is defined and all variables in class are in store + type matches: canStore == True

        :type record: pycultivator.data.DataModel.Record
        :rtype: bool
        """
        result = isinstance(record, DataModel.Record)
        if result:
            result = not self.hasRecordSchema(record)  # if no schema is defined yet: return True
            if not result:  # schema is already defined, check schema
                result = True
                record_schema = record.getSchema()
                store_schema = self.getRecordSchema(record)
                for name in record_schema.getFieldNames():
                    result = store_schema.hasField(name) and \
                             self._compareTypes(record_schema.getType(name), store_schema.getType(name)) and result
        return result

    def hasRecordSchema(self, record, allow_empty=False):
        table = self._parseRecordNameToSQL(record.getName())
        return self._hasTable(table, allow_empty=allow_empty)

    @abstractmethod
    def getRecordSchema(self, record):
        """Returns the schema for the record as defined in this DataStore

        :type record: pycultivator.data.DataModel.Record
        :rtype: pycultivator.data.DataModel.Schema
        """
        raise NotImplementedError

    @abstractmethod
    def createRecordSchema(self, record):
        raise NotImplementedError

    @abstractmethod
    def _read(self, template, criteria=None):
        raise NotImplementedError

    @abstractmethod
    def _write(self, record):
        raise NotImplementedError

    # Helpers

    @abstractmethod
    def _getTables(self):
        raise NotImplementedError

    def _hasTable(self, table, allow_empty=False):
        if not self.isConnected():
            raise SQLStoreException("Not connected to Database")
        # get tables and change to lowercase
        # tables = [ table.lower() for table in self._getSQLTables()]
        tables = self._getTables()
        # return existence or if empty is allowed and there were no tables
        result = table in tables or (allow_empty and len(tables) == 0)
        return result

    @classmethod
    def _buildCriteriaList(cls, operator="=", **criteria):
        """Creates a list of criteria: tuples of name, operator, value

        :param operator: Either one operator to use for all criteria or a dictionary of operators for each variable
        :type operator: str or dict
        :param criteria: A set of key=value pairs to put in criteria list
        :type criteria: dict
        :return: A list with tuples [name, operator, value]
        :rtype: list[tuple[str, str, str | float | int]
        """
        result = []
        if isinstance(operator, str):
            operator = {key: operator for key in criteria.keys()}
        for key in criteria.keys():
            opr = operator.get(key)
            val = criteria[key]
            if opr is not None:
                result.append((key, opr, val))
            else:
                cls.getLog().warning("Could not find operator for {}".format(key))
        return result

    @abstractmethod
    def _buildSelect(self, table, variables=None, criteria=None):
        raise NotImplementedError

    def _buildInsertFromRecord(self, record):
        return self._buildInsert(
            self._parseRecordNameToSQL(record.getName()), **record.getValues()
        )

    @abstractmethod
    def _buildInsert(self, table, **attributes):
        raise NotImplementedError

    def _buildUpdateFromRecord(self, record, criteria=None):
        """Creates a UPDATE query from a record"""
        return self._buildUpdate(
            record.getName(), criteria=criteria, **record.getValues()
        )

    @abstractmethod
    def _buildUpdate(self, table, criteria=None, **attributes):
        raise NotImplementedError

    def _buildCreateTableFromRecord(self, record, add_index=True):
        """ Builds a CREATE TABLE query from a record (class)

        :param record:
        :type record: pycultivator.data.DataModel.Record
        :return: Query string
        :rtype: str
        """
        table = self._parseRecordNameToSQL(record.getName())
        return self._buildCreateTable(table, record.getSchema(), add_index=add_index)

    @abstractmethod
    def _buildCreateTable(self, table, schema, add_index=True):
        """Creates a CREATE TABLE query

        :param table: Name of the table to create
        :type table: str
        :param schema: Schema to use when creating the table
        :type schema: pycultivator.data.DataModel.Schema
        :param add_index: Whether to add a primary key named {table}_id
        :type add_index: bool
        :return: Query to execute
        :rtype: str
        """
        raise NotImplementedError

    @abstractmethod
    def _buildSchema(self, table):
        """Returns a query that returns the tabel schema"""
        raise NotImplementedError

    @classmethod
    def _parseNameToSQL(cls, from_name):
        return cls.dotsToUnder(from_name)

    @classmethod
    def _parseSQLToName(cls, sql_name):
        return cls.underToDots(str(sql_name))

    @classmethod
    def _parseRecordNameToSQL(cls, from_name):
        return cls.camelToUnder(from_name)

    @classmethod
    def _parseSQLToRecordName(cls, sql_name):
        return cls.underToCamel(sql_name)

    @classmethod
    def _parseTableToRecordName(cls, table):
        return cls.underToCamel(table)

    @classmethod
    def _parseTypeToSQL(cls, from_type):
        """Parses a python type to a SQL type name"""
        if from_type not in cls.PYTHON_TO_SQL_TYPES.keys():
            raise SQLStoreException("Unknown python type - {}".format(from_type))
        return cls.PYTHON_TO_SQL_TYPES[from_type]

    @classmethod
    def _parseSQLToType(cls, sql_type):
        """Parses a SQL type name to a python type"""
        result = None
        if sql_type not in cls.SQL_TO_PYTHON_TYPES.keys():
            raise SQLStoreException("Unknown SQL type - {}".format(sql_type))
        return cls.SQL_TO_PYTHON_TYPES[sql_type]

    @classmethod
    def _parseValueToSQL(cls, value):
        """Parses a value to an SQL-compatible value"""
        result = value
        if isinstance(value, bool):
            result = 1 if value else 0
        # sqlite can automatically convert datetime formats
        # if isinstance(value, dt):
        #     result = value.strftime(self.DT_FORMAT)
        return result

    @classmethod
    def _parseSQLToValue(self, sql_value, to_type, **kwargs):
        result = sql_value
        if sql_value is not None:
            if to_type in [bool, "bool"]:
                result = sql_value == 1
            if to_type in [dt, "datetime"]:
                fmt = kwargs.get("dt_fmt", self.DT_FORMAT)
                result = dt.strptime(sql_value, fmt)
        return result

    @classmethod
    def _parseSQLToRecord(cls, template, record=None, **properties):
        """Parses a sql record into a pycultivator record

        :param template:
        :type template: pycultivator.data.DataModel.Record
        :param record:
        :type record: None or dict
        :return:
        :rtype:
        """
        if record is None:
            record = {}
        record.update(properties)
        parsed_variables = {}
        # fetch variables from template
        for name in template.getVariables():
            parsed_name = cls._parseNameToSQL(name)
            variable_type = template.getVariableType(name)
            if parsed_name in record.keys():
                value = record[parsed_name]
                parsed_value = cls._parseSQLToValue(value, variable_type)
                if parsed_value is not None:
                    # add to record
                    parsed_variables[name] = parsed_value
                else:
                    cls.getLog().info("Could not read - {} - to {}".format(value, variable_type))
        # now use read of template
        return template.parse(parsed_variables)

    @classmethod
    def _compareTypes(cls, from_type, sql_type):
        """Compares two types, taking SQL transformation problems into account.

        For example: boolean values translate to integers; so we need to be relaxed about that.
        """
        inferred_sql_type = cls._parseSQLToType(cls._parseTypeToSQL(from_type))
        return inferred_sql_type == sql_type

class SQLStoreException(DataStore.DataStoreException):
    """An exception raised by a SQLStore"""

    pass
