# coding=utf-8

"""Module providing data access interface to SQLite Databases"""

import os
import sqlite3
from datetime import datetime as dt

import SQLStore
from pycultivator.data import DataModel

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class SQLiteStore(SQLStore.SQLStore):
    """Class for using a SQLite Database"""

    SQLITE_MEMORY_STORAGE = ":memory:"
    SQLITE_FILE_STORAGE = ":file:"
    SQLITE_STORAGE_DEFAULT = SQLITE_FILE_STORAGE
    SQLITE_STORAGE_TYPES = [SQLITE_MEMORY_STORAGE, SQLITE_FILE_STORAGE]

    _default_settings = SQLStore.SQLStore.mergeDefaultSettings({
        "storage.type": SQLITE_STORAGE_DEFAULT
    })

    @staticmethod
    def _buildWhereFromCriteria(query, criteria=None):
        if criteria is None:
            criteria = []
        if not isinstance(criteria, list):
            raise SQLiteStoreException("Expected a list of criteria, not - {}".format(type(criteria)))
        variables, values = [], {}
        for item in criteria:
            field, operator, value = None, None, None
            if isinstance(item, (tuple, list)):
                field = item[0]
                operator = item[1]
                value = item[2]
            elif isinstance(item, dict):
                if "field" in item.keys():
                    field = item["field"]
                if "operator" in item.keys():
                    operator = item["operator"]
                if "value" in item.keys():
                    value = item["value"]
            # add if we were able to read it
            if None not in [field, operator, value]:
                variables.append("{} {} :c_{}".format(field, operator, field))
                values["c_{}".format(field)] = value
        if len(variables) > 0:
            query = '{} WHERE {}'.format(query, ' AND '.join(variables))
        return query, values

    def __init__(self, location, settings=None, **kwargs):
        SQLStore.SQLStore.__init__(self, location=location, settings=settings, **kwargs)
        self._connection = None

    # Getters / Setters

    def getCursor(self):
        """Returns the cursor of the active connection

        :rtype: None or sqlite3.Cursor
        """
        result = None
        if self.hasConnection():
            result = self.getConnection().cursor()
        return result

    def hasCursor(self):
        return self.getCursor() is not None

    def getStorageType(self):
        return self.getSetting("storage.type")

    def storesToMemory(self):
        return self.getStorageType() == self.SQLITE_MEMORY_STORAGE

    def storesToFile(self):
        return self.getStorageType() == self.SQLITE_FILE_STORAGE

    def getConnection(self):
        """Returns the connection handler

        :rtype: None or sqlite3.Connection
        """
        return self._connection

    def _setConnection(self, connection):
        self._connection = connection
        return self.getConnection()

    def hasConnection(self):
        """Whether the Data Store has a connection (based on the connection variable)

        :rtype: bool
        """
        return self.getConnection() is not None

    def isConnected(self):
        """Whether the Data Store is connected

        :rtype: bool
        """
        return self.hasConnection() and super(SQLiteStore, self).isConnected()

    # Behaviour

    @classmethod
    def _canOpen(cls, location):
        """Whether this store can open the location

        In this case: directory exists and extension is '.db'

        :param location: Path to the data source location
        :type location: str
        :return: Whether ths store can open the location
        :rtype: bool
        """
        return location == cls.SQLITE_MEMORY_STORAGE or (
            os.path.exists(os.path.dirname(location)) and
            os.path.splitext(location)[1] == ".db"
        )

    @classmethod
    def _exists(cls, location):
        """Whether the location points to an already existing data source.

        Also checks if the location can be open (`canOpen`)

        :param location:
        :type location:
        :return: Whether the location to an already existing data source
        :rtype: bool
        """
        return location == cls.SQLITE_MEMORY_STORAGE or (
            cls.canOpen(location) and os.path.exists(location)
        )

    def _open(self):
        """Internal command for creating a connection to the data source"""
        if self.isConnected():
            raise SQLiteStoreException("Already connected to - {}".format(self.getLocation()))
        if self.storesToFile():
            self._setConnection(sqlite3.connect(self.getLocation()))
        else:
            self._setConnection(sqlite3.connect(":memory:"))
        self.getConnection().row_factory = sqlite3.Row
        self._setConnected(True)
        return self.isConnected()

    def _close(self):
        if not self.isConnected():
            raise SQLiteStoreException("Not connected to Database")
        self.getConnection().close()
        self._setConnected(False)
        return not self.isConnected()

    def getRecordSchema(self, record):
        """Returns the schema for the record as defined in this DataStore

        :type record: pycultivator.data.DataModel.Record
        :rtype: pycultivator.data.DataModel.Schema
        """
        result = DataModel.Schema()
        if not self.isConnected():
            raise SQLiteStoreException("Not connected to Database")
        table = self._parseRecordNameToSQL(record.getName())
        query = self._buildSchema(table)
        fields = self.getCursor().execute(query).fetchall()
        for field in fields:
            field_name = dict(field).get("name", None)
            field_type = dict(field).get("type", None)
            field_required = dict(field).get("notnull", None)
            if None not in [field_name, field_type, field_required]:
                result.addField(DataModel.Variable(
                    self._parseSQLToName(field_name), None, self._parseSQLToType(field_type), field_required == 1))
            else:
                self.getLog().info("Unable to extract field information from {}".format(field))
        return result

    def createRecordSchema(self, record):
        if not self.isConnected():
            raise SQLiteStoreException("Not connected to Database")
        query = self._buildCreateTableFromRecord(record, True)
        self.getCursor().execute(query)
        return self.hasRecordSchema(record, False)

    def _read(self, template, criteria=None):
        if not self.isConnected():
            raise SQLiteStoreException("Not connected to Database")
        if not self.hasRecordSchema(template, False):
            raise SQLiteStoreException("Database has no table for {}".format(template.getName))
        results = []
        # get table name
        table = self._parseRecordNameToSQL(template.getName())
        # build query
        query, values = self._buildSelect(table, criteria=criteria)
        records = self.getCursor().execute(query, values).fetchall()
        # read results
        for record in records:
            result = self._parseSQLToRecord(template, dict(record))
            if result is not None:
                results.append(result)
        return results

    def _write(self, record):
        if not self.isConnected():
            raise SQLiteStoreException("Not connected to Database")
        # check if we need to create the schema
        if not self.hasRecordSchema(record, False):
            self.createRecordSchema(record)
        if not self.hasRecordSchema(record, False):
            raise SQLiteStoreException("Unable to create schema for {}".format(record.getName))
        # now we have a schema, check if we do not create duplicate
        if self.hasRecord(record):
            raise SQLiteStoreException("This already exists in the Database")
        # ok everything ready to write, we can auto generate the query
        query, values = self._buildInsertFromRecord(record)
        self.getCursor().execute(query, values)
        self.getConnection().commit()
        # finished, if you get here, you succeeded so return True
        return True

    # Helpers

    def _getTables(self):
        result = []
        if self.hasCursor():
            query = "SELECT name FROM sqlite_master WHERE type='table';"
            records = self.getCursor().execute(query).fetchall()
            for record in records:
                result.append(str(record["name"]))
        return result

    def _buildSelect(self, table, variables=None, criteria=None):
        """Creates a SELECT query from record/template"""
        if variables is None:
            variables = []
        if isinstance(variables, str):
            variables = [variables]
        if not isinstance(variables, list):
            raise SQLiteStoreException("Expected a list of variables, not - {}".format(type(variables)))
        if not self._hasTable(table):
            raise SQLiteStoreException("Table - {} - does not exist".format(table))
        selector = "*"
        if len(variables) > 0:
            selector = ",".join([ self._parseNameToSQL(variable) for variable in variables ])
        query = "SELECT {} FROM {}".format(selector, table)
        return self._buildWhereFromCriteria(query, criteria)

    def _buildInsert(self, table, **attributes):
        """Creates an INSERT INTO query from a record

        :rtype: tuple[str, dict[str, str | float | int | datetime.datetime]
        """
        fields, values = [], {}
        # collect names and read values
        for name in attributes.keys():
            field = self._parseNameToSQL(name)
            fields.append(field)
            values["i_{}".format(field)] = self._parseValueToSQL(attributes[name])
        # build query
        query = "INSERT INTO {}({}) VALUES ({})".format(
            table, ",".join(fields), ",".join([":i_{field}".format(field=field) for field in fields])
        )
        return query, values

    def _buildUpdate(self, table, criteria=None, **attributes):
        """Builds the update query

        :param table: Name of the table to update
        :type table: str
        :param criteria: Criteria to apply (where clause)
        :type criteria: None or list[tuple[str, str, str | int | float]]
        :param attributes: Attributes to update
        :type attributes: dict[str, str | float | int | datetime.datetime]
        :return: The query to execute and the values to insert
        :rtype: tuple[str, dict[str, str | float | int | datetime.datetime]
        """
        fields, values = [], {}
        # collect names and read values
        for name in attributes.keys():
            fields.append(self._parseNameToSQL(name))
            values["u_{}".format(name)] = self._parseValueToSQL(attributes[name])
        # build query
        query = "UPDATE {} SET {}".format(
            table, ",".join(["{field}=:u_{field}".format(field=field) for field in fields])
        )
        return self._buildWhereFromCriteria(query, criteria), values

    def _buildCreateTable(self, table, schema, add_index=True):
        """Creates a CREATE TABLE query

        :rtype: str
        """
        primary_key = "{}_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,".format(table) if add_index else ""
        # create variables
        variables = [
            "{} {}{}".format(
                self._parseNameToSQL(key),
                self._parseTypeToSQL(schema.getType(key)),
                " NOT NULL" if schema.isRequired(key) else ""
            ) for key in schema.getFieldNames()
        ]
        # build query
        return "CREATE TABLE IF NOT EXISTS '{}'({}{})".format(table, primary_key, ",".join(variables))

    def _buildSchema(self, table):
        """Creates a PRAGMA table_info('table')

        :rtype: str
        """
        return "PRAGMA table_info({});".format(table)

    @classmethod
    def _parseSQLToValue(self, sql_value, to_type, **kwargs):
        result = sql_value
        if sql_value is not None:
            if to_type in [bool, "bool"]:
                result = sql_value == 1
            if to_type in [dt, "datetime"]:
                converter = sqlite3.converters.get("timestamp".upper())
                if converter is not None:
                    result = converter(sql_value)
                else:
                    result = dt.strptime(sql_value, self.DT_FORMAT)
        return result

class SQLiteStoreException(SQLStore.SQLStoreException):
    """An exception raised by a SQLiteStore"""
