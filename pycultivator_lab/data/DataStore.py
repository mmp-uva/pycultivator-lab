# coding=utf-8

"""Module providing the basic interface to a data store"""

from abc import abstractmethod

from pycultivator.data import DataModel
from pycultivator.foundation import pcObject, pcException, pcRegistry

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class DataStore(pcObject.ConfigurableObject):
    """ABSTRACT Data Store that manages the read/write to/from a data source.

    A data store should be able to:
    - store data
    - read data

    data is transported by Records from the DataModel module
    """

    def __init__(self, location, settings=None, **kwargs):
        """Initialise the Data Store"""
        pcObject.ConfigurableObject.__init__(self, settings=settings, **kwargs)
        self._location = location
        self._isOpen = False

    def __enter__(self):
        """Return the object, when used in a with statement"""
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Clean the object, when exiting a with statement

        :rtype: bool
        """
        return self.close()

    # Getters / Setters

    def getLocation(self):
        return self._location

    def _setLocation(self, location):
        if self.canOpen(location):
            self._location = location
        return self.getLocation()

    # Behaviour

    @classmethod
    def create(cls, location, autoOpen=True, **kwargs):
        """Creates a DataStore object from a location specification.

        Determines if the location can be opened and if so creates a DataStore object for that location.

        :param location: The location of the data source
        :type location: str
        :param autoOpen: Whether to automatically open the connection
        :type autoOpen: bool
        :param kwargs: Extra arguments passed to the created object
        :rtype: None or pycultivator.data.DataStore.DataStore
        """
        result = None
        # remove scheme
        location = pcRegistry.URLRegistry.stripScheme(location)
        if cls.canOpen(location):
            result = cls(location, **kwargs)
            if autoOpen:
                result.open()
        return result

    @classmethod
    def exists(cls, location):
        """ABSTRACT Returns whether there is a datastore at the given location

        :param location: The location to look at
        :type location: str
        :return: Whether there already exists a DataStore at the location
        :rtype: bool
        """
        location = pcRegistry.URLRegistry.stripScheme(location)
        return cls._exists(location)

    @classmethod
    def _exists(cls, location):
        raise NotImplementedError

    @classmethod
    def canOpen(cls, location):
        """ABSTRACT Returns whether this store can open the given location

        :param location: Location of the data source
        :type location: str
        :return: Whether this location can be loaded
        :rtype: bool
        """
        location = pcRegistry.URLRegistry.stripScheme(location)
        return cls._canOpen(location)

    @classmethod
    def _canOpen(cls, location):
        raise NotImplementedError

    def open(self, location=None):
        """Opens the connection to the data source
        :rtype: None or pycultivator.data.DataStore.DataStore
        """
        result = False
        if not self.isConnected():
            if location is None:
                location = self.getLocation()
            if self.canOpen(location):
                self._setLocation(location)
                result = self._open()
        return result

    @abstractmethod
    def _open(self):
        """INTERNAL ABSTRACT for creating the connection"""
        raise NotImplementedError

    def isConnected(self):
        """Returns whether this store is connected to the source

        :rtype: bool
        """
        return self._isOpen is True

    def _setConnected(self, state=True):
        """Sets the current connection state"""
        self._isOpen = state is True
        return self.isConnected()

    def close(self):
        """ABSTRACT method; closes the data source"""
        result = False
        if self.isConnected():
            result = self._close()
        return result

    @abstractmethod
    def _close(self):
        raise NotImplementedError

    @abstractmethod
    def canStore(self, record):
        """Checks if the data store can store the given record (class)

        Requires the store to be connected

        :rtype: bool
        """
        raise NotImplementedError

    @abstractmethod
    def getRecordSchema(self, record):
        """Returns the Schema of the record as defined in the DataStore.

        Note some DataStore (such CSV) do not store type; thus the type parameter in these stores cannot be trusted.

        :type record: pycultivator.data.DataModel.Record
        :param record: The record (class) from which the schema comes.
        :rtype: pycultivator.data.DataModel.Schema
        """
        raise NotImplementedError

    @abstractmethod
    def hasRecordSchema(self, record, allow_empty=False):
        """Determines if Schema of the record is defined in the DataStore.

        :type record: pycultivator.data.DataModel.Record
        :param record: The record (class) from which the schema comes.
        :type allow_empty: bool
        :param allow_empty: Whether to treat an empty schema as failure.
        :rtype: bool
        """
        raise NotImplementedError

    @abstractmethod
    def createRecordSchema(self, record):
        """Creates the schema for the record in the DataStore

        Note some DataStore cannot handle multiple schemas or do not allow their schemas to update.
        These will return False and may print a warning message to the console.

        :type record: pycultivator.data.DataModel.Record
        :param record: The record (class) that has the schema to be written.
        :rtype: bool
        """
        raise NotImplementedError

    def read(self, template=None, criteria=None):
        """Read data from the data store

        :param template: Record class to read into
        :type template: None or pycultivator.data.DataModel.Record
        :param criteria: A list of criteria
        :type criteria: list[tuple[str, str, object] | dict[str, str | object]]
        :return: A list of all records that match the criteria
        :rtype: list[pycultivator.data.DataModel.Record]
        """
        result = False
        if template is None:
            template = DataModel.Record
        if self.isConnected():
            result = self._read(template=template, criteria=criteria)
        return result

    @abstractmethod
    def _read(self, template, criteria=None):
        """Read data from the data store

        :param template: A template record to read into
        :type template: pycultivator.data.DataModel.Record
        :param criteria: A list of criteria
        :type criteria: list[tuple[str, str, object] | dict[str, str | object]]
        :return: The data read
        :rtype: list[pycultivator.data.DataModel.Record]
        """

    def hasRecord(self, record):
        """Whether a record with the same id already exists

        Current implementation does not worry about duplicates, it will always return False

        :rtype: bool
        """
        return False

    def writeAll(self, records):
        """Writes multiple records to the data store

        :type records: list[pycultivator.data.DataModel.Record]
        :return: Number of records succesfully written to data store
        :rtype: int
        """
        result = 0
        for record in records:
            if record is not None and self.write(record):
                result += 1
        return result

    def write(self, record):
        """Write data to the data store

        :type record: pycultivator.data.DataModel.Record
        :rtype: bool
        """
        result = False
        if self.isConnected() and self.canStore(record):
            result = self._write(record)
        return result

    @abstractmethod
    def _write(self, record):
        """Write data to the data store

        :type o: pycultivator.data.DataModel.Record
        :rtype: bool
        """
        raise NotImplementedError


class DataStoreException(pcException.PCException):
    """An exception raised by a DataStore"""
