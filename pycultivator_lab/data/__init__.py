# coding=utf-8
"""
The pycultivator.data package contains the drivers for data storage systems, like:
databases, text files etc.

It also comes with a basic DataModel defining a standardized way of handling data.

A registry is used to keep track of all available storage types.
"""

import DataStore
from pycultivator.foundation import pcLogger, pcRegistry

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

# connect to logger
pcLogger.createLogger(__name__).debug("Connected {} to logger".format(__name__))

# load registry class
registry = pcRegistry.URLRegistry(
    # pre-fill registry
    {
        "csv": "pycultivator.data.CSVStore.CSVStore",
        "sqlite": "pycultivator.data.SQLiteStore.SQLiteStore",
    },
    DataStore.DataStore
)
