""" A manager for resources

* Keeps track of loaded resources
* Helps with loading resources
* Reloads resources upon change
* Cleans resources when they

"""

from pycultivator.core import PCException
from pycultivator_lab.core.manager import NamedManager
from base import Resource

import threading

__all__ = [
    'ResourceManager', 'ResourceManagerException'
]


class ResourceManager(NamedManager):

    def __init__(self, parent=None, settings=None, **kwargs):
        super(ResourceManager, self).__init__(parent=parent, settings=settings, **kwargs)
        self._lock = threading.RLock()

    def load(self, name, obj, _class=None, **kwargs):
        """Load a resource and add it to the manager

        >>> m = ResourceManager()
        >>> m.load("a", 1)     #doctest: +ELLIPSIS
        <base.Resource object at 0x...>

        >>> "a" in m
        True

        :param name: Name to register loaded resource to
        :type name: str
        :param obj: Object to be loaded as shared resource
        :type obj: object
        :param _class: Optional Resource klass
        :type _class: callable -> pycultivator_lab.resource.base.Resource
        :param kwargs: Extra keyword arguments
        :return: The resource that was added
        :rtype: pycultivator_lab.resource.base.Resource
        """
        if _class is None:
            _class = Resource
        # make into resource
        resource = _class.load(obj, **kwargs)
        return self.add(name, resource)

    def add(self, name, resource):
        """Add a resource to the manager

        >>> m = ResourceManager()
        >>> r = Resource(1)

        >>> m.add("a", r)     #doctest: +ELLIPSIS
        <base.Resource object at 0x...>

        :param name: Name under which to register the resource
        :type name: str
        :param resource: Resource to register
        :type resource: pycultivator_lab.resource.base.Resource
        :return: The resource that was added
        :rtype: pycultivator_lab.resource.base.Resource
        """
        # register
        self._lock.acquire()
        self._items[name] = resource
        self._lock.release()
        return resource

    def clean(self):
        """Remove unused resources

        >>> m = ResourceManager()
        >>> m.load("a", 1)     #doctest: +ELLIPSIS
        <base.Resource object at 0x...>
        >>> m.load("b", 2)     #doctest: +ELLIPSIS
        <base.Resource object at 0x...>

        >>> m.get("a").acquire()
        True

        >>> m.clean()          #doctest: +ELLIPSIS
        [<base.Resource object at 0x...>]
        >>> "a" in m
        True

        >>> "b" in m
        False

        :return: list of resources removed
        :rtype: list[pycultivator_lab.resource.base.Resource]
        """
        results = []
        self._lock.acquire()
        resources = {}
        for name in self._items.keys():
            resource = self._items[name]
            if resource.in_use:
                resources[name] = resource
            else:
                results.append(resource)
        self._items = resources
        self._lock.release()
        return results

    def has(self, name):
        """Whether a resource with the given name has been loaded

        >>> rm = ResourceManager()
        >>> rm.load("a", 1)     #doctest: +ELLIPSIS
        <base.Resource object at 0x...>

        >>> rm.has("a")
        True
        >>> rm.has("b")
        False

        :param name: Name to check
        :type name: str
        :return: Whether the resource name is registered to the manager
        :rtype: bool
        """
        with self._lock:
            result = name in self._items.keys()
        return result

    def get(self, name):
        """Return the resource object associated with the given name

        Use:

        >>> m = ResourceManager()
        >>> m.load("a", 1)     #doctest: +ELLIPSIS
        <base.Resource object at 0x...>

        >>> m.has("a")
        True

        >>> "a" in m
        True

        >>> with m.get("a") as src:
        ...      value = src + 1
        >>> value
        2

        :param name: Name of resource to get
        :type name: str
        :return: Resource
        :rtype: pycultivator_lab.resource.base.Resource
        """
        with self._lock:
            result = self._items[name]
        return result

    def __contains__(self, item):
        """Whether the item is a resource or a resource name

        >>> m = ResourceManager()
        >>> r = m.load("a", 1)

        >>> "a" in m
        True

        >>> "b" in m
        False

        >>> r in m
        True

        :param item: The item to check for
        :type item: object
        :return: Whether the given item is a key or resource
        :rtype: bool
        """
        return super(ResourceManager, self).__contains__(item)


class ResourceManagerException(PCException):
    """Exception raised by the resource manager"""

    def __init__(self, msg):
        super(ResourceManagerException, self).__init__(msg=msg)
