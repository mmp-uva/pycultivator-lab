"""Implementation a device resource

Device resources come with everything included need to automatically load pycultivator devices from their
configuration sources.

>>> with DeviceResource.load("a", "./pycultivator.xml") as device:
...     device.control("led", "intensity", 1000)

The resource will automatically take care of:

* Loading from configuration source
* Connect and disconnect from device

"""

from pycultivator.config import load_config
from base import AgingResource, ReservableResource, ResourceException
import hashlib
import os

__all__ = [
    'DeviceResource', 'DeviceResourceException',
    'ReloadingDeviceResource'
]


class DeviceResource(ReservableResource, AgingResource):
    """"""

    @staticmethod
    def loadConfig(source, settings=None):
        """Load the config object from the configuration source

        :type source: str
        :type settings: None | dict[str, object]
        :rtype: pycultivator.config.baseConfig.Config
        """
        config = load_config(source, settings=settings)
        return config

    @staticmethod
    def loadDevice(config, settings=None):
        """Load the device from the config object

        :type config: pycultivator.config.baseConfig.Config
        :type settings: None | dict[str, object]
        :rtype: pycultivator.device.device.Device
        """
        xdc = config.getHelper("device")
        device = xdc.load(settings=settings)
        return device

    @classmethod
    def load(cls, name, source, settings=None):
        """Load the device from an configuration source

        :type source: str
        :type settings: None | dict[str, object]
        :rtype: pycultivator_lab.resource.device.DeviceResource
        """
        config = cls.loadConfig(source, settings=None)
        if config is None:
            raise DeviceResourceException("Unable to load {} into a configuration object".format(source))
        # now create device
        device = cls.loadDevice(config)
        if device is None:
            raise DeviceResourceException("Unable to load device object from {}".format(source))
        return cls(name, device, config, source)

    def __init__(self, name, device, config=None, source=None):
        """Initialize a Device Resource object

        :param device: The device to be managed as shared resource
        :type device: pycultivator.device.device.Device
        :param config: Optional config file
        :type config: pycultivator.config.baseConfig.BaseConfig | None
        """
        super(DeviceResource, self).__init__(name, resource=device)
        self._config = config
        self._source = source

    def get(self):
        if not self._resource.isConnected():
            self._resource.connect()
        return super(DeviceResource, self).get()

    def __del__(self):
        if self._resource.isConnected():
            self._resource.disconnect()


class DeviceResourceException(ResourceException):

    def __init__(self, msg):
        super(DeviceResourceException, self).__init__(msg=msg)


class ReloadingDeviceResource(DeviceResource):

    def __init__(self, device, config=None, source=None):
        """Initialize a Device Resource object

        :param device: The device to be managed as shared resource
        :type device: pycultivator.device.device.Device
        :param config: Optional config file
        :type config: pycultivator.config.baseConfig.BaseConfig | None
        """
        super(ReloadingDeviceResource, self).__init__(device=device, config=config, source=source)
        if self._config is not None:
            # get file size
            self._fsize = self.fsize(self._config.location)
            # get modification time
            self._mtime = self.mtime(self._config.location)
            # calculate checksum
            self._fhash = self.hash(self._config.location)

    @staticmethod
    def hash(fn, bs=128*1024):
        """Calculate SHA-456 hash of file

        :param fn: Path to the file to hash
        :
        :param bs: Size of the blocks to read the file in
        :return: Checksum or None if file was not accessible
        :rtype: str | None
        """
        result = None
        if isinstance(fn, str) and os.path.exists(fn):
            h = hashlib.sha256()
            with open(fn, 'rb', buffering=0) as f:
                for b in iter(lambda: f.read(bs), b''):
                    h.update(b)
            result = h.hexdigest()
        return result

    @staticmethod
    def mtime(fn):
        result = None
        if isinstance(fn, str) and os.path.exists(fn):
            info = os.stat(fn)
            result = info.st_mtime
        return result

    @staticmethod
    def fsize(fn):
        result = None
        if isinstance(fn, str) and os.path.exists(fn):
            info = os.stat(fn)
            result = info.st_size
        return result

    def has_changed(self):
        """Compares the current file to the recorded info

        Will first check if size and modification time have changed, if so checks the hash

        :return: Whether the files are equal
        :rtype: bool
        """
        fsize = self.fsize(self._config)
        mtime = self.mtime(self._config)
        result = self._fsize != fsize and self._mtime != mtime
        if result:
            checksum = self.hash(self._config)
            result = checksum != self._fhash
        return result

    def _reload(self):
        """Compares the current file with the recorded info and reloads if necessary

        NOTE: Acquire lock to resource first!
        """
        result = False
        fsize = self.fsize(self._config)
        mtime = self.mtime(self._config)
        checksum = self._fhash
        has_changed = self._fsize != fsize and self._mtime != mtime
        if has_changed:
            checksum = self.hash(self._config)
            result = checksum != self._fhash
        if has_changed:
            self._config = self.loadConfig(self._config.location)
            self._fsize = fsize
            self._mtime = mtime
            self._fhash = checksum
            result = True
        return result

    def get(self):
        # obtain lock
        super(ReloadingDeviceResource, self).get()
        # check whether resource has changed and reload if necessary
        self._reload()
        return self._resource
