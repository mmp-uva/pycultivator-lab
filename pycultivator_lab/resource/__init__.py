"""Package to manage shared resources

* Locking mechanism to limit access to resource to a single user
* Reservation mechanism to signal not to "remove" resource if a user wants to use it
* Aging mechanism to signal whether a resource is already "old" and therefore should be removed
* Manager to manage resources and cleaning of resources

"""

from base import *
from device import *
from manager import *

__all__ = [
    'Resource', 'ResourceException',
    'ReservableResource', 'AgingResource',
    'DeviceResource', 'DeviceResourceException',
    'ReloadingDeviceResource',
    'ResourceManager'
]
