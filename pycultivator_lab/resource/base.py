"""Base implementation of an Resource"""


from pycultivator.core import PCObject, PCException

from datetime import datetime as dt
import threading

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = [
    'Resource', 'ResourceException',
    'AgingResource', 'ReservableResource'
]


class Resource(PCObject):
    """Manage access to a shared object

    >>> res  = Resource("a", 5)
    >>> v = res.get() + 5
    >>> res.release()
    >>> v
    10

    For best performance, use in with statement:

    >>> with Resource("a", 5) as src:
    ...     v = src + 1
    >>> v
    6

    """

    @classmethod
    def load(cls, name, source, **kwargs):
        """Load the resource from a source"""
        return cls(name, source)

    def __init__(self, name, resource):
        super(Resource, self).__init__()
        self._name = name
        self._resource = resource
        self._in_use = False
        self._resource_lock = threading.RLock()
        self._object_lock = threading.RLock()

    @property
    def name(self):
        return self._name

    @property
    def lock(self):
        return self._resource_lock

    @property
    def in_use(self):
        """Whether the resource is being used"""
        with self._object_lock:
            result = self._in_use
        return result

    def acquire(self, blocking=True):
        """Acquire the lock on the resource

        :param blocking: Whether to wait until the lock is release, if locked.
        :type blocking: bool
        :rtype: bool
        """
        with self._object_lock:
            result = self._resource_lock.acquire(blocking)
            if result:
                self._in_use = True
        return result

    def get(self):
        """Access the resource

        Automatically acquires the lock.
        NOTE: you need to manually release the lock when finished with resource
        """
        self.acquire()
        return self._resource

    def set(self, v):
        """Change the resource

        Automatically acquires and releases the lock.
        """
        self.acquire()
        self._resource = v
        self.release()

    def release(self):
        """Release the lock on the resource"""
        with self._object_lock:
            self._in_use = False
            self._resource_lock.release()

    def __enter__(self):
        return self.get()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.release()


class ResourceException(PCException):
    """Exception raised by an resource"""

    def __init__(self, msg):
        super(ResourceException, self).__init__(msg=msg)


class AgingResource(Resource):

    def __init__(self, name, resource):
        super(AgingResource, self).__init__(name, resource)
        self._last_use = dt.now()

    @property
    def last_use(self):
        """Time of last usage, if currently used return now"""
        with self._object_lock:
            result = dt.now() if self.in_use else self._last_use
        return result

    def release(self):
        with self._object_lock:
            self._last_use = dt.now()
            super(AgingResource, self).release()

    def timeSinceLastUse(self):
        """Time difference between now and last usage

        :return: Time difference
        :rtype: datetime.timedelta
        """
        with self._object_lock:
            last_use = self.last_use
        return dt.now() - last_use


class ReservableResource(Resource):
    """Counts the number of users wanting to access this resource in the (near) future"""

    def __init__(self, name, resource):
        super(ReservableResource, self).__init__(name, resource)
        self._users = set()

    def reserve(self, user):
        with self._object_lock:
            self._users.add(user)

    def finish(self, user):
        with self._object_lock:
            if user in self._users:
                self._users.remove(user)

    @property
    def reservations(self):
        with self._object_lock:
            result = len(self._users)
        return result

    @property
    def is_reserved(self):
        with self._object_lock:
            result = self.reservations > 0
        return result
