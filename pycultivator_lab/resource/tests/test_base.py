"""Test base module of the resource package"""

from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator_lab.resource.base import Resource, AgingResource, ReservableResource

import threading
from time import sleep


class TestResource(UvaSubjectTestCase):

    _subject_cls = Resource
    _abstract = False

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(5)

    def test_lock(self):
        self.assertTrue(self.subject.acquire())
        self.subject.release()

        # now test multi threading
        e = threading.Event()

        def f(s):
            s.acquire()
            e.wait()
            s.release()

        t = threading.Thread(target=f, args=(self.subject, ))
        t.start()
        self.assertFalse(self.subject.acquire(False))
        # set e to terminate thread
        e.set()
        t.join()
        self.assertTrue(self.subject.acquire(False))

    def test_in_use(self):
        self.assertFalse(self.subject.in_use)
        self.assertTrue(self.subject.acquire())
        self.assertTrue(self.subject.in_use)
        self.subject.release()
        self.assertFalse(self.subject.in_use)

        # now test multi threading
        e = threading.Event()

        def f(s):
            s.acquire()
            e.wait()
            s.release()

        t = threading.Thread(target=f, args=(self.subject,))
        t.start()
        self.assertTrue(self.subject.in_use)
        # set e to terminate thread
        e.set()
        t.join()
        self.assertFalse(self.subject.in_use)

    def test_lock_independence(self):
        r1 = self.getSubjectClass()(5)
        r2 = self.getSubjectClass()(5)
        self.assertTrue(r1.acquire())
        # test non blocking in case it is not independent
        self.assertTrue(r2.acquire(False))
        r1.release()
        r2.release()


class TestAgingResource(TestResource):

    _subject_cls = AgingResource
    _abstract = False

    def test_aging(self):
        self.assertTrue(self.subject.acquire())
        from time import sleep
        sleep(2)
        # expect it is still used, so last use is 0
        self.assertAlmostEqual(self.subject.timeSinceLastUse().total_seconds(), 0, places=2)
        self.subject.release()
        sleep(2)
        self.assertAlmostEqual(self.subject.timeSinceLastUse().total_seconds(), 2, places=2)


class TestReservableResource(TestResource):

    _subject_cls = ReservableResource
    _abstract = False

    def test_reserve(self):
        self.assertEqual(self.subject.reservations, 0)
        self.subject.reserve()
        self.assertEqual(self.subject.reservations, 1)

    def test_finish(self):
        self.assertEqual(self.subject.reservations, 0)
        self.subject.finish()
        self.assertEqual(self.subject.reservations, 0)
        self.subject.reserve()
        self.assertEqual(self.subject.reservations, 1)
        self.subject.finish()
        self.assertEqual(self.subject.reservations, 0)

    def test_is_reserved(self):
        self.assertFalse(self.subject.is_reserved)
        self.subject.reserve()
        self.assertTrue(self.subject.is_reserved)
        self.subject.finish()
        self.assertFalse(self.subject.is_reserved)

    def test_reservations(self):
        def f(s):
            s.reserve()

        # make 5 threads
        threads = [threading.Thread(target=f, args=(self.subject, )) for i in range(5)]
        self.assertEqual(self.subject.reservations, 0)
        self.assertFalse(self.subject.is_reserved)
        for t in threads:
            t.start()
        self.assertEqual(self.subject.reservations, 5)
        self.assertTrue(self.subject.is_reserved)
        for t in threads:
            t.join()
