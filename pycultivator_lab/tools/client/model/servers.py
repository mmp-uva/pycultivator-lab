
from julesTk import *
import os


class ServersList(JTkObject):

    def __init__(self, heartbeat_location=None):
        super(ServersList, self).__init__()
        self._heartbeat_location = heartbeat_location
        self._data = set()

    @property
    def heartbeat_location(self):
        return self._heartbeat_location

    @heartbeat_location.setter
    def heartbeat_location(self, s):
        self._heartbeat_location = s
        self.update()

    @classmethod
    def read_heartbeats(cls, location, clean=True):
        results = {}
        if isinstance(location, str) and os.path.exists(location):
            with open(location, "r") as src:
                for line in src:
                    pid, uri = "", ""
                    values = line.split(" ")
                    if len(values) >= 2:
                        pid, uri = values[:2]
                        pid.strip()
                        uri.strip()
                    if "" not in (pid, uri):
                        results[pid] = uri
        if clean:
            results = cls.clean_heartbeats(results)
        return results

    @classmethod
    def clean_heartbeats(cls, heartbeats):
        results = {}
        for pid, uri in heartbeats.items():
            if cls.is_heartbeat_alive(pid):
                results[pid] = uri
        return results

    @staticmethod
    def is_heartbeat_alive(pid):
        result = False
        try:
            os.kill(int(pid), 0)
            result = True
        except ValueError:
            pass
        except OSError:
            pass
        return result

    @triggers("model_update")
    def update(self):
        self._data = set()
        heartbeats = self.read_heartbeats(self.heartbeat_location)
        for pid, uri in heartbeats.items():
            self._data.add(uri)
        return self._data
