
from julesTk import *
from tasks import *
import Pyro4
from Pyro4.errors import PyroError


class ServerConnectionModel(JTkObject):
    """Manages the connection to a Server"""

    def __init__(self, uri):
        super(ServerConnectionModel, self).__init__()
        self._uri = uri
        self._connection = None
        self._queued_tasks = TaskQueue()
        self._queued_tasks.add_observer(self)
        self._running_tasks = RunningTasksList()
        self._running_tasks.add_observer(self)
        self._finished_tasks = FinishedTasksList()
        self._finished_tasks.add_observer(self)

    @property
    def uri(self):
        return self._uri

    @property
    def connection(self):
        return self._connection

    @triggers("server_connect")
    def connect(self, uri=None):
        result = False
        if uri is not None:
            self._uri = uri
        try:
            self._connection = Pyro4.Proxy(self.uri)
            self._connection._pyroBind()
            result = True
        except PyroError as pe:
            pass
        return result

    @property
    def queued_tasks(self):
        return self._queued_tasks

    def schedule_task(self, task):
        """Send a task to the server"""
        result = None
        # serialize task
        task_type, arguments, conditions = self.serialize_task(task)
        task_id = self.connection.schedule_task(task_type, arguments=arguments, conditions=conditions)
        if task_id is not None:
            self.queued_tasks.remove(task)
            task.setIdentification(task_id)
            self.running_tasks.add(task)
            self.trigger_event("task_scheduled", data=task_id)
            result = task_id
        return result

    def collect_task(self, task):
        """Retrieve the result of a task

        :type task: pycultivator_lab.tasks.Task
        :return: Dictionary with information about the task
        :rtype: None | dict[str, object]
        """
        task_id = task.getIdentification()
        result = self.connection.collect_task(task_id)
        return result

    @staticmethod
    def serialize_task(task):
        """Break a task down into a type, attributes and conditions

        :type task: pycultivator_lab.tasks.Task
        :return: Task ID
        :rtype: tuple( type, dict[str, str | float | int], dict[str, pycultivator_lab.tasks.condition.Condition]
        """
        task_type = task.getFullName()
        arguments = {}
        for attribute in task.attributes.values():
            arguments[attribute.name] = attribute.value
        return task_type, arguments, task.conditions[:]

    @property
    def running_tasks(self):
        return self._running_tasks

    def update_running_tasks(self):
        self.running_tasks.update(self)

    def is_task_finished(self, task):
        task_id = task.getIdentification()
        result = self.is_task_id_finished(task_id)
        if result:
            self.trigger_event("task_finished", task)
        return result

    def is_task_id_finished(self, task_id):
        result = self.connection.is_task_finished(task_id)
        if result:
            self.trigger_event("task_id_finished", data=task_id)
        return result

    @property
    def finished_tasks(self):
        return self._finished_tasks

    def is_connected(self):
        return self._connection is not None

    @triggers("server_disconnect")
    def disconnect(self):
        self._connection = None
        return True
