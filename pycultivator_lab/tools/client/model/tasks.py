
from julesTk import *
from julesTk.view.listbox import ListModel


def load_task_templates():
    from pycultivator_lab.tasks import find_tasks
    return find_tasks()


class TaskList(ListModel):
    """Base object managing a simple list of tasks"""

    def __init__(self):
        super(TaskList, self).__init__()

    # TODO: Add specific


class TaskQueue(TaskList):
    """Object managing a queue of tasks to be sent to the server"""

    def __init__(self):
        super(TaskQueue, self).__init__()

    # TODO: Add specific


class RunningTasksList(TaskList):
    """Object managing a list of tasks running on the server"""

    def __init__(self):
        super(RunningTasksList, self).__init__()

    def update(self, connection):
        """Iterate over tasks and check if they are still running

        :type connection: pycultivator_lab.client.model.connection.ServerConnection
        """
        new_data = []
        for task in self._data:
            if connection.is_task_finished(task):
                self.remove(task)
                connection.finished_tasks.add(task)
            else:
                new_data.append(task)
        self._data = new_data


class FinishedTasksList(TaskList):
    """Object managing a list of finished tasks"""

    def __init__(self):
        super(FinishedTasksList, self).__init__()
