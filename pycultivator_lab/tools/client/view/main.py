
from julesTk import view, receives


class MainView(view.FrameView):

    def _prepare(self):
        self._prepare_menu()
        self.pack(fill=view.tk.BOTH, expand=1)

        frmb = view.ttk.Frame(self)
        frmb.pack(side=view.tk.TOP, fill=view.tk.BOTH, expand=1)
        self._prepare_body(frmb)

        sep = view.ttk.Separator(self)
        sep.pack(side=view.tk.TOP, fill=view.tk.X)

        frms = view.ttk.Frame(self)
        frms.pack(side=view.tk.BOTTOM, fill=view.tk.X)
        self._prepare_status(frms)

    def _prepare_menu(self):
        menu = view.tk.Menu(self.root)
        self.add_widget("menu", menu)
        self.root.config(menu=menu)

        # server menu
        sem = view.tk.Menu(menu)
        self.add_widget("server_menu", sem)
        menu.add_cascade(label="Server", menu=sem)
        sem.add_cascade(label="Connect...", command=self.connect_server)
        sem.add_cascade(label="Disconnect", command=self.disconnect_server, state="disabled")
        sem.add_separator()
        sem.add_cascade(label="Quit", command=self.close)

        # task menu
        tam = view.tk.Menu(menu)
        self.add_widget("task_menu", tam)
        menu.add_cascade(label="Task", menu=tam)
        tam.add_cascade(label="New...", command=self.create_task)

        # tools menu
        tom = view.tk.Menu(menu)
        self.add_widget("tools_menu", tom)
        menu.add_cascade(label="Tools", menu=tom)
        tom.add_cascade(label="Console", command=self.open_console)

    def _prepare_body(self, parent):
        frma = view.ttk.Frame(parent)
        frma.pack(side='left', fill='both')
        self._prepare_actions(frma)

        sep = view.ttk.Separator(parent)
        sep.pack(side='left', fill='y')

        frmt = view.ttk.Frame(parent)
        frmt.pack(side='left', fill='both', expand=1)
        self._prepare_tasks(frmt)

    def _prepare_actions(self, parent):
        btcon = view.ttk.Button(parent, text="Connect...", command=self.connect_server)
        self.add_widget("button_connect", btcon)
        btcon.pack(side='top', fill='x')
        btdis = view.ttk.Button(parent, text="Disconnect", command=self.disconnect_server, state="disabled")
        self.add_widget("button_disconnect", btdis)
        btdis.pack(side='top', fill='x')
        btnt = view.ttk.Button(parent, text="Create Task", command=self.create_task)
        self.add_widget("button_new_task", btnt)
        btnt.pack(side='top', fill='x')

    def _prepare_tasks(self, parent):
        # overview of queued tasks
        # frmq = view.ttk.Frame(parent)
        # frmq.pack(side='top', fill='both', expand=1)
        self._prepare_tasks_queue(parent)
        sepq = view.ttk.Separator(parent)
        sepq.pack(side='top', fill='x', pady=5)
        # overview of running tasks
        # frmr = view.ttk.Frame(parent)
        # frmr.pack(side='top', fill='both', expand=1)
        self._prepare_tasks_running(parent)
        sepr = view.ttk.Separator(parent)
        sepr.pack(side='top', fill='x', pady=5)
        # # overview of finished tasks
        # frmf = view.ttk.Frame(parent)
        # frmf.pack(side='top', fill='both', expand=1)
        self._prepare_tasks_finished(parent)

    def _prepare_tasks_queue(self, parent):
        from task_list import QueuedTasksListbox, QueuedTaskListboxController
        lb = QueuedTasksListbox(parent)
        self.add_widget("queued_tasks", lb)
        task_queue = self.application.connection.queued_tasks
        lbc = QueuedTaskListboxController(self.controller, lb, task_queue)
        lbc.start()
        lb.pack(side='top', fill='both', expand=1)

    def _prepare_tasks_running(self, parent):
        from task_list import RunningTasksListbox, RunningTasksListboxController
        lb = RunningTasksListbox(parent)
        self.add_widget("running_tasks", lb)
        running_tasks = self.application.connection.running_tasks
        lbc = RunningTasksListboxController(self.controller, lb, running_tasks)
        lbc.start()
        lb.pack(side='top', fill='both', expand=1)

    def _prepare_tasks_finished(self, parent):
        from task_list import FinishedTasksListbox, FinishedTasksListboxController
        lb = FinishedTasksListbox(parent)
        self.add_widget("finished_tasks", lb)
        finished_tasks = self.application.connection.finished_tasks
        lbc = FinishedTasksListboxController(self.controller, lb, finished_tasks)
        lbc.start()
        lb.pack(side='top', fill='both', expand=1)

    def _prepare_status(self, parent):
        """Create statusbar"""
        status = self.add_variable("status", view.tk.StringVar())
        lbl = view.ttk.Label(parent, textvariable=status, anchor="w")
        self.status = "Welcome"
        lbl.pack(side="left", fill=view.tk.BOTH, expand=1)
        # add circle widget reflecting connection state
        stc = view.tk.Canvas(parent, width=50, height=25, borderwidth=0, highlightthickness=0)
        self.add_widget("status_canvas", stc)
        stc.pack(side="right", fill=view.tk.Y)
        # circle = padx 5, pady 5, radius = 15
        conc = stc.create_oval(5, 5, 20, 20, fill="darkred")
        self.add_widget("status_connection", conc)
        comc = stc.create_oval(30, 5, 45, 20, fill="darkred")
        self.add_widget("status_communication", comc)

    def set_connected(self, connected=False):
        stc = self.get_widget("status_canvas")
        cnc = self.get_widget("status_connection")
        stc.itemconfig(cnc, fill="darkgreen" if connected else "darkred")
        self.toggle_connection(connected)

    def set_communicating(self, active=False):
        stc = self.get_widget("status_canvas")
        cmc = self.get_widget("status_communication")
        stc.itemconfig(cmc, fill="darkgreen" if active else "darkred")

    @property
    def status(self):
        return self.get_variable("status").get()

    @status.setter
    def status(self, msg):
        self.get_variable("status").set(msg)

    def connect_server(self):
        self.controller.connect_server()

    @receives("server_connect")
    def event_connect(self, event, source, data=None):
        self.status = "Connected to {}".format(source.uri)
        # update menu
        if data:
            self.set_connected(True)

    def disconnect_server(self):
        self.controller.disconnect_server()

    @receives("server_disconnect")
    def event_disconnect(self, event, source, data=None):
        self.status = "Disconnected from {}".format(source.uri)
        if data:
            self.set_connected(False)

    def toggle_connection(self, state):
        connect_state = "disabled" if state else "normal"
        disconnect_state = "normal" if state else "disabled"
        # update menu
        sem = self.get_widget("server_menu")
        sem.entryconfigure('Connect...', state=connect_state)
        sem.entryconfigure('Disconnect', state=disconnect_state)
        #tam = self.get_widget("task_menu")
        #tam.entryconfigure("New...", state=disconnect_state)
        # update buttons
        self.get_widget("button_connect").configure(state=connect_state)
        self.get_widget("button_disconnect").configure(state=disconnect_state)
        #self.get_widget("button_new_task").configure(state=disconnect_state)

    def open_console(self):
        self.controller.open_console()

    def create_task(self):
        self.controller.create_task()
