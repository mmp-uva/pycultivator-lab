
from julesTk import view, triggers, receives
from julesTk.view import window
from julesTk.utils.console import LogView


class ConsoleView(window.Window):

    def _prepare(self):
        self.title("Console")
        self.geometry('600x150+200+200')
        self.minsize(600, 150)

        self.grid_rowconfigure(1, weight=1)
        self.grid_columnconfigure(0, weight=1)

        frmh = view.ttk.Frame(self, borderwidth=1)
        frmh.grid(row=0, column=0, sticky="ew")
        self._prepare_header(frmh)

        frmb = view.ttk.Frame(self, relief=view.tk.RAISED, borderwidth=1)
        frmb.grid(row=1, column=0, sticky="nsew")
        self._prepare_body(frmb)

        frmt = view.ttk.Frame(self, relief=view.tk.RAISED, borderwidth=1)
        frmt.grid(row=2, column=0, sticky="ew")
        self._prepare_tools(frmt)

    def _prepare_header(self, parent=None):
        if parent is None:
            parent = self
        lbl = view.ttk.Label(parent, text="Log level:")
        lbl.pack(side=view.tk.LEFT)
        v = self.add_variable("log_level", view.tk.StringVar())
        values = [100, 90, 80, 70, 60, 50, 40, 30, 20, 10]
        lib = view.ttk.Combobox(parent, textvariable=v, state="readonly", values=values)
        lib.bind("<<ComboboxSelected>>", self.update_level)
        lib.pack(side=view.tk.LEFT, fill=view.tk.X, expand=1)

    def _prepare_body(self, parent=None):
        if parent is None:
            parent = self
        log = LogView(parent)
        self.add_widget("log", log)
        log.pack(fill=view.tk.BOTH, expand=1)

    def _prepare_tools(self, parent=None):
        if parent is None:
            parent = self
        log = self.get_widget("log")
        btn = view.ttk.Button(parent, text="Close", command=self.close)
        btn.pack(side=view.tk.RIGHT)
        btc = view.ttk.Button(parent, text="Clear", command=log.clear)
        btc.pack(side=view.tk.RIGHT)

    def update_level(self, event=None):
        v = self.get_variable("log_level").get()
        self.controller.set_log_level(int(v))
