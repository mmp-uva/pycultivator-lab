
from julesTk import view, receives, triggers
from julesTk.view.listbox import Listbox, ListboxController


class TaskListbox(Listbox):

    def __init__(self, parent, controller=None):
        super(TaskListbox, self).__init__(parent, controller=controller)

    def get_controller(self):
        """Return the Controller of this view

        :return:
        :rtype: TaskListboxController
        """
        return self._controller

    def item_to_str(self, task):
        index = self.get_list().index(task)
        return "{}. {} ({})".format(index + 1, task.getName(), task.getIdentification())


class TaskListboxController(ListboxController):

    VIEW_CLASS = TaskListbox

    def __init__(self, parent, v=None, list_model=None):
        super(TaskListboxController, self).__init__(parent, view=v, list_model=list_model)

    # TODO: Add specific methods


class QueuedTasksListbox(TaskListbox):

    def _prepare_header(self, parent):
        super(QueuedTasksListbox, self)._prepare_header(parent)
        lbl = self.get_widget("title")
        lbl.configure(text="Queued Tasks")

    def _prepare_buttons(self, parent):
        super(QueuedTasksListbox, self)._prepare_buttons(parent)
        bts = view.ttk.Button(parent, text="Schedule", command=self.schedule)
        bts.pack(side="right")
        btn = view.ttk.Button(parent, text="New", command=self.add_item)
        btn.pack(side='right')

    def schedule(self, event=None):
        task = self.get_selected_item()
        if task is None:
            self.show_nothing_selected()
        else:
            self.controller.schedule_task(task)


class QueuedTaskListboxController(TaskListboxController):

    VIEW_CLASS = QueuedTasksListbox

    def __init__(self, parent, v=None, list_model=None):
        if v is not None and not isinstance(v, QueuedTasksListbox):
            raise ValueError("Invalid view, expected a QueuedTasksListbox")
        super(QueuedTaskListboxController, self).__init__(parent, v, list_model)

    def get_list_model(self):
        """

        :return:
        :rtype: pycultivator_lab.tools.client.model.tasks.TaskQueue
        """
        return super(QueuedTaskListboxController, self).get_list_model()

    def schedule_task(self, task):
        self.parent.schedule_task(task)

    def add_item(self):
        self.trigger_event("create_task")

    def view_item(self, item):
        # redirect
        self.edit_item(item)

    def edit_item(self, item):
        self.parent.edit_task(item)


class RunningTasksListbox(TaskListbox):

    def _prepare_header(self, parent):
        super(RunningTasksListbox, self)._prepare_header(parent)
        lbl = self.get_widget("title")
        lbl.configure(text="Running Tasks")

    def _prepare_buttons(self, parent):
        pass


class RunningTasksListboxController(TaskListboxController):

    VIEW_CLASS = RunningTasksListbox

    def __init__(self, parent, v=None, list_model=None):
        if v is not None and not isinstance(v, RunningTasksListbox):
            raise ValueError("Invalid view, expected a RunningTasksListbox")
        super(RunningTasksListboxController, self).__init__(parent, v, list_model)

    def get_list_model(self):
        """

        :return:
        :rtype: pycultivator_lab.tools.client.model.tasks.RunningTasksList
        """
        return super(RunningTasksListboxController, self).get_list_model()


class FinishedTasksListbox(TaskListbox):

    def _prepare_header(self, parent):
        super(FinishedTasksListbox, self)._prepare_header(parent)
        lbl = self.get_widget("title")
        lbl.configure(text="Finished Tasks")

    def _prepare_buttons(self, parent):
        super(FinishedTasksListbox, self)._prepare_buttons(parent)


class FinishedTasksListboxController(TaskListboxController):

    VIEW_CLASS = FinishedTasksListbox

    def __init__(self, parent, v=None, list_model=None):
        if v is not None and not isinstance(v, FinishedTasksListbox):
            raise ValueError("Invalid view, expected a FinishedTasksListbox")
        super(FinishedTasksListboxController, self).__init__(parent, v, list_model)

    def get_list_model(self):
        """

        :return:
        :rtype: pycultivator_lab.tools.client.model.tasks.RunningTasksList
        """
        return super(FinishedTasksListboxController, self).get_list_model()

    def view_item(self, task):
        self.parent.inspect_finished_task(task)
