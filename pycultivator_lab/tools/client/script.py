"""Client GUI for the pycultivator_lab Task Server"""

from __future__ import print_function
# from julesTk import triggers, receives
from julesTk.app import Application
from controller.main import MainController
from model.servers import ServersList
from model.connection import ServerConnectionModel
from pycultivator_lab.scripts.meta import ConfigurableMetaScript, ScriptAttribute
from pycultivator_lab.core import validators
from pycultivator_lab.scripts.extras.ini_config import INIConfig
import os


class TaskServerClientApp(Application):

    def __init__(self, heartbeats=None):
        super(TaskServerClientApp, self).__init__()
        self._servers = ServersList(heartbeats)
        self._connection = ServerConnectionModel(None)

    @property
    def main(self):
        return self.get_controller("main")

    @property
    def servers(self):
        return self._servers

    @property
    def connection(self):
        return self._connection

    def has_connection(self):
        return self.connection is not None

    def is_connected(self):
        return self.has_connection() and self.connection.is_connected()

    def _start(self):
        self.main.start()

    def _prepare(self):
        self.root.wm_title("pyCultivator Task Server Client")
        self.root.wm_minsize(450, 550)
        self.root.geometry('550x450+100+100')
        self.add_controller("main", MainController(self))
        return True

    def _stop(self):
        super(TaskServerClientApp, self)._stop()
        if self.connection.is_connected():
            self.connection.disconnect()


class TaskServerClient(ConfigurableMetaScript):

    ini_file = ScriptAttribute(
        str, default=None, aliases=["ini"], is_required=False,
        validators=[validators.PathValidator()], weight=90
    )

    _namespace = "script.tasks.client"
    _default_settings = {
        "root.dir": None,
        "heartbeat.path": None,
        "heartbeat.name": "pycultivator-servers.lib",
    }

    def __init__(self, *args, **kwargs):
        super(TaskServerClient, self).__init__()
        self._application = None

    def get_root_dir(self):
        result = self.getSetting("root.dir")
        if not isinstance(result, str) or not os.path.exists(result):
            result = os.getcwd()
        return result

    def get_library_path(self):
        result = self.getSetting("heartbeat.path")
        if result is None:
            result = os.path.join(self.get_root_dir(), self.get_library_name())
        return result

    def get_library_name(self):
        return self.getSetting("heartbeat.name")

    # script methods

    def prepare(self):
        super(TaskServerClient, self).prepare()
        if self.ini_file is not None:
            settings = INIConfig.load_ini(self.ini_file)
            self.settings.update(settings)
        return True

    def execute(self):
        heartbeats = self.get_library_path()
        self._application = TaskServerClientApp(heartbeats=heartbeats)
        self._application.run()
        return True


def main():
    TaskServerClient().run()


if __name__ == "__main__":
    main()
