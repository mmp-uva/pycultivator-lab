from julesTk import view, receives
from julesTk.dialog import DialogTemplate
from julesTk.controller import ViewController
from pycultivator_lab.tools.client.model.servers import ServersList


class ServersDialog(DialogTemplate):
    """A dialog showing the option the enter a Server URI or pick from a heartbeat list"""

    def _prepare(self):
        self.title("Select a Server")
        self.geometry('400x300+200+200')
        self.minsize(300, 300)
        super(ServersDialog, self)._prepare()

    def _prepare_header(self, parent):
        return True

    def _prepare_body(self, parent):
        lbls = view.ttk.Label(parent, text="Discovered Servers:")
        lbls.pack(side='top', fill='x', expand=1)
        frms = view.ttk.Frame(parent)
        frms.pack(side='top', fill='both', expand=1)
        self._prepare_server_list(frms)
        lblu = view.ttk.Label(parent, text="Or enter custom URI:")
        lblu.pack(side='top', fill='x', expand=1)
        uri = self.add_variable("server_uri", view.tk.StringVar())
        tbu = view.ttk.Entry(parent, textvariable=uri)
        tbu.pack(side='top', fill='x', expand=1)

    def _prepare_server_list(self, parent):
        lbs = view.tk.Listbox(parent)
        self.add_widget("server_list", lbs)
        scbs = view.tk.Scrollbar(parent, orient="vertical")
        scbs.config(command=lbs.yview)
        scbs.pack(side='right', fill='y')
        lbs.pack(side='left', fill='both', expand=1)
        lbs.bind("<<ListboxSelect>>", self.update_uri)
        lbs.bind("<Double-Button-1>", self.accept_uri)

    def _prepare_footer(self, parent):
        bto = view.ttk.Button(parent, text="OK", command=self.accept_uri)
        bto.pack(side="right", padx=5)
        btr = view.ttk.Button(parent, text="Refresh", command=self.refresh_servers)
        btr.pack(side="right", padx=5)
        btc = view.ttk.Button(parent, text="Cancel", command=self.cancel)
        btc.pack(side="right", padx=5)

    def get_uri_from_list(self):
        result = None
        lbs = self.get_widget("server_list")
        """:type: Tkinter.Listbox | tkinter.Listbox"""
        items = lbs.curselection()
        if len(items) > 0:
            result = lbs.get(items[0])
        if result is None and lbs.size() > 0:
            lbs.selection_set(0)
            result = lbs.get(0)
        return result

    def get_uri_from_entry(self):
        return self.get_variable("server_uri").get()

    def refresh_servers(self):
        self.trigger_event("update_server_list")

    def update_uri(self, event):
        uri = self.get_uri_from_list()
        if uri != "No servers found":
            self.get_variable("server_uri").set(uri)

    @receives("model_update")
    def _event_update_servers(self, event, source, data=None):
        self.update_servers(data)

    def update_servers(self, servers=None):
        if servers is None:
            servers = []
        lbs = self.get_widget("server_list")
        """:type: Tkinter.Listbox"""
        lbs.configure(state="normal")
        lbs.delete(0, "end")    # clear
        if len(servers) == 0:
            lbs.insert('end', "No servers found")
            lbs.configure(state="disabled")
        else:
            for uri in servers:
                lbs.insert('end', uri)
        lbs.update_idletasks()

    def accept_uri(self, event=None):
        server_uri = None
        list_uri = self.get_uri_from_list()
        custom_uri = self.get_uri_from_entry()
        if list_uri not in ("", None, "No servers found"):
            server_uri = list_uri
        if custom_uri not in ("", None, list_uri):
            server_uri = custom_uri
        if server_uri is not None:
            self._response = server_uri
            self.close()

    def cancel(self):
        self.close()


class ServerDialogController(ViewController):

    VIEW_CLASS = ServersDialog

    def __init__(self, parent, view=None, model=None):
        super(ServerDialogController, self).__init__(parent=parent, view=view)
        if model is None:
            model = ServersList()
        self._model = model

    @property
    def model(self):
        return self._model

    @receives("update_server_list")
    def update_server_list(self, event, source, data=None):
        return self.model.update()

    def _prepare(self):
        super(ServerDialogController, self)._prepare()
        if self.has_view():
            # connect view to model events
            self.model.add_observer(self.view)
        self.model.update()

    def _start(self):
        super(ServerDialogController, self)._start()
        result = None
        if self.has_view():
            result = self.view.response
        return result

    def _stop(self):
        if self.has_view():
            self.model.remove_observer(self.view)
        super(ServerDialogController, self)._stop()
