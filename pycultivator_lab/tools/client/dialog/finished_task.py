
from julesTk import view, receives, triggers
from julesTk.controller import ViewController
from julesTk.dialog import BaseDialog
from julesTk.utils.validation import *
from datetime import datetime as dt

from pycultivator_lab.tools.client.model.tasks import load_task_templates


class FinishedTaskDialog(BaseDialog):
    """A dialog showing the details of a task"""

    def __init__(self, parent, ctrl=None):
        super(FinishedTaskDialog, self).__init__(parent, ctrl=ctrl)
        self._parameter_widgets = {}
        self._parameter_variables = {}

    @property
    def task(self):
        return self.get_task()

    def get_task(self):
        """

        :return:
        :rtype: None | pycultivator_lab.tasks.Task
        """
        result = None
        if self.has_controller():
            result = self.controller.get_task()
        return result

    def has_task(self):
        return self.get_task() is not None

    def _prepare(self):
        self.title("Task Results")
        self.geometry('400x300+200+200')
        self.minsize(300, 300)
        self.bind("<Escape>", self.close)
        # self.bind("<Return>", self.accept)
        # register styles
        view.ttk.Style().configure("Error.TLabel", foreground="red")
        view.ttk.Style().configure("Error.TEntry", foreground="red")
        # create layout
        frmp = view.ttk.Frame(self)
        frmp.pack(side='top', fill='both', expand=1)
        # create header
        frmh = view.ttk.Frame(frmp)
        frmh.pack(side='top', fill='x', padx=5, pady=5)
        self._prepare_header(frmh)
        seph = view.ttk.Separator(frmp)
        seph.pack(side='top', fill='x')
        # create body
        frmb = view.ttk.Frame(frmp)
        frmb.pack(side='top', fill='both', expand=1)
        self._prepare_body(frmb)
        sepf = view.ttk.Separator(frmp)
        sepf.pack(side='top', fill='x')
        # create footer
        frmf = view.ttk.Frame(frmp)
        frmf.pack(side='bottom', fill='x', padx=5, pady=5)
        self._prepare_footer(frmf)
        self.update_results()

    def _prepare_header(self, parent):
        task = self.get_task()
        # name
        lblt = view.ttk.Label(parent, text="Task Template:")
        lblt.grid(row=0, column=0, sticky="nsw")
        ett = view.ttk.Entry(parent)
        ett.insert(0, task.getFullName())
        ett.configure(state="readonly")
        ett.grid(row=0, column=1, sticky="nse")
        # identification
        lbli = view.ttk.Label(parent, text="Task ID:")
        lbli.grid(row=1, column=0, sticky="nsw")
        eti = view.ttk.Entry(parent)
        eti.insert(0, task.getIdentification())
        eti.configure(state="readonly")
        eti.grid(row=1, column=1, sticky="nse")
        # start time
        lblst = view.ttk.Label(parent, text="Start Time:")
        lblst.grid(row=2, column=0, sticky="nsw")
        etst = view.ttk.Entry(parent, state="readonly")
        self.add_widget("task_t_start", etst)
        etst.grid(row=2, column=1, sticky="nse")
        # finish time
        lblft = view.ttk.Label(parent, text="Finish Time:")
        lblft.grid(row=3, column=0, sticky="nsw")
        etft = view.ttk.Entry(parent, state="readonly")
        self.add_widget("task_t_finish", etft)
        etft.grid(row=3, column=1, sticky="nse")
        # success?
        lblsc = view.ttk.Label(parent, text="Success:")
        lblsc.grid(row=4, column=0, sticky="nsw")
        etsc = view.ttk.Entry(parent, state="readonly")
        self.add_widget("task_success", etsc)
        etsc.grid(row=4, column=1, sticky="nse")
        # layout
        self.configure_column(parent, 1)
        self.configure_row(parent, range(5))

    def _prepare_body(self, parent):
        lbls = view.ttk.Label(parent, text="Result:")
        lbls.pack(side='top', fill='x')
        frmr = view.ttk.Frame(parent, relief="sunken", borderwidth=1)
        frmr.pack(side='top', fill='both', expand=1)
        self._prepare_results(frmr)

    def _prepare_results(self, parent):
        txr = view.tk.Text(parent, state="disabled", height=5)
        # txr.bind("<1>", lambda event: txr.focus_set())
        txr.pack(side="top", fill='both', expand=1)
        self.add_widget("task_result", txr)

    @receives("update_task_results")
    def _event_update_results(self, event, source, data=None):
        self.update_results()

    def update_results(self):
        task_results = self.controller.get_task_results()
        # update start time
        start_time = task_results.get("t_start")
        if isinstance(start_time, (str, unicode)):
            start_time = dt.strptime(start_time, "%Y-%m-%dT%H:%M:%S.%f")
        if isinstance(start_time, dt):
            start_time = start_time.strftime("%Y-%m-%d %H:%M:%S")
        etst = self.get_widget("task_t_start")
        self.update_entry_widget(etst, start_time)
        # update finish time
        finish_time = task_results.get("t_finish")
        if isinstance(finish_time, (str, unicode)):
            finish_time = dt.strptime(finish_time, "%Y-%m-%dT%H:%M:%S.%f")
        if isinstance(finish_time, dt):
            finish_time = finish_time.strftime("%Y-%m-%d %H:%M:%S")
        etft = self.get_widget("task_t_finish")
        self.update_entry_widget(etft, finish_time)
        # update success
        task_success = task_results.get("success")
        etsc = self.get_widget("task_success")
        if task_success:
            self.update_entry_widget(etsc, "True")
        else:
            self.update_entry_widget(etsc, "False", style="Error.TEntry")
        # update result
        task_result = task_results.get("result")
        txr = self.get_widget("task_result")
        self.update_entry_widget(txr, str(task_result))

    def update_entry_widget(self, widget, value, **kwargs):
        widget.configure(state="normal")
        if isinstance(widget, view.tk.Text):
            widget.delete("1.0", 'end')
        else:
            widget.delete(0, 'end')
        widget.insert('end', value)
        widget.configure(**kwargs)
        if isinstance(widget, (view.tk.Entry, view.ttk.Entry)):
            widget.configure(state="readonly")
        else:
            widget.configure(state="disabled")

    def _prepare_footer(self, parent):
        btc = view.ttk.Button(parent, text="Close", command=self.close)
        btc.focus_set()
        self.add_widget("button_close", btc)
        btc.pack(side="right", padx=5)


class FinishedTaskDialogController(ViewController):

    VIEW_CLASS = FinishedTaskDialog

    def __init__(self, parent, view=None):
        super(FinishedTaskDialogController, self).__init__(parent=parent, view=view)
        self._task = None
        self._task_results = {}

    def get_task(self):
        return self._task

    @property
    def task(self):
        return self.get_task()

    def has_task(self):
        return self.get_task() is not None

    def get_task_results(self):
        return self._task_results

    def _prepare(self):
        super(FinishedTaskDialogController, self)._prepare()

    def _start(self):
        super(FinishedTaskDialogController, self)._start()
        result = False
        if self.has_view() and self.view.response:
            result = True
        return result

    def load_task_templates(self):
        """Load a Dictionary with all installed tasks

        :rtype: dict[str, callable -> pycultivator_lab.tasks.Task]
        """
        return load_task_templates()

    def collect_task_info(self):
        results = {}
        if self.has_task():
            results = self.application.connection.collect_task(self.get_task())
        self._task_results = results
        return results

    def start(self, task=None):
        if task is not None:
            self._task = task
            self.collect_task_info()
        #     import copy
        #     self._task = copy.deepcopy(task)
        #     self._task.setIdentification(task.getIdentification())
        return super(FinishedTaskDialogController, self).start()

    def _stop(self):
        super(FinishedTaskDialogController, self)._stop()

