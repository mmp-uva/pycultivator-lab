
from julesTk import view, receives, triggers
from julesTk.controller import ViewController
from julesTk.dialog import BaseDialog
from julesTk.utils.validation import *
from julesTk.view.listbox import *

from pycultivator_lab.tools.client.model.tasks import load_task_templates


class EditTaskDialog(BaseDialog):
    """A dialog showing the option the enter a Server URI or pick from a heartbeat list"""

    def __init__(self, parent, ctrl=None):
        super(EditTaskDialog, self).__init__(parent, ctrl=ctrl)
        self._parameter_widgets = {}
        self._parameter_variables = {}

    @property
    def task(self):
        return self.get_task()

    def get_task(self):
        """

        :return:
        :rtype: None | pycultivator_lab.tasks.Task
        """
        result = None
        if self.has_controller():
            result = self.controller.get_task()
        return result

    def has_task(self):
        return self.get_task() is not None

    def _prepare(self):
        if not self.has_task():
            self.title("Create Task")
        else:
            self.title("Edit Task")
        self.geometry('500x400+200+200')
        self.minsize(500, 400)
        self.bind("<Escape>", self.cancel)
        # self.bind("<Return>", self.accept)
        frmp = view.ttk.Frame(self)
        frmp.pack(side='top', fill='both', expand=1)
        # create header
        frmh = view.ttk.Frame(frmp)
        frmh.pack(side='top', fill='x', padx=5, pady=5)
        self._prepare_header(frmh)
        seph = view.ttk.Separator(frmp)
        seph.pack(side='top', fill='x')
        # create parameters body
        frmb = view.ttk.Frame(frmp)
        frmb.pack(side='top', fill='both', expand=1)
        self._prepare_body(frmb)
        sepf = view.ttk.Separator(frmp)
        sepf.pack(side='top', fill='x')
        # create footer
        frmf = view.ttk.Frame(frmp)
        frmf.pack(side='bottom', fill='x', padx=5, pady=5)
        self._prepare_footer(frmf)
        if not self.has_task():
            self.change_template()
        else:
            self.update_parameters()

    def _prepare_header(self, parent):
        lbls = view.ttk.Label(parent, text="Task Template:")
        lbls.pack(side='left', fill='x')
        # load list of available templates
        templates = self.controller.load_task_templates()
        template_names = templates.keys()
        task = self.add_variable("task_template", view.tk.StringVar())
        cbt = view.ttk.Combobox(
            parent, textvariable=task, values=template_names, state="readonly"
        )
        cbt.bind("<<ComboboxSelected>>", self.change_template)
        selection = None
        if self.has_task():
            cbt.configure(state="disabled")
            selection = self.find_template_name(templates, self.get_task())
        elif len(template_names) > 0:
            selection = template_names[0]
        if selection is None:
            selection = "No templates available"
        # set variable
        task.set(selection)
        # align
        cbt.pack(side='right', fill='x', expand=1)

    def find_template_name(self, templates, task):
        result = None
        template_klass = task.__class__
        for key, klass in templates.items():
            if klass is template_klass:
                result = key
                break
        return result

    def _prepare_body(self, parent):
        frmp = view.ttk.Frame(parent)
        frmp.pack(side='left', fill='both', expand=1, padx=5, pady=5)
        self._prepare_parameters(frmp)
        sepc = view.ttk.Separator(parent)
        sepc.pack(side='left', fill='y', padx=5)
        # create conditions body
        frmc = view.ttk.Frame(parent)
        frmc.pack(side='right', fill='both', expand=1, padx=5, pady=5)
        self._prepare_conditions(frmc)

    def _prepare_parameters(self, parent):
        lbls = view.ttk.Label(parent, text="Task Parameters")
        lbls.pack(side='top', fill='x')
        frmp = view.ttk.Frame(parent, relief="sunken", borderwidth=1)
        frmp.pack(side='top', fill='both', expand=1)
        self.add_widget("frame_parameters", frmp)

    def _prepare_conditions(self, parent):
        lb = ConditionsListbox(parent)
        self.add_widget("conditions", lb)
        conditions = ListModel()
        lbc = ConditionsListboxController(self.controller, lb, conditions)
        lbc.start()
        lb.pack(side='top', fill='both', expand=1)

    def update_parameters(self, parent=None):
        if parent is None:
            parent = self.get_widget("frame_parameters")
        self.clear_parameters()
        # rebuild
        row_idx = 0
        if self.has_task():
            task = self.get_task()
            task_id = task.getIdentification()
            if task_id is None:
                task_id = "None"
            lbl, widget = self.create_parameter_widget(parent, "ID", task_id, row_idx=row_idx)
            lbl.configure(text="Task ID")
            widget.configure(state="readonly")
            row_idx += 1
            for attr_name, attr in task.attributes.items():
                self.create_parameter_widget(
                    parent, attr_name, attr.value, attr.type, row_idx=row_idx
                )
                row_idx += 1
        self.configure_column(parent, [1])
        self.configure_row(parent, range(row_idx))

    def create_parameter_widget(self, parent, name, value, type_=str, row_idx=0):
        # create widget from a form
        lbl, widget = self.create_form_widget(parent, name, type_, value, row_idx=row_idx)
        self._parameter_widgets["{}_lbl".format(name.lower())] = lbl
        self._parameter_widgets["{}_widget".format(name.lower())] = widget
        return lbl, widget

    def create_form_widget(self, parent, name, type_, value, row_idx=0):
        lbl = view.ttk.Label(parent, text=name)
        lbl.grid(row=row_idx, column=0, sticky="nwe")
        if type_ in (int, "int"):
            widget = ValidatingEntry(parent, value=value, validator=IntegerValidator())
        elif type_ in (float, "float"):
            widget = ValidatingEntry(parent, value=value, validator=FloatValidator())
        else:
            widget = ValidatingEntry(parent, value=value)
        widget.grid(row=row_idx, column=1, sticky="nwe")
        return lbl, widget

    def clear_parameters(self):
        while len(self._parameter_widgets) > 0:
            widget_name = next(iter(self._parameter_widgets.keys()))
            widget = self._parameter_widgets.pop(widget_name)
            widget.destroy()

    def _prepare_footer(self, parent):
        bto = view.ttk.Button(parent, text="OK", command=self.accept)
        bto.focus_set()
        self.add_widget("button_ok", bto)
        bto.pack(side="right", padx=5)
        btv = view.ttk.Button(parent, text="Validate", command=self.validate)
        self.add_widget("button_validate", btv)
        btv.pack(side="right", padx=5)
        btc = view.ttk.Button(parent, text="Cancel", command=self.cancel)
        self.add_widget("button_cancel", btc)
        btc.pack(side="left", padx=5)

    @receives("update_template")
    def update_template(self, event, source, data=None):
        self.update_parameters()

    def change_template(self, event=None):
        task = self.get_variable("task_template").get()
        self.controller.update_task_template(task)

    def validate(self):
        result = True
        if self.has_task():
            task = self.get_task()
            for widget_name in self._parameter_widgets.keys():
                if widget_name.endswith("_widget"):
                    widget = self._parameter_widgets[widget_name]
                    name = widget_name.replace("_widget", "")
                    if task.hasAttribute(name):
                        attr = task.getAttribute(name)
                        is_valid = attr.validate(widget.value)
                        widget.configure(highlightbackground="white" if is_valid else "darkred")
                        result = is_valid and result
        return result

    def apply(self):
        """Apply settings to task"""
        result = True
        if self.has_task():
            task = self.get_task()
            for widget_name in self._parameter_widgets.keys():
                if widget_name.endswith("_widget"):
                    widget = self._parameter_widgets[widget_name]
                    name = widget_name.replace("_widget", "")
                    if task.hasAttribute(name):
                        attr = task.getAttribute(name)
                        is_valid = attr.set(widget.value)
                        widget.configure(highlightbackground="white" if is_valid else "darkred")
                        result = is_valid and result
        return result

    def accept(self, event=None):
        if self.apply():
            self._response = True
            self.close()

    def cancel(self, event=None):
        self.close()


class TaskEditDialogController(ViewController):

    VIEW_CLASS = EditTaskDialog

    def __init__(self, parent, view=None):
        super(TaskEditDialogController, self).__init__(parent=parent, view=view)
        self._task = None

    def get_task(self):
        return self._task

    @property
    def task(self):
        return self.get_task()

    def has_task(self):
        return self.get_task() is not None

    def _prepare(self):
        super(TaskEditDialogController, self)._prepare()

    def _start(self):
        super(TaskEditDialogController, self)._start()
        result = None
        if self.has_view() and self.view.response:
            result = self.task
        return result

    def load_task_templates(self):
        """Load a Dictionary with all installed tasks

        :rtype: dict[str, callable -> pycultivator_lab.tasks.Task]
        """
        return load_task_templates()

    def update_task_template(self, template_name):
        template_klass = None
        if template_name in self.load_task_templates().keys():
            template_klass = self.load_task_templates()[template_name]
        if template_klass is not None:
            if not isinstance(self._task, template_klass):
                # replace
                self._task = template_klass()
                self.trigger_event("update_template")

    def start(self, task=None):
        if task is not None:
            self._task = task
        #     import copy
        #     self._task = copy.deepcopy(task)
        #     self._task.setIdentification(task.getIdentification())
        return super(TaskEditDialogController, self).start()

    def _stop(self):
        super(TaskEditDialogController, self)._stop()


class ConditionsList(ListModel):

    def __init__(self):
        super(ConditionsList, self).__init__()


class ConditionsListbox(Listbox):

    def _prepare_header(self, parent):
        super(ConditionsListbox, self)._prepare_header(parent)
        lbl = self.get_widget("title")
        lbl.configure(text="Task Conditions")

    def _prepare_buttons(self, parent):
        super(ConditionsListbox, self)._prepare_buttons(parent)


class ConditionsListboxController(ListboxController):

    def __init__(self, parent, v=None, list_model=None):
        super(ConditionsListboxController, self).__init__(parent, v, list_model)

    def add_item(self):
        pass

    def edit_item(self, item):
        pass

    def delete_item(self, item):
        pass

