

from julesTk import controller, receives
from julesTk.controller.poller import Poller
from pycultivator_lab.tools.client.view.main import MainView


class MainController(controller.ViewController):

    VIEW_CLASS = MainView

    def __init__(self, parent, view=None):
        super(MainController, self).__init__(parent, view=view)
        self._task_poller = TaskStatusPoller(self)
        self.task_poller.interval = 0.5

    @property
    def task_poller(self):
        """Return Polling agent

        :return:
        :rtype: TaskStatusPoller
        """
        return self._task_poller

    def _prepare(self):
        super(MainController, self)._prepare()
        if self.has_view():
            self.application.add_observer(self.view)
            self.application.connection.add_observer(self.view)

    def _stop(self):
        if self.has_view():
            self.application.remove_observer(self.view)
            self.application.connection.remove_observer(self.view)
        if self.task_poller.is_polling():
            self.task_poller.stop()
        self.parent.stop()
        super(MainController, self)._stop()

    def select_server(self):
        from pycultivator_lab.tools.client.dialog.list_servers import ServerDialogController
        c = ServerDialogController(self, model=self.application.servers)
        result = c.start()
        return result

    def connect_server(self, server_uri=None):
        result = False
        if server_uri is None:
            server_uri = self.select_server()
        if server_uri is not None:
            connection = self.application.connection
            """:type: pycultivator_lab.tools.client.model.servers.ServerConnectionModel"""
            result = connection.connect(server_uri)
            # start poller for task status
            self.task_poller.start()
        return result

    def disconnect_server(self):
        self.task_poller.stop()
        connection = self.application.connection
        """:type: pycultivator_lab.tools.client.model.servers.ServerConnectionModel"""
        result = connection.disconnect()
        return result

    @receives("create_task")
    def event_create_task(self, event, source, data=None):
        self.create_task()

    def create_task(self):
        task = self.edit_task(None)
        if task is not None:
            self.application.connection.queued_tasks.add(task)

    def edit_task(self, task):
        from pycultivator_lab.tools.client.dialog.edit_task import TaskEditDialogController
        c = TaskEditDialogController(self)
        result = c.start(task)
        # if cancelled, return original task
        if result is None:
            result = task
        return result

    def schedule_task(self, task):
        result = True
        if task is None:
            import tkMessageBox
            tkMessageBox.showerror("Nothing selected", "No task selected")
            result = False
        if not self.application.connection.is_connected():
            import tkMessageBox
            tkMessageBox.showerror("Not Connected", "Connect to a Server first!")
            result = False
        if result:
            result = self.application.connection.schedule_task(task)
            if not result:
                import tkMessageBox
                tkMessageBox.showerror("Failed to Schedule", "Unable to schedule task.\nCheck server log.")

    def update_running_tasks(self):
        self.application.connection.update_running_tasks()

    def clear_task_queue(self):
        self.application.connection.clear_queued_tasks()

    def clear_task_running(self):
        self.application.connection.clear_running_tasks()

    def inspect_finished_task(self, task):
        from pycultivator_lab.tools.client.dialog.finished_task import FinishedTaskDialogController
        c = FinishedTaskDialogController(self)
        c.start(task)

    def clear_task_finished(self):
        self.application.connection.clear_finished_tasks()

    def open_console(self):
        from console import ConsoleController
        if not self.application.has_controller("console"):
            c = ConsoleController(self.application)
            self.application.add_controller("console", c)
        return self.application.get_controller("console").start()


class TaskStatusPoller(Poller):

    def execute(self):
        if self.application.is_connected():
            self.application.connection.update_running_tasks()
