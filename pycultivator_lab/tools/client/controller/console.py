
from pycultivator.core import pcLogger
from pycultivator_lab.tools.client.view.console import ConsoleView
from julesTk import controller


class ConsoleController(controller.ViewController):

    VIEW_CLASS = ConsoleView
    # class wide logger instance
    _log = None

    def __init__(self, parent):
        super(ConsoleController, self).__init__(parent=parent)
        self._handler = None

    @classmethod
    def getRootLog(cls):
        """Returns the root logger object used in this script

        :rtype: logging.Logger
        """
        return pcLogger.getLogger()

    @classmethod
    def getLog(cls):
        """Return the logger object of this class

        :return: The logger object connected to this class
        :rtype: logging.Logger
        """
        if cls._log is None:
            cls._log = pcLogger.createLogger(cls.__name__)
        return cls._log

    @property
    def handler(self):
        return self._handler

    def has_handler(self):
        return self.handler is not None

    def _start(self):
        super(ConsoleController, self)._start()
        # create handler and register stream output
        self._handler = pcLogger.UVAStreamHandler(self.view.get_widget("log"))
        # register handler to root logger
        self.getRootLog().addHandler(self._handler)
        self.view.get_variable("log_level").set(self.getLog().getEffectiveLevel())

    def set_log_level(self, level):
        self.getRootLog().level = level
        if self.has_handler():
            self.handler.level = level

    def _stop(self):
        if self.application.has_controller("console"):
            self.application.remove_controller("console")
        super(ConsoleController, self)._stop()
