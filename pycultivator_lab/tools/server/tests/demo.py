"""Small demo utility"""

from pycultivator.core import pcLogger
from pycultivator_lab.tools.server.server import TaskServer
from pycultivator_lab.tasks import Task
from pycultivator_lab.tasks.conditions import TimeCondition
from datetime import datetime as dt, timedelta as td
import time


class HelloWorldTask(Task):

    def _execute(self):
        print("Hello World!")
        return True


if __name__ == "__main__":
    # connect log to console
    log = pcLogger.getLogger()
    log.setLevel(10)
    handler = pcLogger.UVAStreamHandler()
    handler.setLevel(10)
    log.addHandler(handler)
    # load server with 4 workers
    s = TaskServer(4)
    # start server
    s.start()
    # schedule 100 tasks
    for i in range(100):
        s.schedule_task(HelloWorldTask())
    # schedule 1 task for over 5 seconds
    s.queueTask(HelloWorldTask(
        conditions=TimeCondition(dt.now() + td(seconds=5)))
    )
    # print server information
    log.info(s.getServerInfo())
    # ask server to quit when ready
    s.stop()
    # now we print info on the server
    while s.isAlive():
        log.info(s.getServerInfo())
        time.sleep(1)
