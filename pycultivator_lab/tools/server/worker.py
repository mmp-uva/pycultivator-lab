"""Workers are threads utilized by server to execute tasks for them"""

from pycultivator.core.objects import PCObject
from pycultivator.core import pcException
from pycultivator_lab.tasks import *
import threading, sys

if sys.version_info[0] < 3:
    import Queue
else:
    import queue as Queue

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = ["Worker", "WorkerException"]


class Worker(threading.Thread, PCObject):
    """A small object executing any task queued to it"""

    def __init__(self, parent):
        super(Worker, self).__init__()
        from server import TaskServer
        if not isinstance(parent, TaskServer):
            raise ValueError("A Worker object needs a Server object as parent")
        # parent object which has all resources
        self._parent = parent
        # stop event: graceful stop
        self._stop_event = threading.Event()
        # halt event: quit immediately
        self._halt_event = threading.Event()

    def getParent(self):
        return self._parent

    @property
    def parent(self):
        return self.getParent()

    # actions

    def run(self):
        """Start the server"""
        self._resetEvents()
        # execute tasks until we are told
        while not self._canStop() and not self._mustStop():
            try:
                task = self._get_task()
                if task is not None:
                    self._run_task(task)
                    self._report_task(task)
            except KeyboardInterrupt:
                self.terminate()
                raise
        if self._mustStop():
            self.getLog().info("Forced to quit, please wait a bit so I can clean")
        self.clean()

    def _get_task(self):
        task = None
        try:
            task = self.parent.getTaskQueue().get(timeout=1)
        except Queue.Empty:
            pass
        return task

    def _run_task(self, task):
        """Runs the task in a sandbox

        :param task:
        :type task: pycultivator_lab.server.task.Task
        :return:
        :rtype: bool
        """
        results = []
        result = False
        try:
            results = task.start()
            self._process_results(results)
            result = True
        except TaskException:
            pass
        except Exception:
            pass
        return result

    def _process_results(self, results):
        import collections
        if isinstance(results, collections.Iterable):
            for result in results:
                if isinstance(result, Task):
                    # queue this task
                    self.parent.queueTask(result)
        elif isinstance(results, Task):
            self.parent.queueTask(results)

    def _report_task(self, task):
        # report task done
        self.parent.getTaskQueue().task_done()
        self.parent.finishTask(task)

    def clean(self):
        self.getLog().info("Worker is exiting...")
        return True

    def stop(self, graceful=True, blocking=True):
        """Inform server to stop"""
        if graceful:
            self._stop_event.set()
        else:
            self.terminate()
        if blocking:
            self.join()

    def terminate(self):
        self._stop_event.set()
        self._halt_event.set()

    def _resetEvents(self):
        """Reset all events"""
        self._stop_event.clear()
        self._halt_event.clear()

    def _mayStop(self):
        """Whether the stop event has been triggered"""
        return self._stop_event.isSet()

    def _mustStop(self):
        """Whether the halt event has been triggered"""
        return self._halt_event.isSet()

    def _canStop(self):
        """Whether the worker can exit"""
        return self._mayStop()


class WorkerException(pcException.PCException):
    """Exception raised by a worker"""

    def __init__(self, msg):
        super(WorkerException, self).__init__(msg=msg)
