"""To execute a pyCultivator Task Server

The script will automatically set-up and start a pycultivator server instance.

"""

from pycultivator.core.objects import PCObject
from pycultivator_lab.scripts.meta import ConfigurableMetaScript, ScriptAttribute, ScriptCountingAttribute
from pycultivator_lab.scripts.extras.ini_config import INIConfig
from server import *
import threading
import Pyro4
import os

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = [
    "ServerScript", "ServerWatcher"
]


# a small script class for running and starting a server
class ServerScript(ConfigurableMetaScript):

    ini_file = ScriptAttribute(str, default=None, aliases=["ini"], is_required=False)
    root_dir = ScriptAttribute(str, default=None, is_required=False)
    config_dir = ScriptAttribute(str, default=None, is_required=False)
    data_dir = ScriptAttribute(str, default=None, is_required=False)

    workers = ScriptAttribute(int, default=1, is_required=False)
    # reporting interval
    report_interval = ScriptAttribute(
        int, default=5*60, aliases=["interval"], is_required=False,
        help="How often should reports be published about server performance? (in seconds, default=5*60)"
    )
    verbose = ScriptCountingAttribute(default=0, multiplier=10, aliases=['v'], is_required=False)

    _namespace = "script.tasks.server"
    _default_settings = {
        "root.dir": None,
        "config.dir": None,
        "log.file.state": False,
        "log.file.dir": None,
        "data.dir": None,
        "heartbeat.path": None,
        "heartbeat.name": "pycultivator-servers.lib"
    }

    def __init__(self, *args, **kwargs):
        super(ServerScript, self).__init__(*args, **kwargs)
        self._ini = None
        self._server = None
        self._watcher = None
        self._pyro_daemon = None
        self._pyro_uri = None

    def get_root_dir(self):
        result = self.getSetting("root.dir")
        if not isinstance(result, str) or not os.path.exists(result):
            result = os.getcwd()
        return result

    def get_library_path(self):
        result = self.getSetting("heartbeat.path")
        if result is None:
            result = os.path.join(self.get_root_dir(), self.get_library_name())
        return result

    def get_library_name(self):
        return self.getSetting("heartbeat.name")

    def get_server(self):
        """Server instance created by this script

        :return:
        :rtype: None | pycultivator_lab.tools.server.server.TaskServer
        """
        return self._server

    def has_server(self):
        return self.get_server() is not None

    @property
    def server(self):
        return self.get_server()

    def get_watcher(self):
        """Return the Server Watcher used to report about server performance

        :return:
        :rtype: ServerWatcher
        """
        return self._watcher

    def has_watcher(self):
        return self.get_watcher() is not None

    @property
    def server_watcher(self):
        return self.get_watcher()

    def get_pyro_daemon(self):
        return self._pyro_daemon

    def has_pyro_daemon(self):
        return self.get_pyro_daemon() is not None

    @property
    def pyro_daemon(self):
        return self.get_pyro_daemon()

    def get_pyro_uri(self):
        return self._pyro_uri

    def has_pyro_uri(self):
        return self.get_pyro_uri() is not None

    @property
    def pyro_uri(self):
        return self.get_pyro_uri()

    def prepare(self):
        # load ini settings
        if self.ini_file is not None:
            settings = INIConfig.load_ini(self.ini_file)
            self.settings.update(settings)
        if self.root_dir is not None:
            self.setSetting("root.dir", self.root_dir)
        if self.config_dir is not None:
            self.setSetting("config.dir", self.config_dir)
        # load specific attributes
        if self.verbose > 0:
            level = 50 - self.verbose
            if level < 0:
                level = 0
            self.connect_console_log(level)
            if self.getSetting("log.file.state", default=False) is True:
                self.connect_file_log(level=level)
        # create server
        self._server = TaskServer(self.workers)
        # start server
        self.server.start()
        # create watcher
        self._watcher = ServerWatcher(self.server, self.report_interval)
        # start watcher
        self.server_watcher.start()
        return self.server.isAlive()

    def has_heartbeat(self, location=None, pid=None):
        if location is None:
            location = self.get_library_path()
        if pid is None:
            pid = os.getpid()
        heartbeats = self.read_heartbeats(location=location)
        return str(pid) in heartbeats.keys()

    def read_heartbeats(self, location=None):
        results = {}
        if location is None:
            location = self.get_library_path()
        if os.path.exists(location):
            with open(location, "r") as src:
                for line in src:
                    pid, uri = "", ""
                    values = line.split(" ")
                    if len(values) >= 2:
                        pid, uri = values[:2]
                        pid.strip()
                        uri.strip()
                    if "" not in (pid, uri):
                        results[pid] = uri
        return results

    def clean_heartbeats(self, heartbeats):
        results = {}
        for pid, uri in heartbeats.items():
            if self.is_heartbeat_alive(pid):
                results[pid] = uri
        return results

    def is_heartbeat_alive(self, pid):
        result = False
        try:
            os.kill(int(pid), 0)
            result = True
        except ValueError:
            pass
        except OSError:
            pass
        return result

    def register_heartbeat(self, location=None, pid=None):
        if location is None:
            location = self.get_library_path()
        if pid is None:
            pid = os.getpid()
        # first read heartbeats
        heartbeats = self.read_heartbeats(location=location)
        # now add our own
        heartbeats[pid] = self.get_pyro_uri()
        return self.write_heartbeats(heartbeats, location=location)

    def unregister_heartbeat(self, location=None, pid=None):
        if location is None:
            location = self.get_library_path()
        if pid is None:
            pid = os.getpid()
        # first read heartbeats
        heartbeats = self.read_heartbeats(location=location)
        if pid in heartbeats.keys():
            heartbeats.pop(pid)
        return self.write_heartbeats(heartbeats, location=location)

    def write_heartbeats(self, heartbeats, location=None, clean=True):
        result = False
        if location is None:
            location = self.get_library_path()
        if clean:
            heartbeats = self.clean_heartbeats(heartbeats)
        if os.path.exists(os.path.dirname(location)):
            try:
                with open(location, "w") as dest:
                    for pid, uri in heartbeats.items():
                        dest.write("{} {}".format(pid, uri))
                result = True
            except IOError:
                self.getLog().error("Unable to write heartbeat")
        return result

    def connect_file_log(self, location=None, level=0):
        if location is None:
            log_dir = self.getSetting("log.file.dir", default=None)
            if log_dir is not None:
                # take file name, remove extension
                log_file = os.path.splitext(__file__)[0]
                location = os.path.join(log_dir, "{}.log".format(log_file))
        if isinstance(location, str) and os.path.exists(os.path.dirname(location)):
            from pycultivator.core import pcLogger
            handler = pcLogger.UVARotatingFileHandler(location)

    def connect_console_log(self, level=0):
        from pycultivator.core import pcLogger
        handler = pcLogger.UVAStreamHandler()
        handler.setLevel(level)
        self.getRootLog().addHandler(handler)
        if self.getRootLog().getEffectiveLevel() > level:
            self.getRootLog().setLevel(level)
        self.getLog().debug("Logger is now connected to the console")

    def execute(self):
        self.info("Start Pyro4 Daemon...")
        self._pyro_daemon = Pyro4.Daemon()
        self._pyro_uri = self.pyro_daemon.register(self, objectId="pycultivator.server")
        self.register_heartbeat()
        self.info(
            "\n{attention}\n\tPyro4 Server URI: '{uri}'\n{attention}".format(
                uri=self._pyro_uri,
                attention="{:#<50s}".format("")
            )
        )
        self.info("Waiting for tasks to execute...")
        self.pyro_daemon.requestLoop()
        return True

    @Pyro4.expose
    def echo(self, msg):
        return "You said: {}".format(msg)

    @Pyro4.expose
    def request_task_id(self):
        """Obtain a Task ID for a task without scheduling it"""
        return self.server.requestTaskID()

    @Pyro4.expose
    def schedule_task(self, task_name, arguments=None, conditions=None, block=False):
        """Schedule a task on the server and return ID of Task

        :return: None if no task was loaded, positive integer is ID, -1 when not all attributes were set
        :rtype: None or int
        """
        result = False
        task = self.load_task(task_name, arguments=arguments, conditions=conditions)
        if task is not None:
            if task.areAttributesFilled():
                result = self.server.queueTask(task)
                if result:
                    result = task.getIdentification()
                self.info("Scheduled task to start... ID = {}".format(result))
                if block:
                    task.waitFinished()
                    result = task.isSuccessful()
            else:
                self.getLog().warning("Not all required attributes of task {} were set".format(task_name))
                result = False
        else:
            self.getLog().warning("Could not find task with name {}".format(task_name))
        return result

    def load_task(self, task_name, arguments=None, conditions=None):
        """Loads a task class from the task library and creates an instance

        :param task_name:
        :type task_name: str
        :return: Task instance or None
        :rtype: None | pycultivator_lab.tasks.Task
        """
        result = None
        tasks = self.find_tasks()
        if not task_name.startswith("pycultivator_lab.tasks"):
            task_name = "pycultivator_lab.tasks.{}".format(task_name)
        if task_name in tasks.keys():
            task_class = tasks[task_name]
            result = task_class(attributes=arguments, conditions=conditions)
        return result

    def find_tasks(self, force=False):
        from pycultivator_lab.tasks import find_tasks
        return find_tasks(force=force)

    @Pyro4.expose
    def list_available_tasks(self, force=False):
        results = {}
        tasks = self.find_tasks(force=force)
        for key, task in tasks.items():
            results[key] = {"name": task.name, "description": task.to_arguments_string()}
        return results

    @Pyro4.expose
    def has_task_id(self, task_id):
        """Whether the server has a task with the given ID"""
        return self.server.isTaskRegistered(task_id)

    @Pyro4.expose
    def is_task_finished(self, task_id):
        """Returns whether a task with the given id is running"""
        return self.server.isTaskFinished(task_id)

    @Pyro4.expose
    def collect_task(self, task_id):
        """Returns the task result or None if task is not finished

        :return: Dictionary with all the information about the task
        :rtype: None | dict[str, object]
        """
        result = None
        task = None
        if self.server.isTaskWaiting(task_id):
            task = self.server.collectWaitingTask(task_id)
        if self.server.isTaskFinished(task_id):
            task = self.server.collectFinishedTask(task_id)
        if task is not None:
            result = self.serialize_task_result(task)
        return result

    @staticmethod
    def serialize_task_result(task):
        return {
            "t_start": task.getTimeStart(),
            "t_finish": task.getTimeFinished(),
            "success": task.isSuccessful(),
            "finished": task.isFinished(),
            "result": task.result,
        }

    @Pyro4.expose
    def clear_finished_tasks(self):
        """Clears the finished tasks list"""
        self.server.clearFinishedTasks()

    @Pyro4.expose
    def clear_resources(self):
        self.server.clearResources()

    @Pyro4.expose
    def inform(self):
        """Inform user about performance"""
        if self.has_server():
            result = self.server.getServerInfo()
        else:
            result = "No server defined..."
        return result

    @Pyro4.expose
    def shutdown(self):
        if self.has_pyro_daemon():
            self.info("Shutdown Pyro4 Daemon...")
            self.pyro_daemon.shutdown()

    def clean(self):
        # remove heartbeat
        self.unregister_heartbeat()
        if self.has_pyro_daemon():
            self.shutdown()
        if self.has_server() and self.server.isAlive():
            self.server.stop()
            # block until server quit
            self.server.join()
        if self.has_watcher() and self.server_watcher.isAlive():
            self.server_watcher.stop()
            # block until watcher quit
            self.server_watcher.join()
        return True


class ServerWatcher(threading.Thread, PCObject):

    def __init__(self, server, interval):
        super(ServerWatcher, self).__init__()
        self._server = server
        self._interval = interval
        self._stop_event = threading.Event()

    def get_server(self):
        return self._server

    @property
    def server(self):
        return self.get_server()

    def get_interval(self):
        return self._interval

    @property
    def interval(self):
        return self._interval

    def run(self):
        self.getLog().info("Server watcher started...")
        while not self.must_stop():
            self.report()
            self._stop_event.wait(self.interval)
        self.getLog().info("Server watcher finished...")

    def report(self):
        self.getLog().info(
            self.server.getServerInfo()
        )

    def stop(self):
        self._stop_event.set()

    def must_stop(self):
        return self._stop_event.isSet()


# entry-point for console script
def main():
    script = ServerScript()
    # start script
    script.run()


if __name__ == "__main__":
    main()
