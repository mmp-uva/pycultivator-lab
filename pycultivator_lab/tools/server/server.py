"""The server manages resources and executes tasks in threaded workers

NOTE: Be aware that due to the Global Interpreter Lock (GIL) multi-threading in CPython is not true parallel.
The GIL exists because two threads within the same process need to access the same interpreter, causing conflicts.
GIL is designed to prevent this by allowing only 1 thread at the time to access the interpreter (virtually making it
single threaded). This problem is specific to the C implementation of Python.
Other implementations like IronPython or Jython don't have this problem.

Using a multi-process set-up may be an solution, but involves copying the pycultivator objects back and forth
between processes. This copying is deemed more expensive than what benefit comes from using multiple processes (NOT
TESTED).

Also the currently common usage of pycultivator in the lab is only with a few experiments running in parallel and
it's expected that in many cases the tasks will be I/O-bound, rather than CPU-bound. Threads are a good solution for
these tasks as threads do help alleviate I/O bound applications, by allowing the program to continue with other tasks
while waiting for a task to finish while it communicates with a device (I/O-bound).

In this setup the server is the TaskHost and NOT the worker, as the server is responsible for managing the resources.
"""

from pycultivator_lab.tasks.host import *
from worker import *
import threading, sys

if sys.version_info[0] < 3:
    import Queue
else:
    import queue as Queue

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = [
    "TaskServer", "TaskServerException",
]


class TaskServer(TaskHost, threading.Thread):
    """Manage resources and execute tasks"""

    def __init__(self, worker_pool_size=1):
        """Initialize server"""
        TaskHost.__init__(self)
        threading.Thread.__init__(self)
        # stop event: graceful stop
        self._stop_event = threading.Event()
        # halt event: quit immediately
        self._halt_event = threading.Event()
        # update event: something has changed
        self._update_event = threading.Event()
        # number of workers created by the server
        self._worker_pool_size = worker_pool_size
        # list of workers executing tasks for the server
        self._worker_pool = []
        # safety lock for anything related to worker management
        self._workers_lock = threading.RLock()
        # task scheduled for workers
        self._tasks_queue = Queue.Queue()
        # lock to be acquired before messing with task list
        self._tasks_lock = threading.RLock()
        # lock to be acquired before messing with the resource dictionary (i.e. retrieving, adding and removing)
        self._resource_lock = threading.RLock()

    # worker handlers

    def countWorkers(self):
        """Count the number of workers currently in the pool

        NOTE: Obtained value may not reflect actual pool size (as other threads may change the pool size).
        """
        result = 0
        with self._workers_lock:
            result = len(self._worker_pool)
        return result

    def getWorkerPoolSize(self):
        """Return the current required worker pool size

        NOTE: This does not necessarily equate to the actual pool size.
        """
        result = 0
        with self._workers_lock:
            result = self._worker_pool_size
        return result

    def setWorkerPoolSize(self, n):
        """Resize the worker pool size"""
        with self._workers_lock:
            self._worker_pool_size = n
            self.update()

    def _manageWorkerPool(self, max_rounds=50, graceful=True):
        """Will adjust the worker pool according to the pool size"""
        with self._workers_lock:
            count = 0
            # do this until pool size matches required size, or server is exiting
            while self.countWorkers() != self.getWorkerPoolSize():
                if self.countWorkers() > self.getWorkerPoolSize():
                    self._removeWorker(self.countWorkers() - 1, graceful=graceful)
                if self.countWorkers() < self.getWorkerPoolSize():
                    self._createWorker()
                # remove workers that are not alive
                self._cleanWorkers()
                count += 1
                if count > max_rounds:
                    break
            # return whether we were successful
            result = self.countWorkers() == self.getWorkerPoolSize()
        return result

    def _createWorker(self):
        # worker = None
        # if self.countWorkers() > 0 and self._shouldStop():
        #     self.getLog().info("Server is exiting and does not allow new workers to be spawned")
        # else:
        with self._workers_lock:
            worker = Worker(self)
            self._worker_pool.append(worker)
            worker.start()
        return worker

    def _removeWorker(self, index=-1, graceful=True):
        with self._workers_lock:
            # get worker
            worker = self._worker_pool[index]
            # ask the worker to stop
            # since worker is no longer in pool we do not push it to hurry
            worker.stop(graceful=graceful)
        return worker

    def _cleanWorkers(self):
        with self._workers_lock:
            new_workers = []
            for worker in self._worker_pool:
                if worker.isAlive():
                    new_workers.append(worker)
            self._worker_pool = new_workers

    def _terminateWorker(self, index=-1):
        with self._workers_lock:
            # remove worker from pool
            worker = self._worker_pool.pop(index)
            # force worker to quit
            worker.terminate()
        return worker

    # resource handlers

    def getResourceLock(self):
        """Return the lock needed before working with the resource dictionary"""
        return self._resource_lock

    def hasResource(self, name):
        """Determine if the server has a resource with the given name

        :return: Whether the resource is present
        :rtype: bool
        """
        result = False
        with self.getResourceLock():
            result = super(TaskServer, self).hasResource(name)
        return result

    def getResource(self, name):
        """Retrieve a resource from the server

        NOTE: Returns a Resource object (wrapping the resource), not the resource itself.

        :param name: Name of the resource requested
        :type name: str
        :return:
        :rtype: pycultivator_lab.resource.ReservableResource
        """
        result = None
        with self.getResourceLock():
            result = super(TaskServer, self).getResource(name)
        return result

    def addResource(self, name, resource):
        result = False
        with self.getResourceLock():
            result = super(TaskServer, self).addResource(name, resource)
        return result

    def removeResource(self, name):
        result = False
        with self.getResourceLock():
            result = super(TaskServer, self).removeResource(name)
        return result

    def reserveResource(self, name):
        """Increase resource usage with one

        The number of reservations made for one particular resource informs the server about whether the resource is
        still needed. When the number of reservations reaches zero, the server may remove the resource.
        """
        with self.getResourceLock():
            super(TaskServer, self).reserveResource(name)

    def finishResource(self, name):
        """Decrease resource usage with one

        The number of reservations made for one particular resource informs the server about whether the resource is
        still needed. When the number of reservations reaches zero, the server may remove the resource.
        """
        with self.getResourceLock():
            super(TaskServer, self).finishResource(name)

    def countResources(self):
        result = 0
        with self.getResourceLock():
            result = super(TaskServer, self).countResources()
        return result

    def clearResources(self):
        result = 0
        with self.getResourceLock():
            result = super(TaskServer, self).clearResources()
        return result

    def _manageResources(self):
        # overload for better resource management
        self.clearResources()

    # task handlers

    def countWaitingTasks(self):
        result = 0
        with self._tasks_lock:
            result = super(TaskServer, self).countWaitingTasks()
        return result

    def queueTask(self, task):
        """Ask server to execute a task"""
        result = False
        if self._shouldStop():
            self.getLog().info("Server is asked to quit and therefore does not accept new tasks")
        else:
            with self._tasks_lock:
                result = super(TaskServer, self).queueTask(task)
        return result

    def isTaskIDAvailable(self, task_id):
        result = False
        with self._tasks_lock:
            result = super(TaskServer, self).isTaskIDAvailable(task_id)
        return result

    def _registerTaskResource(self, task, resource):
        result = False
        with self._resource_lock:
            result = super(TaskServer, self)._registerTaskResource(task, resource)
        return result

    def _manageTasks(self):
        """Iterate over tasks and check whether they are ready to be executed"""
        scheduled_count = 0
        with self._tasks_lock:
            scheduled_count = super(TaskServer, self)._manageTasks()
        return scheduled_count

    def finishTask(self, task):
        with self._tasks_lock:
            super(TaskServer, self).finishTask(task)

    def _runTask(self, task):
        """Queues the task for execution by workers"""
        self.getTaskQueue().put(task)
        return True

    def isTaskRegistered(self, task_id):
        """Whether the task is either waiting or finished

        If False it can be that a worker is currently running the task.

        """
        result = False
        with self._tasks_lock:
            result = super(TaskServer, self).isTaskRegistered(task_id)
        return result

    def isTaskWaiting(self, task_id):
        result = False
        with self._tasks_lock:
            result = super(TaskServer, self).isTaskWaiting(task_id)
        return result

    def collectWaitingTask(self, task_id):
        result = None
        with self._tasks_lock:
            result = super(TaskServer, self).collectWaitingTask(task_id)
        return result

    def isTaskFinished(self, task_id):
        result = False
        with self._tasks_lock:
            result = super(TaskServer, self).isTaskFinished(task_id)
        return result

    def collectFinishedTask(self, task_id):
        result = None
        with self._tasks_lock:
            result = super(TaskServer, self).collectFinishedTask(task_id)
        return result

    def getFinishedTaskPoolSize(self):
        result = None
        with self._tasks_lock:
            result = super(TaskServer, self).getFinishedTaskPoolSize()
        return result

    def _manageFinishedTasks(self):
        with self._tasks_lock:
            super(TaskServer, self)._manageFinishedTasks()

    def getTaskQueue(self):
        """Tasks queued for execution by the workers"""
        return self._tasks_queue

    def countFinishedTasks(self):
        result = 0
        with self._tasks_lock:
            result = super(TaskServer, self).countFinishedTasks()
        return result

    def countQueuedTasks(self):
        return self.getTaskQueue().qsize()

    def countRunningTasks(self):
        return self.getTaskQueue().unfinished_tasks - self.countQueuedTasks()

    def countUnFinishedTasks(self):
        """All tasks waiting for execution"""
        return self.countWaitingTasks() + self.countQueuedTasks() + self.countRunningTasks()

    # actions

    def run(self):
        """Run the server in a separate thread"""
        self._resetEvents()
        # execute tasks until we are told
        self.getLog().info("Server started...")
        while not self._canStop() and not self._mustStop():
            # reset update trigger
            self._update_event.clear()
            try:
                # manage pool size (create or remove workers if necessary)
                self._manageWorkerPool()
                # check all tasks on readiness
                self._manageTasks()
                # check resources on usage
                self._manageResources()
                # wait for 0.5 second or until notified
                self._update_event.wait(0.5)
            except KeyboardInterrupt:
                self.terminate()
                raise
        if self._mustStop():
            # number of tasks waiting + tasks queued + tasks running (unfinished)
            self.getLog().info(
                "Forced to quit: left {} jobs unprocessed".format(self.countUnFinishedTasks())
            )
        self._clean()
        self.getLog().info("Server finished...")

    def _clean(self):
        """Cleans up

        - Terminate all workers
        - Disconnect any resources left connected
        """
        # inform world about exit
        self.getLog().info("Server is exiting...")
        # set pool to zero
        self.setWorkerPoolSize(0)
        # remove all workers gracefully
        result = self._manageWorkerPool()
        if not result:
            self.getLog().info("Failed to stop workers gracefully, now terminate workers")
            self._manageWorkerPool(graceful=False)

    # event handlers

    def update(self):
        """Triggers the update event"""
        self._update_event.set()

    def stop(self, graceful=True):
        """Inform server to stop"""
        if graceful:
            self._stop_event.set()
        else:
            self.terminate()
        return True

    def terminate(self):
        # set stop event in case terminate was called directly
        self._stop_event.set()
        self._halt_event.set()

    def _resetEvents(self):
        """Reset all events"""
        self._stop_event.clear()
        self._halt_event.clear()

    def _shouldStop(self):
        """Whether the stop event has been triggered"""
        return self._stop_event.isSet()

    def _mustStop(self):
        """Whether the halt event has been triggered"""
        return self._halt_event.isSet()

    def _canStop(self):
        """Whether the server is asked to stop and all tasks are finished"""
        return self._shouldStop() and self.countUnFinishedTasks() == 0

    # information

    def getServerInfo(self):
        lines = ["", "##  Server Information  ##"]
        lines += self.getWorkerInfo() + self.getTaskInfo() + self.getResourceInfo()
        return "\n".join(lines)

    def getWorkerInfo(self):
        lines = [
            "### Worker information",
            "- {} worker(s) running (pool = {})".format(self.countWorkers(), self.getWorkerPoolSize()),
        ]
        return lines

    def getTaskInfo(self):
        lines = [
            "### Task information",
            "- {} task(s) waiting".format(self.countWaitingTasks()),
            "- {} task(s) queued".format(self.countQueuedTasks()),
            "- {} task(s) running".format(self.countRunningTasks()),
            "- {} task(s) finished".format(self.countFinishedTasks()),
        ]
        return lines

    def getResourceInfo(self):
        lines = [
            "### Resource information",
            "- {} resource(s) loaded".format(self.countResources())
        ]
        return lines


class TaskServerException(TaskHostException):
    """Exception raised by server instances"""

    def __init__(self, msg):
        super(TaskServerException, self).__init__(msg=msg)
