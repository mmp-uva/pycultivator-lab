"""
Module for testing the pyCultivator XML Configuration Objects
"""

import test_baseConfig
from pycultivator_lab.config import xmlConfig
from pycultivator.config.tests import test_xmlConfig
from pycultivator.core import pcXML
from unittest2 import SkipTest
import os

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

SCHEMA_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "schema")
CONFIG_PATH = os.path.join(SCHEMA_DIR, "configuration.xml")
SCHEMA_PATH = os.path.join(SCHEMA_DIR, "pycultivator_lab.xsd")


class TestXMLConfig(test_xmlConfig.TestXMLConfig, test_baseConfig.TestConfig):
    """Class for testing the configuration class"""

    _abstract = False
    _subject_cls = xmlConfig.XMLConfig
    schema_path = SCHEMA_PATH
    config_location = CONFIG_PATH

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass().load(self.getConfigLocation())

    def getSubject(self):
        """Return reference to the instance of the class subjected to this test

        :rtype: pycultivator.config.xmlConfig.XMLConfig
        """
        return test_baseConfig.TestConfig.getSubject(self)

    def test_load(self):
        """Test opening a XMLConfiguration file"""
        self.assertIsNotNone(
            self.getSubjectClass().load(self.getConfigLocation())
        )
        self.assertIsNone(
            self.getSubjectClass().load("bla.xml")
        )

    def test_check(self):
        self.assertTrue(
            self.getSubjectClass().check(self.getConfigLocation())
        )


class TestXMLObjectConfig(test_baseConfig.test_baseConfig.TestObjectConfig):
    """Class for testing XMLObjectConfig Object"""

    _abstract = True
    _subject_cls = xmlConfig.xmlConfig.XMLObjectConfig
    _config_cls = xmlConfig.XMLConfig
    config_location = CONFIG_PATH

    @classmethod
    def getConfguration(cls):
        result = xmlConfig.XMLConfig.load(cls.getConfigLocation())
        if result is None:
            raise ValueError("Unable to load configuration")
        return result

    def getDefaultObject(self):
        return self.getSubject().loadDefault()

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator_lab.config.xmlConfig.XMLExperimentConfig
        """
        return super(TestXMLObjectConfig, self).getSubject()

    def initSubject(self, *args, **kwargs):
        c = self.getConfguration()
        return self.getSubjectClass()(c)

    def test_cleanTag(self):
        self.assertEqual(self.getSubject().cleanTag("a"), "a")
        self.assertEqual(self.getSubject().cleanTag("<a>a"), "<a>a")
        self.assertEqual(self.getSubject().cleanTag("{a}a"), "a")
        self.assertEqual(self.getSubject().cleanTag("{Ab2}a"), "a")
        self.assertEqual(self.getSubject().cleanTag("{}a"), "a")
        self.assertEqual(self.getSubject().cleanTag(" {A2b}a "), "a")
        self.assertEqual(self.getSubject().cleanTag(" {A2b}A "), "a")

    def test_find(self):
        raise SkipTest("Not implemented yet")

    def test_read(self):
        raise SkipTest("Not implemented yet")

    def test_save(self):
        raise SkipTest("Not implemented yet")

    def test_write(self):
        raise SkipTest("Not implemented yet")


class TestXMLExperimentConfig(TestXMLObjectConfig, test_baseConfig.TestExperimentConfig):
    """Test XMLExperimentConfig class"""

    _abstract = False
    _subject_cls = xmlConfig.XMLExperimentConfig

    def test_isValidElementTag(self):
        e = pcXML.et.XML("<experiment></experiment>")
        self.assertTrue(self.getSubject().isValidElementTag(e))
        e = pcXML.et.XML("<bla></bla>")
        self.assertFalse(self.getSubject().isValidElementTag(e))
        root = self.getConfguration().getXML().getRoot()
        self.assertTrue(self.getSubject().isValidElementTag(root))

    def test_read(self):
        root = self.getConfguration().getXML().getRoot()
        config = self.getSubject().read(root)
        from pycultivator_lab.experiment import Experiment
        self.assertIsInstance(config,  Experiment)


class TestXMLProtocolConfig(TestXMLObjectConfig, test_baseConfig.TestProtocolConfig):
    """Class for testing the XMLPartConfig class"""

    _abstract = False
    _subject_cls = xmlConfig.XMLProtocolConfig

    def test_isValidElementTag(self):
        e = pcXML.et.XML("<protocol></protocol>")
        self.assertTrue(self.getSubject().isValidElementTag(e))
        e = pcXML.et.XML("<bla></bla>")
        self.assertFalse(self.getSubject().isValidElementTag(e))

