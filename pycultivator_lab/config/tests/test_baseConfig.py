"""
Module for testing the pycultivator Configuration class
"""

from pycultivator.config.tests import test_baseConfig
from pycultivator_lab.config import baseConfig

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TestConfig(test_baseConfig.TestConfig):
    """Class for testing the pycultivator Config class"""

    _abstract = True
    _subject_cls = baseConfig.Config

    def test_getHelperFor(self):
        # test using a string
        self.assertIsInstance(
            self.subject.getHelperFor("experiment"),
            baseConfig.ExperimentConfig
        )
        # test with invalid string
        from pycultivator.config.baseConfig import ConfigException
        with self.assertRaises(ConfigException):
            self.subject.getHelperFor("foo")
        # test with class
        # self.assertIsInstance(
        #     self.subject.getHelperFor(baseConfig.device.ComplexDevice),
        #     baseConfig.ComplexDeviceConfig
        # )
        # test with class
        self.assertIsInstance(
            self.subject.getHelperFor(baseConfig.Experiment),
            baseConfig.ExperimentConfig
        )


class TestExperimentConfig(test_baseConfig.TestObjectConfig):
    """Test the experiment config"""

    _abstract = True
    _subject_cls = baseConfig.ExperimentConfig


class TestProtocolConfig(test_baseConfig.TestObjectConfig):
    """Class for testing the pycultivator PartConfig class"""

    _abstract = True
    _subject_cls = baseConfig.ProtocolConfig


class TestResourceConfig(test_baseConfig.TestObjectConfig):
    """Test the pycultivator BaseDeviceConfig class"""

    _abstract = True
    _subject_cls = baseConfig.ResourceConfig

