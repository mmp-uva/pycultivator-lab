
from pycultivator.config import xmlConfig
import baseConfig
import os


def check_source(source):
    if not isinstance(source, XMLConfig):
        raise XMLConfigException("Expected a pycultivator_lab XML Configuration Source")


class XMLBaseTaskConfig(xmlConfig.XMLObjectConfig, baseConfig.BaseTaskConfig):
    """Helper for configuration of task objects from XML"""

    XML_ELEMENT_TAG = "task"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLBaseTaskConfig, cls).assert_source(source, expected=expected)

    def read_properties(self, definition, obj):
        """Read the properties of the experiment

        :param definition: Definition to write properties to
        :type definition: lxml.etree._Element._Element
        :param obj: Object to read properties from
        :type obj: pycultivator_lab.protocol.experiment.Experiment
        :return: The updated object
        :rtype: pycultivator_lab.protocol.experiment.Experiment
        """
        obj = super(XMLBaseTaskConfig, self).read_properties(definition, obj)
        """:type: pycultivator_lab.protocol.experiment.Experiment"""
        state = self.xml.getElementActive(definition)
        obj.setActive(state)
        weight = self.xml.getElementAttribute(definition, "weight", _type=int, default=0)
        obj.weight = weight
        return obj

    def write_properties(self, obj, definition):
        """Write the properties of the experiment

        :param obj: Object to read properties from
        :type obj: pycultivator_lab.protocol.experiment.Experiment
        :param definition: Definition to write properties to
        :type definition: lxml.etree._Element._Element
        :return: Whether this was successful
        :rtype: bool
        """
        result = super(XMLBaseTaskConfig, self).write_properties(obj, definition)
        if result:
            self.xml.setElementActive(definition, "state", obj.is_active)
            self.xml.setElementAttribute(definition, "weight", obj.weight)
        return result


class XMLExperimentConfig(xmlConfig.XMLObjectConfig, baseConfig.ExperimentConfig):
    """Helper for configuration of experiment objects"""

    XML_ELEMENT_TAG = "experiment"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLExperimentConfig, cls).assert_source(source, expected=expected)

    def initialize(self, definition, class_, **kwargs):
        id_ = self.xml.getElementId(definition, default="0")
        active = self.xml.getElementActive(definition)
        return class_(id_, active=active)

    def read_properties(self, definition, obj):
        """Read property information from configuration source

        :type definition: lxml.etree._Element._Element
        :type obj: pycultivator_lab.protocol.experiment.Experiment
        :rtype: pycultivator_lab.protocol.experiment.Experiment
        """
        name = self.xml.getElementId(definition, default="0")
        obj.name = name
        active = self.xml.getElementActive(definition)
        obj.is_active = active
        return obj

    def read_resources(self, definition, obj):
        root = self.find_element("resources", root=definition)
        if root is not None:
            obj = super(XMLExperimentConfig, self).read_resources(root, obj)
        return obj

    def read_protocols(self, definition, obj):
        # find protocols definition
        root = self.find_element("protocols", root=definition)
        if root is not None:
            obj = super(XMLExperimentConfig, self).read_protocols(root, obj)
        return obj

    def write_resources(self, obj, definition):
        root = self.find_element("resources", root=definition)
        if root is None:
            # create new instruments element
            root = self.getXML().addElement(definition, "resources")
        return super(XMLExperimentConfig, self).write_resources(obj, definition=root)

    def write_protocols(self, obj, definition):
        root = self.find_element("protocols", root=definition)
        if root is None:
            # create new instruments element
            root = self.getXML().addElement(definition, "protocols")
        return super(XMLExperimentConfig, self).write_protocols(obj, definition=root)


class XMLScheduleConfig(xmlConfig.XMLBaseObjectConfig, baseConfig.ScheduleConfig):

    XML_ELEMENT_TAG = "schedule"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLScheduleConfig, cls).assert_source(source, expected=expected)


class XMLProtocolConfig(XMLBaseTaskConfig, baseConfig.ProtocolConfig):
    """Helper for configuration of protocol objects"""

    XML_ELEMENT_TAG = "protocol"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLProtocolConfig, cls).assert_source(source, expected=expected)

    def initialize(self, definition, class_, **kwargs):
        id_ = self.xml.getElementId(definition, default="0")
        active = self.xml.getElementState(definition)
        return class_(id_, active=active, **kwargs)

    def read_properties(self, definition, obj):
        """Read property information from configuration source

        :type definition: lxml.etree._Element._Element
        :type obj: pycultivator_lab.protocol.base.Protocol
        :rtype: pycultivator_lab.protocol.base.Protocol
        """
        name = self.xml.getElementId(definition, default="0")
        obj.name = name
        active = self.xml.getElementActive(definition)
        obj.is_active = active
        return obj


class XMLResourceConfig(xmlConfig.XMLBaseObjectConfig, baseConfig.ResourceConfig):
    """Helper for the configuration of resource objects"""

    XML_ELEMENT_TAG = "resource"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLResourceConfig, cls).assert_source(source, expected=expected)

    def initialize(self, definition, class_, **kwargs):
        result = None
        id_ = self.xml.getElementId(definition)
        source = self.xml.getElementAttribute(definition, "source")
        from pycultivator_lab.resource import ResourceException
        # determine if the resource is not
        try:
            result = class_.load(id_, source)
        except ResourceException:
            self.getLog().warning("Unable to load resource {} from {}".format(id_, source))
        return result


class XMLConfig(xmlConfig.XMLConfig, baseConfig.Config):
    """ The config class provides the API to configure xml experiment configuration files

    Provides:
    - Loading of XML data source
    - Data checking
    - Element access

    """

    # a list of available helper classes
    _known_helpers = [
        XMLExperimentConfig,
        XMLScheduleConfig,
        XMLProtocolConfig,
        XMLResourceConfig,
        xmlConfig.XMLSettingsConfig
    ]

    default_settings = {
        # add default settings
    }

    XML_SCHEMA_COMPATIBILITY = {
        '1.0': 2,
        # version: level
        # level 2: full support
        # level 1: deprecated
        # level 0: not supported
    }

    # path to package contained schema
    XML_SCHEMA_PATH = os.path.join("schema", "pycultivator_lab.xsd")

    @classmethod
    def load(cls, path, settings=None, **kwargs):
        """Loads the configuration source into memory and creates a config object

        :param path: Location from which the configuration will be loaded
        :type path: str
        :param settings: Settings dictionary that should be loaded into the configuration
        :type settings: None or dict[str, object]
        :rtype: None or pycultivator_lab.config.xmlConfig.XMLConfig
        """
        return super(XMLConfig, cls).load(path, settings, **kwargs)


class XMLConfigException(baseConfig.ConfigException):
    """An exception raised by the pycultivator.config.XMLConfig.XMLConfig object"""

    def __init__(self, msg):
        super(XMLConfigException, self).__init__(msg)
