"""Configuration of protocol objects"""

from pycultivator.config import baseConfig
from pycultivator_lab.task import BaseTask
from pycultivator_lab.protocol import Protocol
from pycultivator_lab.experiment import Experiment
from pycultivator_lab.resource import Resource
from pycultivator_lab.core.schedule import Schedule


class BaseTaskConfig(baseConfig.ObjectConfig):
    """Helper for the configuration of task objects"""

    _configures = BaseTask
    _configures_types = {'task'}

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(BaseTaskConfig, cls).assert_source(source, expected=expected)


class ExperimentConfig(baseConfig.ObjectConfig):
    """Helper for configuration of experiment object"""

    _configures = Experiment
    _configures_types = {'experiment'}

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(ExperimentConfig, cls).assert_source(source, expected=expected)

    def read(self, definition, obj, parent=None, **kwargs):
        obj = super(ExperimentConfig, self).read(definition, obj)
        # read schedule information
        obj = self.read_schedule(definition, obj)
        # read resource information
        obj = self.read_resources(definition, obj)
        # read protocol information
        obj = self.read_protocols(definition, obj)
        return obj

    def read_schedule(self, definition, obj):
        """Read schedule information from the configuration source.

        :param definition:
        :type obj: pycultivator_lab.experiment.base.Experiment
        :return:
        """
        helper = self.source.getHelperFor("schedule")
        schedule = None
        if helper is not None:
            schedule = helper.load(root=definition, parent=obj)
            """:type: list[pycultivator_lab.core.schedule.Schedule]"""
        if schedule is not None:
            obj.schedule = schedule
        return obj

    def read_protocols(self, definition, obj):
        helper = self.source.getHelperFor("protocol")
        protocols = []
        if helper is not None:
            protocols = helper.load_all(root=definition, parent=obj)
            """:type: list[pycultivator_lab.protocol.base.Protocol]"""
        for p in protocols:
            obj.add(p)
        return obj

    def read_resources(self, definition, obj):
        """Read resource information from the configuration source

        :param obj: The experiment to retrieve protocol information from
        :type obj: pycultivator_lab.experiment.base.Experiment
        :param definition: The definition to write to
        :return: Whether this was successful
        :rtype: bool
        """
        helper = self.source.getHelperFor("resource")
        resources = []
        # TODO: implement registration of resources
        if helper is not None:
            resources = helper.load_all(root=definition, parent=obj)
            """:type: list[pycultivator_lab.resource.Resource]"""
        for r in resources:
            obj.resources.add(r.name, r)
        return obj

    def read_properties(self, definition, obj):
        obj = super(ExperimentConfig, self).read_properties(definition, obj)
        # read properties
        return obj

    def write(self, obj, definition):
        """Write experiment information to the configuration source

        :param obj: The experiment to retrieve protocol information from
        :type obj: pycultivator_lab.experiment.base.Experiment
        :param definition: The definition to write to
        :return: Whether this was successful
        :rtype: bool
        """
        result = super(ExperimentConfig, self).write(obj, definition)
        if result:
            self.write_protocols(obj, definition)
        return result

    def write_schedule(self, obj, definition):
        """Write the schedule information from the experiment to the configuration source

        :param obj: The experiment to retrieve protocol information from
        :type obj: pycultivator_lab.experiment.base.Experiment
        :param definition: The definition to write to
        :return: Whether this was successful
        :rtype: bool
        """
        helper = self.source.getHelperFor("schedule")
        """:type: pycultivator_lab.config.baseConfig.ScheduleConfig"""
        if helper is not None:
            helper.save(obj.schedule, root=definition)
        return obj

    def write_resources(self, obj, definition):
        """Write the resource information from the experiment to the configuration source

        :param obj: The experiment to retrieve protocol information from
        :type obj: pycultivator_lab.experiment.base.Experiment
        :param definition: The definition to write to
        :return: Whether this was successful
        :rtype: bool
        """
        helper = self.source.getHelperFor("resource")
        """:type: pycultivator_lab.config.baseConfig.ResourceConfig"""
        if helper is not None:
            for r in obj.resources:
                helper.save(r, root=definition)
        return obj

    def write_protocols(self, obj, definition):
        """Write the protocol information from the experiment to the configuration source

        :param obj: The experiment to retrieve protocol information from
        :type obj: pycultivator_lab.experiment.base.Experiment
        :param definition: The definition to write to
        :return:
        """
        helper = self.source.getHelperFor("protocol")
        """:type: pycultivator_lab.config.baseConfig.ProtocolConfig"""
        if helper is not None:
            for p in obj.protocols:
                helper.save(p, root=definition)
        return obj


class ScheduleConfig(baseConfig.BaseObjectConfig):
    """Helper for configuration of schedule objects"""

    _configures = Schedule
    _configures_types = {'schedule'}

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(ScheduleConfig, cls).assert_source(source, expected=expected)


class ProtocolConfig(BaseTaskConfig):
    """Helper for configuration protocol objects"""

    _configures = Protocol
    _configures_types = {'protocol'}

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(ProtocolConfig, cls).assert_source(source, expected=expected)

    def load_class(self, class_):
        """Search for protocol classes"""
        # search for registered protocol classes
        result = super(ProtocolConfig, self).load_class(class_)
        return result


class ResourceConfig(baseConfig.BaseObjectConfig):
    """Helper for configuration of resource objects"""

    _configures = Resource
    _configures_types = {'resource'}

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(ResourceConfig, cls).assert_source(source, expected=expected)


class RelationConfig(baseConfig.PartConfig):
    """Helper for configuration of relationships between instruments"""

    _configures = None
    _configures_types = {'relation'}

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(RelationConfig, cls).assert_source(source, expected=expected)


class Config(baseConfig.Config):
    """Manage a configuration source and it's helpers"""

    _known_helpers = [
        ExperimentConfig,
        ProtocolConfig,
        ResourceConfig,
        RelationConfig,
        baseConfig.PolynomialConfig,
        baseConfig.SettingsConfig,
    ]

    default_settings = {
        # default settings for this class
    }

    def __init__(self, settings=None, **kwargs):
        super(Config, self).__init__(settings, **kwargs)

    @classmethod
    def load(cls, location, settings=None, **kwargs):
        """ABSTRACT Loads the source into memory and creates the config object

        :param location: Location from which the configuration will be loaded
        :type location: str
        :param settings: Settings dictionary that should be loaded into the configuration
        :type settings: None or dict[str, object]
        :rtype: None or pycultivator_lab.config.baseConfig.Config
        """
        return super(Config, cls).load(location, settings=settings, **kwargs)


class ConfigException(baseConfig.ConfigException):
    """Exception raised in by pycultivator_lab.config.baseConfig classes"""

    def __init__(self, msg):
        super(ConfigException, self).__init__(msg)

