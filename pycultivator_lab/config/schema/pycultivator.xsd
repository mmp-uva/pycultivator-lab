<?xml version="1.0" encoding="UTF-8"?>
<xs:schema version="3.1" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
           targetNamespace="pyCultivator" elementFormDefault="qualified">
	<xs:annotation>
		<xs:documentation>
			Standard schema, contains most common elements defined in pyCultivator
		</xs:documentation>
	</xs:annotation>

	<!-- Complex config -->
	<xs:complexType name="configType">
		<xs:annotation>
			<xs:documentation>
				Define a configuration
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="settings" type="pc:settingsType" minOccurs="0"/>
			<!-- Include device element in own schema -->
			<!-- <xs:element name="device" type="pc:baseDeviceType" /> -->
		</xs:sequence>
		<xs:attribute name="version" type="xs:string" />
		<xs:attribute name="class" type="xs:string" />
	</xs:complexType>

	<!-- Root element -->
	<xs:element name="config" type="pc:configType" />

	<!-- Complex object -->
	<xs:complexType name="objectType">
		<xs:annotation>
			<xs:documentation>
				An object is the most basic object that can be configured using XML in pyCultivator.
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="settings" type="pc:settingsType" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="class" type="xs:string"/>
	</xs:complexType>

	<!-- Complex part -->
	<xs:complexType name="partType">
		<xs:annotation>
			<xs:documentation>
				A part is a hierarchical object used to describe devices, instruments and others.
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="pc:objectType">
				<xs:attribute name="id" type="xs:string" use="required" />
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

    <!-- Complex ConnectedDevice -->
	<xs:complexType name="baseDeviceType">
		<xs:annotation>
			<xs:documentation>
				An connected Device
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="pc:partType">
				<xs:sequence>
					<!-- connection settings, such as port, speed etc -->
					<xs:element name="connection" type="pc:connectionType"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

    <!-- Complex Device -->
	<xs:complexType name="deviceType">
		<xs:annotation>
			<xs:documentation>
				An simple device (without channels)
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="pc:baseDeviceType">
				<xs:sequence>
					<xs:element name="instruments" type="pc:instrumentsType"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

	<!-- Complex ComplexDevice-->
	<xs:complexType name="complexDeviceType">
		<xs:annotation>
			<xs:documentation>
				The device elements holds all information required for using this device
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="pc:deviceType">
				<xs:sequence>
					<xs:element name="channels" type="pc:channelsType"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

    <!-- Complex Connection -->
	<xs:complexType name="connectionType">
		<xs:annotation>
			<xs:documentation>An connection describes how to connect to the device</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="pc:objectType">
				<!-- customize -->
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

	<!-- Complex Channels -->
	<xs:complexType name="channelsType">
		<xs:annotation>
			<xs:documentation>Contains all (cultivation)channels in the device</xs:documentation>
		</xs:annotation>
		<xs:choice minOccurs="0" maxOccurs="unbounded">
            <!-- leave emtpy -->
            <!-- <xs:element name="channel" type="pc:channelType" minOccurs="1" maxOccurs="unbounded" /> -->
		</xs:choice>
	</xs:complexType>

	<!-- Complex Channel -->
	<xs:complexType name="channelType">
		<xs:annotation>
			<xs:documentation>Holds all information specific to this channel</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="pc:partType">
				<xs:sequence>
					<xs:element name="instruments" type="pc:instrumentsType" minOccurs="0" />
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

	<!-- Complex Instruments -->
	<xs:complexType name="instrumentsType">
		<xs:annotation>
			<xs:documentation>Instruments contains the instruments in the device</xs:documentation>
		</xs:annotation>
		<xs:choice minOccurs="0" maxOccurs="unbounded">
			<!-- Leave empty -->
		</xs:choice>
	</xs:complexType>

    <!-- Complex Instrument -->
	<xs:complexType name="instrumentType">
		<xs:annotation>
			<xs:documentation>
				An Instrument definition holds all information required to use the instrument
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="pc:partType">
				<xs:sequence>
					<xs:element name="calibration" type="pc:calibration" minOccurs="0"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

	<!-- Complex settings -->
	<xs:complexType name="settingsType">
		<xs:annotation>
			<xs:documentation>The settings element holds settings for a particular element</xs:documentation>
		</xs:annotation>
		<xs:choice minOccurs="0" maxOccurs="unbounded">
			<xs:element name="setting" type="pc:settingType" minOccurs="0"/>
		</xs:choice>
	</xs:complexType>
	<xs:complexType name="settingType">
		<xs:annotation>
			<xs:documentation>The setting elements hold a setting value</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="value" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="key" type="xs:string" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
		<xs:attribute type="xs:integer" name="id" />
		<xs:attribute type="xs:string" name="name" use="required"/>
		<xs:attribute type="pc:variableType" name="type" use="required"/>
	</xs:complexType>

	<!-- Complex Definition -->
	<xs:complexType name="definitionType">
		<xs:annotation>
			<xs:documentation>
				A definition defines a relation between two things, by a set of mathematical relations.
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="pc:partType">
				<xs:attribute name="active" type="xs:boolean" default="false" />
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

	<!-- Complex Relation -->
	<xs:complexType name="relationType">
		<xs:annotation>
			<xs:documentation>
				A relation describes the relation between two elements.
				For example a mathematical relation in the form of a polynomial equation
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="pc:partType">
				<xs:attribute name="active" type="xs:boolean" default="false" />
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

    <!-- Complex Calibration -->
	<xs:complexType name="calibration">
		<xs:annotation>
			<xs:documentation>A calibration defines a relation between a input signal and output</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="pc:definitionType">
				<xs:sequence>
					<xs:element name="polynomial" type="pc:polynomialRelationType" minOccurs="0" maxOccurs="unbounded"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

	<!-- Complex Polynomial -->
	<xs:complexType name="polynomialRelationType">
		<xs:annotation>
			<xs:documentation>
				A polynomial relation is a general mathematical form of writing a relation.
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="pc:relationType">
				<xs:sequence maxOccurs="unbounded">
					<xs:element name="coefficient" type="pc:coefficientType"/>
				</xs:sequence>
				<xs:attribute name="domain" type="pc:domainType" default="(,)"/>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

	<!-- Complex Exponential -->
	<xs:complexType name="exponentialRelationType">
		<xs:annotation>
			<xs:documentation>
				A special form of a polynomial equation. The exponential equation looks like:
				y = a*e^(x*b) + c
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="pc:polynomialRelationType">
				<xs:sequence>
					<xs:element name="e_coefficient" type="pc:coefficientType"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

	<!-- complex coefficient -->
	<xs:complexType name="coefficientType">
		<xs:annotation>
			<xs:documentation>
				Define one coefficient of a mathematical equation. A coefficient has a
				specific order and a value. A zeroth order coefficient is also known as the intercept or
				constant.
			</xs:documentation>
		</xs:annotation>
		<xs:simpleContent>
			<xs:extension base="xs:float">
				<xs:attribute name="order" type="xs:integer"/>
			</xs:extension>
		</xs:simpleContent>
	</xs:complexType>

	<!-- Simple types -->

	<xs:simpleType name="variableType">
		<xs:restriction base="xs:string">
			<xs:enumeration value="bool"/>
			<xs:enumeration value="int"/>
			<xs:enumeration value="float"/>
			<xs:enumeration value="string"/>
			<xs:enumeration value="str"/>
			<!--<xs:enumeration value="list" />-->
			<!--<xs:enumeration value="dict" />-->
			<xs:enumeration value="dt"/>
			<xs:enumeration value="datetime"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="domainType">
		<xs:restriction base="xs:string">
			<xs:pattern value="[\(|\[]\d*(\.\d+)?,\d*(\.\d+)?[\)|\]]"/>
		</xs:restriction>
	</xs:simpleType>
</xs:schema>
