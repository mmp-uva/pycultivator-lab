from baseConfig import Config
from xmlConfig import XMLConfig


__all__ = [
    "Config", "XMLConfig"
]
