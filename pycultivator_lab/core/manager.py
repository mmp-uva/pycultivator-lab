
from pycultivator.core.objects import ConfigurableObject

import threading


class BaseManager(ConfigurableObject):
    """A manager keeps track of a set of items"""

    def __init__(self, parent=None, settings=None, **kwargs):
        super(BaseManager, self).__init__(settings=settings, **kwargs)
        self._parent = parent
        self._items = None
        self._object_lock = threading.RLock()

    @property
    def parent(self):
        """Host of the Manager

        :return: The host of the manager
        :rtype: pycultivator_lab.host.base.Host
        """
        with self._object_lock:
            result = self._parent
        return result

    def add(self, item):
        raise NotImplementedError

    def get(self, item):
        raise NotImplementedError

    def has(self, item):
        raise NotImplementedError

    @property
    def items(self):
        return self._items

    @property
    def size(self):
        raise NotImplementedError

    def __contains__(self, item):
        return self.has(item)


class Manager(BaseManager):
    """Manages a list of items"""

    _namespace = "manager"
    _default_settings = {

    }

    def __init__(self, parent=None, settings=None, **kwargs):
        super(Manager, self).__init__(parent=parent, settings=settings, **kwargs)
        self._items = []

    def add(self, item):
        with self._object_lock:
            self._items.append(item)

    def get(self, index):
        with self._object_lock:
            result = self._items[index]
        return result

    def has(self, item):
        with self._object_lock:
            result = item in self._items
        return result

    @property
    def size(self):
        return len(self._items)


class NamedManager(BaseManager):
    """Manages a dictionary of items"""

    def __init__(self, parent=None, settings=None, **kwargs):
        super(NamedManager, self).__init__(parent=parent, settings=settings, **kwargs)
        self._items = {}

    def add(self, item):
        raise NotImplementedError

    def get(self, name):
        with self._object_lock:
            result = self._items[name]
        return result

    def has(self, item):
        """Whether this manager has the item or name

        :param item: The name or the item to search for
        :rtype: bool
        """
        with self._object_lock:
            result = item in self._items.keys() or item in self._items.values()
        return result

    @property
    def size(self):
        return len(self._items.keys())
