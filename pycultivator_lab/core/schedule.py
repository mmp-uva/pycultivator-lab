"""A schedule """

from pycultivator.core.objects import PCObject

from datetime import datetime as dt, timedelta as td
import threading


class Schedule(PCObject):
    """Schedules execution at regular intervals"""

    def __init__(self, parent, callback=None):
        super(Schedule, self).__init__()
        self._object_lock = threading.RLock()
        self._parent = parent
        self._callback = callback
        self._start = None
        self._end = None
        self._last_run = None
        self._interval = None
        self._tolerance = None

    @property
    def parent(self):
        with self._object_lock:
            result = self._parent
        return result

    @parent.setter
    def parent(self, p):
        with self._object_lock:
            self._parent = p

    @property
    def time_start(self):
        with self._object_lock:
            result = self._start
        return result

    @time_start.setter
    def time_start(self, t_point):
        if t_point is not None and not isinstance(t_point, dt):
            raise TypeError("Time start must be a datetime object or None")
        with self._object_lock:
            self._start = t_point

    @property
    def time_end(self):
        return self._end

    @time_end.setter
    def time_end(self, t_point):
        if t_point is not None and not isinstance(t_point, dt):
            raise TypeError("Time end must be a datetime object or None")
        with self._object_lock:
            self._end = t_point

    @property
    def last_run(self):
        with self._object_lock:
            result = self._last_run
        return result

    @property
    def interval(self):
        with self._object_lock:
            result = self._interval
        return result

    @interval.setter
    def interval(self, value):
        if not isinstance(value, (float, int)):
            raise TypeError("Interval must be a float or integer")
        if value < 0:
            raise ValueError("Interval must be positive")
        with self._object_lock:
            self._interval = value

    @property
    def next_run(self):
        result = None
        if None not in [self.interval, self.last_run]:
            result = self.last_run + td(seconds=self.interval)
        return result

    @property
    def tolerance(self):
        return self._tolerance

    @tolerance.setter
    def tolerance(self, value):
        if not isinstance(value, (float, int)):
            raise TypeError("Interval must be a float or integer")
        if value < 0:
            raise ValueError("Interval must be positive")
        self._tolerance = value

    def should_run(self, t_now=None):
        """Whether it is time to execute the parent object"""
        result = True
        if t_now is None:
            t_now = dt.now()
        if not isinstance(t_now, dt):
            raise TypeError("Time now must be a datetime object")
        if self.time_start is not None:
            result = t_now > self.time_start
        if self.time_end is not None:
            result = result and t_now < self.time_end
        # if next run is scheduled
        if self.next_run is not None:
            result = result and self.next_run < t_now
        else:
            result = self.last_run is None
        # if tolerance is set, determine if we comply to it
        if self.tolerance is not None:
            result = result and t_now < self.next_run + td(seconds=self.tolerance)
        return result

    def update(self, t_now=None):
        """Update the last_run variable"""
        if t_now is None:
            t_now = dt.now()
        if not isinstance(t_now, dt):
            raise TypeError("Time now must be a datetime object")
        self._last_run = t_now

    def execute(self, callback=None, *args, **kwargs):
        """Determine if the schedule should run and execute the callback"""
        result = False
        if callback is None:
            callback = self._callback
        if self.should_run():
            if callback is not None:
                result = callback(*args, **kwargs)
        return result
