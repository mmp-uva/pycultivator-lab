"""Classes for the validation of a value"""

from pycultivator.core.objects import BaseObject

__all__ = [
    "BaseValidator",
    "NotNoneValidator", "RangeValidator", "PathValidator",
]


class BaseValidator(BaseObject):
    """BaseValidator does nothing and always returns true

    >>> v = BaseValidator()
    ... v.tests(9)
    True

    """

    def __init__(self):
        super(BaseValidator, self).__init__()

    def test(self, value):
        """Tests whether the value is valid for this validator

        :type value: object
        :rtype: bool
        """
        return True

    def copy(self, memo=None):
        result = type(self)()
        return result

    def to_dict(self):
        return {
            'class': ".".join([self.__class__.__module__, self.__class__.__name__]),
        }

    @classmethod
    def from_dict(cls, d):
        result = None
        klass_path = d.get("class")
        if klass_path is not None:
            from pycultivator.core import pcUtility
            result = pcUtility.import_object_from_str(klass_path)
        if isinstance(result, type) and issubclass(result, cls):
            result = result()
        return result

    def __eq__(self, other):
        return isinstance(other, self.__class__)

    def __ne__(self, other):
        return not self == other


class NotNoneValidator(BaseValidator):

    def test(self, value):
        return value is not None


class RangeValidator(BaseValidator):

    def __init__(
            self, minimum=None, include_minimum=True, maximum=None, include_maximum=True
    ):
        super(RangeValidator, self).__init__()
        self._minimum = minimum
        self._include_minimum = include_minimum is True
        self._maximum = maximum
        self._include_maximum = include_maximum is True

    def get_minimum(self):
        return self._minimum

    def set_minimum(self, m):
        self._minimum = m

    def has_minimum(self):
        return self.get_minimum() is not None

    def includes_minimum(self):
        return self._include_minimum is True

    def include_minimum(self, state):
        self._include_minimum = state is True

    @property
    def minimum(self):
        return self.get_minimum()

    @minimum.setter
    def minimum(self, m):
        self.set_minimum(m)

    def get_maximum(self):
        return self._maximum

    def set_maximum(self, m):
        self._maximum = m

    def has_maximum(self):
        return self.get_maximum() is not None

    def includes_maximum(self):
        return self._include_maximum is True

    def include_maximum(self, state):
        self._include_maximum = state is True

    @property
    def maximum(self):
        return self.get_maximum()

    @maximum.setter
    def maximum(self, m):
        self.set_maximum(m)

    def test(self, value):
        min_result = True
        if self.has_minimum():
            min_result = value > self.minimum or (value >= self.minimum and self.includes_minimum())
        max_result = True
        if self.has_maximum():
            max_result = value < self.maximum or (value <= self.maximum and self.includes_maximum())
        return min_result and max_result

    def copy(self, memo=None):
        result = type(self)(
            minimum=self.minimum, maximum=self.maximum,
            include_minimum=self.includes_minimum(), include_maximum=self.includes_maximum()
        )
        return result

    def to_dict(self):
        d = super(RangeValidator, self).to_dict()
        d["minimum"] = self.minimum
        d["includes_minimum"] = self.includes_minimum()
        d["maximum"] = self.maximum
        d["includes_maximum"] = self.includes_maximum()
        return d

    @classmethod
    def from_dict(cls, d):
        result = super(RangeValidator, cls).from_dict(d)
        """:type: pycultivator_lab.core.validators.RangeValidator"""
        minimum = d.get("minimum")
        if minimum is not None:
            result.minimum = minimum
        includes_minimum = d.get("includes_minimum")
        if includes_minimum is not None:
            result.include_minimum(includes_minimum)
        maximum = d.get("maximum")
        if maximum is not None:
            result.maximum = maximum
        includes_maximum = d.get("includes_maximum")
        if includes_minimum is not None:
            result.include_maximum(includes_maximum)
        return result

    def __eq__(self, other):
        result = super(RangeValidator, self).__eq__(other)
        if result:
            result = self.minimum == other.minimum and self.maximum == other.maximum
            result = result and (self.includes_maximum() == other.includes_maximum())
            result = result and (self.includes_minimum() == other.includes_minimum())
        return result


class PathValidator(BaseValidator):
    """Checks if the given value is a valid path"""

    def __init__(self, require_exist=True):
        """Initialize PathValidator

        :param require_exist: Whether to require the path to exist (instead of only reachable)
        """
        super(PathValidator, self).__init__()
        self._require_exist = require_exist

    def requires_exist(self):
        return self._require_exist is True

    def require_exist(self, state):
        self._require_exist = state is True

    def test(self, value):
        result = isinstance(value, str)
        import os
        if result:
            # transform into complete path
            target = os.path.realpath(value)
            # get target directory
            target_dir = os.path.dirname(target)
            # check if target directory is reachable
            result_dir = os.path.exists(target_dir)
            # target directory should exist and either do not require the path to exist or the path exists
            result = result_dir and (not self.requires_exist() or os.path.exists(value))
        return result

    def to_dict(self):
        result = super(PathValidator, self).to_dict()
        result["require_exist"] = self.requires_exist()
        return result

    @classmethod
    def from_dict(cls, d):
        result = super(PathValidator, cls).from_dict(d)
        """:type: pycultivator_lab.core.validators.PathValidator"""
        require_exist = d.get("require_exist")
        if require_exist is not None:
            result.require_exist(require_exist)
        return result

    def __eq__(self, other):
        result = super(PathValidator, self).__eq__(other)
        if result:
            result = self.requires_exist() == other.requires_exist()
        return result
