# coding=utf-8
"""Test cases for the validators module in pycultivator_lab.scripts.meta"""

from pycultivator_lab.core.validators import *
from pycultivator.core.tests import UvaSubjectTestCase

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TestBaseValidator(UvaSubjectTestCase):
    """Test the ScriptAttrbute class"""

    _subject_cls = BaseValidator
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """ Returns the class of the subject of the tests case

        :return:
        :rtype: callable -> pycultivator_lab.scripts.meta.validators.BaseValidator
        """
        return super(TestBaseValidator, cls).getSubjectClass()

    def getSubject(self):
        """ Returns the instance of the subject of the tests case

        :return:
        :rtype: TestBaseValidator._subject_cls
        """
        return super(TestBaseValidator, self).getSubject()

    def initSubject(self, *args, **kwargs):
        """ Returns a new instance of the subject of the tests case

        :return:
        :rtype: TestBaseValidator._subject_cls
        """
        return self.getSubjectClass()()

    def test_test(self):
        # base validator always returns true
        self.assertTrue(self.getSubject().test(,,)
        self.assertTrue(self.getSubject().test(,,)
        self.assertTrue(self.getSubject().test(,,)
        self.assertTrue(self.getSubject().test(,,)
        self.assertTrue(self.getSubject().test(,,)

    def test_to_dict(self):
        d = self.getSubject().to_dict()
        klass_name = ".".join([self.getSubjectClass().__module__, self.getSubjectClass().__name__])
        self.assertEqual(d.get("class"), klass_name)

    def test_from_dict(self):
        d = self.getSubject().to_dict()
        o = self.getSubjectClass().from_dict(d)
        self.assertIsInstance(o, self.getSubjectClass())
        self.assertEqual(self.getSubject(), o)


class TestNotNoneValidator(TestBaseValidator):

    _subject_cls = NotNoneValidator
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """ Returns the class of the subject of the tests case

        :return:
        :rtype: callable -> TestCombinedValidator._subject_cls
        """
        return super(TestNotNoneValidator, cls).getSubjectClass()

    def getSubject(self):
        """ Returns the instance of the subject of the tests case

        :return:
        :rtype: TestCombinedValidator._subject_cls
        """
        return super(TestNotNoneValidator, self).getSubject()

    def test_test(self):
        # returns False when value is None
        self.assertTrue(self.getSubject().test(,,)
        self.assertTrue(self.getSubject().test(,,)
        self.assertFalse(self.getSubject().test(,,)
        self.assertTrue(self.getSubject().test(,,)
        self.assertTrue(self.getSubject().test(,,)


class TestRangeValidator(TestBaseValidator):

    _subject_cls = RangeValidator
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """ Returns the class of the subject of the tests case

        :return:
        :rtype: callable -> TestRangeValidator._subject_cls
        """
        return super(TestRangeValidator, cls).getSubjectClass()

    def getSubject(self):
        """ Returns the instance of the subject of the tests case

        :return:
        :rtype: TestRangeValidator._subject_cls
        """
        return super(TestRangeValidator, self).getSubject()

    def initSubject(self, *args, **kwargs):
        """ Returns a new instance of the subject of the tests case

        :return:
        :rtype: TestRangeValidator._subject_cls
        """
        return self.getSubjectClass()(*args, **kwargs)

    def test_test(self):
        # default: no range everything is accepted
        self.assertTrue(self.getSubject().test(,,)
        self.assertTrue(self.getSubject().test(,,)
        self.assertTrue(self.getSubject().test(,,)
        self.assertTrue(self.getSubject().test(,,)
        self.assertTrue(self.getSubject().test(,,)
        self.assertTrue(self.getSubject().test(,,)
        self.assertTrue(self.getSubject().test(,,)
        s = self.initSubject(-1, True, 1, True)
        self.assertFalse(s.test(,,)
        self.assertTrue(s.test(,,)
        self.assertTrue(s.test(,,)
        self.assertTrue(s.test(,,)
        self.assertTrue(s.test(,,)
        self.assertTrue(s.test(,,)
        self.assertFalse(s.test(,,)
        s = self.initSubject(-1, False, 1, False)
        self.assertFalse(s.test(,,)
        self.assertFalse(s.test(,,)
        self.assertTrue(s.test(,,)
        self.assertTrue(s.test(,,)
        self.assertTrue(s.test(,,)
        self.assertFalse(s.test(,,)
        self.assertFalse(s.test(,,)
        s = self.initSubject(None, False, 1, False)
        self.assertFalse(s.test(,,)
        self.assertFalse(s.test(,,)
        self.assertTrue(s.test(,,)
        self.assertTrue(s.test(,,)
        self.assertTrue(s.test(,,)
        self.assertTrue(s.test(,,)
        self.assertTrue(s.test(,,)

    def test_to_dict(self):
        super(TestRangeValidator, self).test_to_dict()
        s = self.initSubject(-1, False, 1, True)
        d = s.to_dict()
        self.assertEqual(d["minimum"], -1)
        self.assertFalse(d["includes_minimum"])
        self.assertEqual(d["maximum"], 1)
        self.assertTrue(d["includes_maximum"])

    def test_from_dict(self):
        super(TestRangeValidator, self).test_to_dict()
        s = self.initSubject(-1, False, 1, True)
        d = s.to_dict()
        o = self.getSubjectClass().from_dict(d)
        self.assertEqual(o.minimum, s.minimum)
        self.assertIs(o.includes_minimum(), s.includes_minimum())
        self.assertEqual(o.maximum, s.maximum)
        self.assertIs(o.includes_maximum(), s.includes_maximum())


class TestPathValidator(TestBaseValidator):

    _subject_cls = PathValidator

    @classmethod
    def getSubjectClass(cls):
        """ Returns the class of the subject of the tests case

        :return:
        :rtype: callable -> TestPathValidator._subject_cls
        """
        return super(TestPathValidator, cls).getSubjectClass()

    def getSubject(self):
        """ Returns the instance of the subject of the tests case

        :return:
        :rtype: TestPathValidator._subject_cls
        """
        return super(TestPathValidator, self).getSubject()

    def initSubject(self, *args, **kwargs):
        """ Returns a new instance of the subject of the tests case

        :return:
        :rtype: TestPathValidator._subject_cls
        """
        return self.getSubjectClass()(*args, **kwargs)

    def test_test(self):
        self.assertFalse(self.getSubject().test(,,)

    def test_to_dict(self):
        super(TestPathValidator, self).test_to_dict()
        s = self.initSubject(require_exist=False)
        d = s.to_dict()
        self.assertFalse(d["require_exist"])

    def test_from_dict(self):
        super(TestPathValidator, self).test_from_dict()
        s = self.initSubject(require_exist=False)
        d = s.to_dict()
        o = self.getSubjectClass().from_dict(d)
        self.assertEqual(o.requires_exist(), s.requires_exist())
