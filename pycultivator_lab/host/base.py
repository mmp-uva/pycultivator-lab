"""Basic Host system

* Hosts have a manager to manage tasks
* Hosts have a manager to manage resources

"""

from pycultivator.core.objects import ConfigurableObject
from pycultivator_lab.task import Task, TaskManager
from pycultivator_lab.resource import Resource, ResourceManager

import threading


class Host(ConfigurableObject):
    """A simple host object"""

    _namespace = "task.host"
    _default_settings = {
        # add default settings
    }

    def __init__(self, settings=None, **kwargs):
        super(Host, self).__init__(settings=settings, **kwargs)
        self._object_lock = threading.RLock()
        self._task_manager = TaskManager(parent=self)
        self._resource_manager = ResourceManager(parent=self)
        # data manager
        self._update_event = threading.Event()
        self._stop_event = threading.Event()
        self._terminate_event = threading.Event()

    @property
    def tasks(self):
        """The task manager of this host

        :rtype: pycultivator_lab.task.manager.TaskManager
        """
        return self._task_manager

    @property
    def resources(self):
        """The task manager of this host

        :rtype: pycultivator_lab.resource.manager.ResourceManager
        """
        return self._resource_manager

    def add_task(self, task):
        if not isinstance(task, Task):
            raise TypeError("Invalid task")
        self.tasks.schedule(task)
        # inform host
        self.update()

    def add_resource(self, resource):
        if not isinstance(resource, Resource):
            raise TypeError("Invalid resource")
        self.resources.schedule(resource)

    def start(self):
        """General start method (mimick Thread)"""
        self.run()

    def run(self):
        """Run the host"""
        self.getLog().info("Good morning folks!")
        while self.canRun():
            try:
                self._update_event.clear()
                self.execute()
                self._update_event.wait(1)
            except KeyboardInterrupt:
                self.stop()
        self.clean()
        self.getLog().info("bye bye!")

    def execute(self):
        """Execute regular actions"""
        self.manage_tasks()
        while self.tasks.countWaitingTasks() > 0:
            self.manage_tasks()
            self._execute_task()
            self.manage_resources()

    def clean(self):
        """Clean up"""
        # remove unfinished tasks
        self.tasks.clean()
        # remove all resources
        self.resources.clean()
        return True

    def manage_tasks(self):
        self.tasks.process()
        self.tasks.clean()

    def manage_resources(self):
        self.resources.clean()

    def schedule_task(self, task):
        result = False
        if not self.shouldStop():
            result = self.tasks.schedule(task)
        return result

    def _execute_task(self):
        task = self.tasks.retrieve()
        if task is not None:
            try:
                task.start()
            except KeyboardInterrupt:
                self.stop()
                raise
            finally:
                self.tasks.finish(task)

    def canRun(self):
        """Whether the host should continue running

        * not terminating
        * still have work

        :rtype: bool
        """
        return not self.shouldStop() and self.hasWork()

    def hasWork(self):
        """Whether the host has unfinished tasks"""
        return self.tasks.countQueuedTasks() > 0

    def shouldStop(self):
        """Whether the host has been signalled to stop"""
        return self._stop_event.isSet()

    def mustStop(self):
        """Whether the host has been signalled to terminate"""
        return self._terminate_event.isSet()

    def stop(self):
        """Stop running the host"""
        self.getLog().info("Exiting gracefully...")
        self._stop_event.set()

    def terminate(self):
        """Terminate the host"""
        self.getLog().info("Terminating...")
        self._terminate_event.set()

    def update(self):
        """Inform the host there was an update"""
        self._update_event.set()
