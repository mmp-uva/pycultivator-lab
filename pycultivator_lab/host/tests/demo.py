
from __future__ import print_function
from pycultivator.core import pcLogger
from pycultivator_lab.host import Host
from pycultivator_lab.task.base import Task


class HelloWorldTask(Task):

    def run(self, *args, **kwargs):
        print("Hello World!")
        return True


if __name__ == "__main__":
    log = pcLogger.getLogger()
    log.setLevel(pcLogger.levels('info'))
    pcLogger.connectStreamHandler(log, level=10)
    log.info("Start Host Demo")
    host = Host()
    # add tasks
    for i in range(10):
        host.add_task(HelloWorldTask())
    host.start()
