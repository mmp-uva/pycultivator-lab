"""Base Implementation of a Task Manager

* Managers generate an unique ID for each task

* Managers keep track of tasks scheduled for execution (but not ready yet)
* Managers keep track of tasks waiting to be executed
* Managers keep track of tasks being executed
* Managers keep track of finished tasks

* Managers remove finished tasks when backlog is full

"""

from pycultivator_lab.core.manager import NamedManager
from .base import Task

import threading


class TaskManager(NamedManager):

    _namespace = "manager.task"
    _default_settings = {

    }

    def __init__(self, settings=None, **kwargs):
        super(TaskManager, self).__init__(settings=settings, **kwargs)
        # task lock
        self._object_lock = threading.RLock()
        # tasks scheduled for execution
        self._scheduled = {}
        # tasks ready for execution (but waiting)
        self._waiting = {}
        # tasks running
        self._running = {}
        # task finished
        self._finished = {}
        # contains list of id's: first is oldest
        self._finish_order = []
        # maximum number of finished tasks to store
        self._finish_pool_size = 1000

    def generateTaskID(self):
        """Generates a new task ID"""
        import uuid
        # generate random ID for the task
        result = str(uuid.uuid4())
        # check availability to be sure, or re-issue.
        while self.hasTaskID(result):
            result = str(uuid.uuid4())
        return result

    def hasTask(self, task):
        with self._object_lock:
            result = self.isTaskIDScheduled(task) or \
                     self.isTaskWaiting(task) or \
                     self.isTaskFinished(task)
        return result

    def isTaskScheduled(self, task):
        with self._object_lock:
            result = task in self._scheduled.values()
        return result

    def isTaskWaiting(self, task):
        with self._object_lock:
            result = task in self._waiting.values()
        return result

    def isTaskFinished(self, task):
        with self._object_lock:
            result = task in self._finished.values()
        return result

    def hasTaskID(self, task_id):
        with self._object_lock:
            result = self.isTaskIDScheduled(task_id) or \
                     self.isTaskIDWaiting(task_id) or \
                     self.isTaskIDFinished(task_id)
        return result

    def isTaskIDScheduled(self, task_id):
        with self._object_lock:
            result = task_id in self._scheduled.keys()
        return result

    def isTaskIDWaiting(self, task_id):
        with self._object_lock:
            result = task_id in self._waiting.keys()
        return result

    def isTaskIDFinished(self, task_id):
        with self._object_lock:
            result = task_id in self._finished.keys()
        return result

    def getFinishedPoolSize(self):
        return self._finish_pool_size

    def setFinishedPoolSize(self, v):
        if not isinstance(v, int) or v <= 0:
            raise ValueError("Pool Size has to be positive non-zero integer")
        with self._object_lock:
            self._finish_pool_size = v

    def schedule(self, task):
        """Schedule a task

        :param task: Task to be scheduled
        :type task: pycultivator_lab.task.base.Task

        """
        result = False
        # lock manager
        with self._object_lock:
            # check if ID is unique
            if not self.hasTaskID(task.uuid):
                # add task to schedule
                self._scheduled[task.uuid] = task
        return result

    def process(self):
        """Check scheduled tasks and place task who are ready in waiting

        :return: Number of tasks that were ready
        """
        # count number of ready tasks
        result = 0
        with self._object_lock:
            scheduled = {}
            for uuid, task in self._scheduled.items():
                if task.is_ready:
                    self._waiting[uuid] = task
                    result += 1
                else:
                    scheduled[uuid] = task
            self._scheduled = scheduled
        return result

    def retrieve(self):
        """Get a waiting task"""
        task = None
        with self._object_lock:
            if len(self._waiting) > 0:
                # get task
                uuid, task = self._waiting.popitem()
                self._running[uuid] = task
        return task

    def finish(self, task):
        """Notify manager about finishing a task"""
        result = False
        with self._object_lock:
            # remove from running tasks list
            self._running.pop(task.uuid)
            # add to finished
            self._finished[task.uuid] = task
            # record task finish order
            self._finish_order.append(task.uuid)
            # clean
            self.clean()
        return result

    def clean(self):
        """Remove finished tasks from history

        :return: Number of tasks removed from finished
        :rtype: int
        """
        result = 0
        with self._object_lock:
            max_pool_size = self.getFinishedPoolSize()
            pool_size = len(self._finish_order)
            while 0 < pool_size > max_pool_size:
                # remove oldest
                uuid = self._finish_order.pop(0)
                self._finished.pop(uuid)
                result += 1
                pool_size = len(self._finish_order)
        return result

    def empty(self):
        with self._object_lock:
            # reset scheduled tasks
            self._scheduled = {}
            # reset waiting list
            self._waiting = {}
        return self.countWaitingTasks()

    def countScheduledTasks(self):
        with self._object_lock:
            result = len(self._scheduled.keys())
        return result

    def countWaitingTasks(self):
        with self._object_lock:
            result = len(self._waiting.keys())
        return result

    def countQueuedTasks(self):
        with self._object_lock:
            result = self.countScheduledTasks() + self.countWaitingTasks()
        return result

    def countRunningTasks(self):
        with self._object_lock:
            result = len(self._running.keys())
        return result

    def countFinishedTasks(self):
        with self._object_lock:
            result = len(self._finish_order)
        return result

    def __str__(self):
        return "{} Tasks Scheduled, {} Tasks Waiting, {} Tasks Running, {} Tasks Finished".format(
            self.countScheduledTasks(), self.countWaitingTasks(),
            self.countRunningTasks(), self.countFinishedTasks()
        )

    def __contains__(self, item):
        """Whether the item is scheduled or finished"""
        result = item is not None
        if isinstance(item, Task):
            # test if the task has an id, use that ID to check
            if item.uuid is not None:
                result = self.hasTaskID(item.uuid)
            else:
                result = self.hasTask(item)
        return result
