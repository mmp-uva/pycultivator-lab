"""Base Implementation of a Task

* Tasks perform actions on a resource


"""

from pycultivator.core import ConfigurableObject, PCException
from .context import TaskContext
import threading

import uuid


class BaseTask(ConfigurableObject):

    _namespace = "task"
    _default_settings = {

    }

    def __init__(self, active=True, weight=0, context=None, settings=None, **kwargs):
        super(BaseTask, self).__init__(settings=settings, **kwargs)
        self._object_lock = threading.RLock()
        #
        self._uuid = uuid.uuid4()
        self._is_active = active
        self._weight = weight
        #
        self._running = False
        self._finished = False
        self._success = False
        self._result = None
        # run time in seconds
        self._runtime = 0
        # context
        if not isinstance(context, TaskContext):
            context = None
        self._context = context

    @property
    def context(self):
        return self._context

    @property
    def uuid(self):
        with self._object_lock:
            result = self._uuid
        return result

    @property
    def is_active(self):
        with self._object_lock:
            result = self._is_active
        return result

    @is_active.setter
    def is_active(self, state):
        with self._object_lock:
            self._is_active = state is True

    @property
    def weight(self):
        with self._object_lock:
            result = self._weight
        return result

    @weight.setter
    def weight(self, w):
        with self._object_lock:
            self._weight = w

    @property
    def is_ready(self):
        return True     # overload

    @property
    def is_finished(self):
        with self._object_lock:
            result = self._finished
        return result

    @property
    def is_running(self):
        with self._object_lock:
            result = self._running
        return result

    @property
    def is_successful(self):
        with self._object_lock:
            result = self._success
        return result

    @property
    def result(self):
        with self._object_lock:
            result = self._result
        return result

    @property
    def runtime(self):
        """Amount of seconds this task was running"""
        return self._runtime

    def start(self, context=None, *args, **kwargs):
        """Execute the task"""
        with self._object_lock:
            if isinstance(context, TaskContext):
                self._context = context
            self._running = True
        result = None
        success = False
        from datetime import datetime as dt
        t_start = dt.now()
        try:
            if self.is_active:
                with self._object_lock:
                    result = self.run(*args, **kwargs)
            else:
                self.getLog().warning("Not executing {}, because is not active!".format(self.uuid))
            success = True
        except Exception as e:
            self.getLog().error("Unexpected Error in {}: {}".format(
                self.uuid, e
            ))
        seconds = (dt.now() - t_start).total_seconds()
        if result is None:
            result = True
        with self._object_lock:
            self._running = False
            self._finished = True
            self._success = success
            self._result = result
            self._runtime = seconds
        return result

    def run(self, *args, **kwargs):
        return True   # overload

    def copy(self, memo=None):
        result = super(BaseTask, self).copy(memo=memo)
        result._weight = self.weight
        result._is_active = self.is_active
        result._context = self._context
        return result

    def __str__(self):
        state = "IDLE"
        if self.is_ready:
            state = "WAITING"
        if self.is_running:
            state = "RUNNING"
        if self.is_finished:
            state = "FINISHED"
        return "{} ({})".format(
            self.uuid, state
        )


class Task(BaseTask):

    pass


class ConditionalTask(Task):

    def __init__(self, conditions=None, **kwargs):
        super(ConditionalTask, self).__init__(**kwargs)
        if conditions is None:
            conditions = []
        self._conditions = conditions

    @property
    def conditions(self):
        return self._conditions

    @property
    def is_ready(self):
        result = True
        with self._object_lock:
            context = self.context
            for condition in self.conditions:
                if not condition.isSatisfied(context):
                    result = False
                    break
        return result


class TaskException(PCException):
    """Exception raised by a Task"""

    def __init__(self, msg):
        super(TaskException, self).__init__(msg=msg)
