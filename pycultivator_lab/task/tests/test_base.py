"""Test base module of the task package"""

from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator_lab.task.base import Task


class TestTask(UvaSubjectTestCase):

    _subject_cls = Task
    _abstract = False

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(5)

    def test_weight(self):
        self.assertEqual(self.subject.weight, 0)
        self.subject.weight = 5
        self.assertEqual(self.subject.weight, 5)

    def test_init(self):
        a = self.getSubjectClass()()
        self.assertEqual(a.weight, 0)
        self.assertTrue(a.is_active)
        self.assertIsNone(a.result)
        self.assertFalse(a.is_running)
        self.assertFalse(a.is_finished)
        self.assertFalse(a.is_successful)
        a = self.getSubjectClass()(weight=10)
        self.assertEqual(a.weight, 10)
        a = self.getSubjectClass()(active=False)
        self.assertFalse(a.is_active)

    def test_copy(self):
        a = self.getSubjectClass()()
        a.start()
        b = a.copy()
        self.assertIsNone(b.result)
        self.assertFalse(b.is_running)
        self.assertTrue(a.is_finished)
        self.assertFalse(b.is_finished)
        self.assertTrue(a.is_successful)
        self.assertFalse(b.is_successful)
        a = self.getSubjectClass()(weight=10, active=False)
        b = a.copy()
        self.assertEqual(a.weight, b.weight)
        self.assertEqual(a.is_active, b.is_active)

