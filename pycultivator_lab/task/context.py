
from pycultivator_lab.core.context import Context


class TaskContext(Context):

    def __init__(self, task_manager=None):
        if task_manager is None:
            from manager import TaskManager
            task_manager = TaskManager()
        self._task_manager = task_manager

    @property
    def task_manager(self):
        return self._task_manager
