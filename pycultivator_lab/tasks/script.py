"""Script for executing a task in a pycultivator task host

Load the script object and provide the class name of the task that should be executed.
The script will define argparse arguments based on the task attributes and run the task.
"""

from pycultivator_lab.scripts import meta
from pycultivator_lab.tasks.host import TaskHost
from pycultivator_lab.tasks.attributes import ListAttribute

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


# a small script class for running and starting a server
class TaskMetaScript(meta.ConfigurableMetaScript, TaskHost):

    verbose = meta.ScriptCountingAttribute(default=0, multiplier=10, aliases=['v'], is_required=False)

    def __init__(self, task_class=None, *args, **kwargs):
        super(TaskMetaScript, self).__init__(*args, **kwargs)
        self._script_attributes = {}
        self._task_class = task_class
        self._initial_task = None

    def getInitialTask(self):
        """Return the task loaded as first task of this script

        :return: The first task of this script
        :rtype: pycultivator_lab.tasks.Task
        """
        return self._initial_task

    def hasInitialTask(self):
        return self.getInitialTask() is not None

    def add_arguments(self, parser, arguments=None):
        """Adds arguments to the parser"""
        results = {}
        # load parser argument defined in script
        script_arguments = self.attributes
        # load extra arguments
        if isinstance(arguments, dict):
            script_arguments.update(arguments)
        # load parser arguments defined in task
        if self._initial_task is not None:
            task_arguments = self.add_task_arguments(self._initial_task)
            results.update(task_arguments)
        # overwrite with any arguments defined in the script
        results.update(script_arguments)
        # store in script for reading
        self._script_attributes = results
        # now process all arguments
        parser = super(TaskMetaScript, self).add_arguments(parser, _attributes=results)
        return parser

    def add_task_arguments(self, task):
        """

        :param task:
        :type task: pycultivator_lab.tasks.Task
        :rtype: dict[str, pycultivator_lab.scripts.meta.attributes.ScriptAttribute]
        """
        results = {}
        for name in task.attributes.keys():
            result = None
            attribute = task.attributes[name]
            # we do not allow duplicates: use this to overwrite!
            if name not in self.attributes.keys():
                if isinstance(attribute, ListAttribute):
                    result = self.create_list_arguments(attribute)
                else:
                    result = self.create_task_argument(attribute)
            if result is not None:
                results[name] = result
        return results

    def create_task_argument(self, attribute):
        return meta.ScriptAttribute(
            attribute.type, default=attribute.default, name=attribute.name,
            aliases=attribute.aliases, is_required=attribute.is_required(),
            help=attribute.description, validators=attribute.validators
        )

    def create_list_arguments(self, attribute):
        """Create all arguments for list attributes

        :param attribute:
        :type attribute: pycultivator_lab.tasks.attributes.ListAttribute
        """
        return meta.ScriptListAttribute(
            attribute.type, default=attribute.default, name=attribute.name,
            aliases=attribute.aliases, is_required=attribute.is_required(),
            help=attribute.description,validators=attribute.validators
        )

    def parse_arguments(self, arguments=None, _attributes=None):
        if _attributes is None:
            _attributes = self._script_attributes
        result = super(TaskMetaScript, self).parse_arguments(arguments=arguments, _attributes=_attributes)
        # now set the values of the task
        if result and self._initial_task is not None:
            result = self.load_task_arguments(self._initial_task, _attributes)
        return result

    def load_task_arguments(self, task, _attributes=None):
        result = True
        if _attributes is None:
            _attributes = self._script_attributes
        for name in task.attributes.keys():
            if name in _attributes.keys():
                attribute = task.attributes[name]
                attribute.set(_attributes[name].get())
        return result

    def prepare(self):
        if self.verbose > 0:
            level = 50 - self.verbose
            if level < 0:
                level = 0
            self.connect_log(level)
        return True

    def connect_log(self, level=0):
        from pycultivator.core import pcLogger
        handler = pcLogger.UVAStreamHandler()
        handler.setLevel(level)
        self.getRootLog().addHandler(handler)
        if self.getRootLog().getEffectiveLevel() > level:
            self.getRootLog().setLevel(level)
        self.getLog().debug("Logger is now connected to the console")

    def run(self, load_arguments=True, task_class=None):
        # load task class
        if task_class is None:
            task_class = self._task_class
        from pycultivator_lab.tasks import Task
        if isinstance(task_class, type) and issubclass(task_class, Task):
            task = task_class()
            # register task to host
            self._initial_task = task
        return meta.MetaScript.run(self, load_arguments=load_arguments)

    def execute(self):
        if self.hasInitialTask() and self.getInitialTask().isFilled():
            self.queueTask(self.getInitialTask())
        self.getLog().debug("Number of tasks pre-loaded: {}".format(self.countWaitingTasks()))
        return TaskHost.run(self)

    def clean(self):
        return True
