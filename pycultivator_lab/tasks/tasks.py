"""Tasks to be executed by a server"""

from pycultivator.core import pcObject, pcException, pcTemplate
from attributes import *
from pycultivator_lab.core.resources import *
from conditions import *
from datetime import datetime as dt
import threading

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = ["Task", "TaskException", "RecurringTask"]


class TaskMeta(type):
    """Meta allowing for the dynamic specification of task resources as class attributes"""

    @staticmethod
    def __new__(mcs, name, bases=None, attrs=None):
        return super(TaskMeta, mcs).__new__(mcs, name, bases, attrs)

    def __init__(cls, name, bases=None, attrs=None):
        super(TaskMeta, cls).__init__(name, bases, attrs)
        if bases is None:
            bases = []
        if attrs is None:
            attrs = {}
        # create dictionaries to hold attributes and resources
        cls.defined_attributes = {}
        # fill meta with meta from parents
        for base in bases:
            if isinstance(base, TaskMeta):
                cls.defined_attributes.update(base.getDefinedAttributes())
        # process variables
        for obj_name, obj in attrs.items():
            if isinstance(obj, Attribute):
                cls.__addAttribute(obj_name, obj)

    def __addAttribute(cls, name, obj):
        # remove _ from obj_name
        pub_name = name.lstrip("_")
        # store variable under public name in meta
        cls.defined_attributes[pub_name] = obj
        obj.set_name(name)
        # if variable is public: create public property
        setattr(cls, pub_name, pcTemplate.DeferredAttribute(pub_name, "_attributes"))

    def getDefinedAttributes(cls):
        return cls.defined_attributes


class Task(pcObject.PCObject):
    """A task holds the code for execution on a pycultivator_lab Server

    Tasks inform the server about what resources are needed and when it is ready to start.

    Attribute access
    ----------------

    The value of Attributes objects defined as class attributes can be accessed directly in the instance using
    `self.<attribute>`, as they are processed by the metaclass.

    Resource access
    ---------------

    Use with-statements to access resources and perform proper locking, otherwise ensure to release resource as soon
    as possible.

    """

    __metaclass__ = TaskMeta
    _name = None
    description = """A general task"""

    def __init__(self, parent=None, attributes=None, resources=None, conditions=None, **kwargs):
        """Initialize task

        :param parent: Reference to the TaskHost
        :type parent: pycultivator_lab.tasks.host
        :param attributes: Extra attributes to be defined in the task
        :type attributes: dict[str, pycultivator_lab.tasks.attributes.Attribute]
        :param resources: Extra resources to be used in the task
        :type attributes: dict[str, pycultivator_lab.tasks.resources.Resources]
        :param conditions: Conditions that need to be met before the task can start
        """
        super(Task, self).__init__()
        # identification used by parent
        self._id = None
        # lock used to protect some attributes
        self._lock = threading.RLock()
        # reference to server executing the task
        self._parent = None
        if parent is not None:
            self.setParent(parent)
        # information about attributes used
        self._attributes = {}
        # load from defined
        import copy
        for name in self.getDefinedAttributes().keys():
            attribute = copy.deepcopy(self.getDefinedAttributes()[name])
            self._attributes[name] = attribute
        # load given attributes
        if attributes is None:
            attributes = {}
        # update
        for name, attribute in attributes.items():
            if isinstance(attribute, Attribute):
                self._attributes[name] = attribute
        # publish attributes
        for name in self.getAttributes().keys():
            self._registerAttribute(name)
        # information about the resources needed
        self._resources = {}
        # load given resources
        if resources is None:
            resources = {}
        # update
        self._resources.update(resources)
        # publish resources
        for name in self.getResources().keys():
            self._registerResource(name)
        # information about conditions before task can start
        if conditions is None:
            conditions = []
        if not isinstance(conditions, (tuple, list)):
            conditions = [conditions]
        self._conditions = conditions
        # information about task performance
        self._t_start = None
        self._t_finish = None
        self._success = False
        self._running = False
        self._result = None
        self._finished = threading.Event()
        # take attribute values from attributes that are not Attribute classes
        for name, value in attributes.items():
            if not isinstance(value, Attribute) and name in self.attributes:
                self.attributes[name].set(value)
        # take attribute values from given kwargs
        for name, value in kwargs.items():
            if self.hasAttribute(name):
                self.getAttribute(name).set(value)

    @classmethod
    def getName(cls):
        result = cls._name
        if result is None:
            result = cls.__name__
        result = result.replace(".", "_")
        result = result.replace("-", "_")
        return result

    @classmethod
    def getFullName(cls):
        return "{}.{}".format(cls.__module__, cls.getName())

    @classmethod
    def getDescription(cls):
        return cls.description

    def _registerAttribute(self, name, attributes=None):
        """Register an attribute from"""
        if attributes is None:
            attributes = self.getAttributes()
        attribute = attributes.get(name)
        # remove "_" from name
        pub_name = name.lstrip("_")
        if isinstance(attribute, ResourceAttribute):
            self._resources[name] = ResourceAttribute.getResource()
            # register resource under public name
            self._registerResource(name)
        elif isinstance(attribute, Attribute):
            # create deferred attribute, to give direct access to value
            setattr(self.__class__, pub_name, pcTemplate.DeferredAttribute(pub_name, "_attributes"))
            setattr(self, "_{}".format(pub_name), attribute)

    def _registerResource(self, name):
        # remove "_" from name
        pub_name = name.lstrip("_")
        setattr(self, pub_name, lambda: self.getResource(name))

    # identification

    def getIdentification(self):
        return self._id

    def hasIdentification(self):
        return self.getIdentification() is not None

    def setIdentification(self, value):
        self._id = value

    # parent

    def getParent(self):
        """The parent executing this task

        :rtype: None | pycultivator_lab.tasks.host.TaskHost
        """
        with self._lock:
            result = self._parent
        return result

    def hasParent(self):
        """Whether this task has been registered to a server (i.e. scheduled for execution)"""
        return self.getParent() is not None

    @property
    def parent(self):
        return self.getParent()

    def setParent(self, parent):
        with self._lock:
            from host import TaskHost
            if parent is not None and not isinstance(parent, TaskHost):
                raise ValueError("Tasks need a TaskHost object as parent, not {}".format(type(parent)))
            self._parent = parent

    # attributes

    @classmethod
    def getDefinedAttributes(cls):
        return cls.defined_attributes

    def getAttributes(self):
        """Dictionary of attributes used in this task

        :rtype: dict[str, pycultivator_lab.tasks.attributes.Attribute]
        """
        return self._attributes

    @property
    def attributes(self):
        return self.getAttributes()

    def setAttributes(self, arguments):
        """Will set the attributes to the values from the given dictionary

        :type arguments: dict[str, str | float | int | bool]
        """
        for name in self.attributes.keys():
            if name in arguments.keys():
                self.attributes[name].set(arguments[name])

    def getAttribute(self, name):
        """Get an attribute by name

        :rtype: None | pycultivator_lab.tasks.attributes.Attribute
        """
        result = None
        if not self.hasAttribute(name):
            self.getLog().warning("This task does not have an attribute with the name: '{}'".format(name))
        else:
            result = self.getAttributes()[name]
        return result

    def hasAttribute(self, name):
        return name in self.attributes.keys()

    def areAttributesFilled(self):
        """Checks if all required attributes are set [DEPRECATED, use isFilled()]

        i.e. there is at least one required attribute not set

        :return: Whether all required attributes are set
        :rtype: bool
        """
        return self.isFilled()

    def isFilled(self):
        """Checks whether all required attributes are set"""
        result = True
        for attribute in self.attributes.values():
            if attribute.is_required() and not attribute.isSet():
                result = False
                break
        return result

    # resources

    def getResources(self):
        """List of resources required by this task

        :rtype: dict[str, pycultivator_lab.tasks.resources.Resource]
        """
        return self._resources

    @property
    def resources(self):
        return self.getResources()

    def hasResource(self, name):
        """Checks if the resource exists in the host"""
        result = False
        if not self.hasParent():
            self.getLog().warning("This task has no parent, so access to resources is unavailable!")
        else:
            result = self.parent.hasResource(name)
        return result

    def getResource(self, name):
        result = None
        if not self.hasResource(name):
            self.getLog().warning("Resource not available, ignoring")
        else:
            # hasResource checks both parent presence and resource availability
            result = self.parent.getResource(name)
        return result

    # conditions

    def getConditions(self):
        """List all conditions set on the execution of this task

        :rtype: list[pycultivator_lab.server.condition.Condition]
        """
        return self._conditions

    @property
    def conditions(self):
        return self.getConditions()

    def addCondition(self, condition):
        result = False
        if isinstance(condition, Condition) and condition not in self.conditions:
            self.conditions.append(condition)
            result = True
        return result

    def isReady(self):
        """Return whether all conditions have been met, if none is specified return True

        Performs lazy checking: stops checking as soon one condition is False.

        :rtype: bool
        """
        result = True
        for condition in self._conditions:
            if isinstance(condition, Condition):
                result = result and condition.isSatisfied()
            if not result:
                break
        return result

    # performance

    def getTimeStart(self):
        """The time that this job has started"""
        return self._t_start

    def _setTimeStart(self, t):
        if t is not None and not isinstance(t, dt):
            raise ValueError("Invalid value for start time")
        self._t_start = t

    def getTimeFinished(self):
        return self._t_finish

    def _setTimeFinish(self, t):
        if t is not None and not isinstance(t, dt):
            raise ValueError("Invalid value for finish time")
        self._t_finish = t

    def isRunning(self):
        with self._lock:
            result = self._running is True
        return result

    def isSuccessful(self):
        """Whether the task execution was successful"""
        with self._lock:
            result = self._success is True
        return result

    def isFinished(self):
        """Whether the task has been executed

        :return: Whether the task has finished
        :rtype: bool
        """
        return self._finished.isSet()

    def waitFinished(self, timeout=None):
        """wait until finished (or timeout)

        :param timeout: Optional time-out on waiting, None for indefinite waiting
        :type timeout: None | float
        :return: True if task finished or False if timeout occured (if set)
        :rtype: bool
        """
        return self._finished.wait(timeout=timeout)

    def getResult(self):
        return self._result

    @property
    def result(self):
        return self.getResult()

    # actions

    def reset(self):
        with self._lock:
            self._setTimeStart(None)
            self._setTimeFinish(None)
            self._running = False
            self._success = False
            self._result = None
            self._finished.clear()

    def run(self):
        """Run the task

        :rtype: None | generator
        """
        with self._lock:
            self.reset()
            # set running
            self._running = True
            # register start time
            self._setTimeStart(dt.now())
            result = True
            try:
                # start prepare and execute in container
                result = self._prepare() and self._execute()
            except KeyboardInterrupt:
                import traceback
                self._result = traceback.format_exc()
                result = False
                # re-raise so host can exit
                raise
            except Exception:
                import traceback
                self._result = traceback.format_exc()
                result = False
            finally:
                try:
                    result = self._clean() and result
                except KeyboardInterrupt:
                    result = False
                    raise
                except Exception:
                    self.getLog().warning("Error occurred while cleaning task {}".format(
                        self.getIdentification(), exc_info=1
                    ))
                    result = False
                finally:
                    # register finish time
                    self._setTimeFinish(dt.now())
                    self._success = result
                    self._finished.set()
                    self._running = False
        return self.isFinished()

    def _prepare(self):
        result = True
        # check if all required attributes are set
        for name, attribute in self.attributes.items():
            attribute_result = not attribute.is_required() or attribute.isSet()
            if not attribute_result:
                self.getLog().error("Cannot start task: {} attribute is REQUIRED but NOT set!".format(name))
            result = attribute_result and result
        # check if all
        return result

    def _execute(self):
        raise NotImplementedError

    def _clean(self):
        return True

    def __str__(self):
        return self.to_string()

    @classmethod
    def to_string(cls):
        return "{name} Task: {arguments}".format(
            name=cls.name, arguments=cls.to_arguments_string()
        )

    @classmethod
    def to_arguments_string(cls):
        return " ".join([str(a) for a in cls.defined_attributes.values()])


class TaskException(pcException.PCException):
    """Exception raised by a task"""

    def __init__(self, msg):
        super(TaskException, self).__init__(msg=msg)


class RecurringTask(Task):
    """A task that will reschedule itself to be executed a given intervals"""

    def __init__(self, parent=None, resources=None, conditions=None, interval=None):
        """Initialize a RecurringTask

        Recurs every interval seconds (or timedelta distance) from now or schedule time from conditions.

        The recurring tasks works because it will update/create the TimeCondition to a new time based on configured
        interval and then resubmits itself to the server.

        If no TimeCondition is found in the conditions, it will create a new TimeCondition.

        :param interval: Interval at which this task should start. When a number, time in seconds.
        :type interval: int | float | datetime.timedelta
        """
        super(RecurringTask, self).__init__(parent=parent, resources=resources, conditions=conditions)
        from datetime import timedelta as td
        if isinstance(interval, (int, float)):
            interval = td(seconds=interval)
        if not isinstance(interval, td):
            raise ValueError("Invalid interval value, should be a integer, float or timedelta object")
        self._interval = interval

    def getTimeInterval(self):
        return self._interval

    def _execute(self):
        raise NotImplementedError

    def _clean(self):
        condition = None
        # filter time conditions
        conditions = filter(lambda c: isinstance(c, TimeCondition), self.conditions)
        # take highest time condition (i.e. the one that is the furthest away in time
        if len(conditions) > 0:
            conditions = sorted(conditions, key=lambda c: c.getTimeScheduled(), reverse=True)
            # take first
            condition = conditions[0]
        # no time condition found, take when task started as scheduled time
        if condition is None:
            condition = TimeCondition(t_scheduled=self.getTimeStart())
        condition.setTimeScheduled(condition.getTimeScheduled() + self.getTimeInterval())
        # resubmit to server
        self.parent.queueTask(self)
