"""Condition objects inform the user whether a certain condition is met without knowing about the condition itself"""

from pycultivator.core.objects import BaseObject

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = ["Condition", "CustomCondition", "TimeCondition"]


class Condition(BaseObject):
    """Base condition class: always returns True"""

    def isSatisfied(self, host=None):
        """Method called to determine whether the condition is met

        Should be overloaded by extending classes

        :type host: pycultivator_lab.tasks.host.TaskHost
        :return: Whether the condition is satisfied
        :rtype: bool
        """
        return True

    @classmethod
    def getName(cls):
        return cls.__name__

    @classmethod
    def getFullName(cls):
        return ".".join([cls.__module__, cls.getName()])


class CustomCondition(Condition):
    """Condition executing a pre-defined expression to tests whether it is met"""

    def __init__(self, condition):
        """Initialize a custom condition

        NOTE: CustomCondition allows you to provide a variable as condition,
        but remember that immutable variables (str, bool, int, etc.) are call-by-value
        and therefore the value passed to the condition will not change even if the variable "holding" it is.
        Use python's lambda to be sure you access the actual value.

        >>> v = False
        >>> c = CustomCondition(condition=v)
        >>> c.isSatisfied()
        False

        However since bool is immutable, changing v will not change the value in the condition:
        >>> v = True
        >>> c.is_satisfied
        False

        Instead use lambda:
        >>> v = False
        >>> c = CustomCondition(condition=lambda: v)
        >>> c.is_satisfied
        False

        >>> v = True
        >>> c.is_satisfied
        True

        :param condition: A callable expression that will evaluate to True
        :type condition: bool | () -> bool
        """
        super(CustomCondition, self).__init__()
        self._condition = condition

    def isSatisfied(self, host=None):
        result = self._condition
        if callable(self._condition):
            result = self._condition()
        return result is True


class TimeCondition(Condition):
    """Condition testing whether the current time is after the given time point"""

    def __init__(self, t_scheduled=None):
        super(TimeCondition, self).__init__()
        from datetime import datetime as dt
        if t_scheduled is None:
            t_scheduled = dt.now()
        if not isinstance(t_scheduled, dt):
            raise ValueError("Invalid value for scheduled time")
        self._t_scheduled = t_scheduled

    def getTimeScheduled(self):
        return self._t_scheduled

    @property
    def time_scheduled(self):
        return self.getTimeScheduled()

    def setTimeScheduled(self, t):
        from datetime import datetime as dt
        if t is None:
            t = dt.now()
        if not isinstance(t, dt):
            raise ValueError("Invalid value for scheduled time")
        self._t_scheduled = t

    @time_scheduled.setter
    def time_scheduled(self, t):
        self.setTimeScheduled(t)

    def isSatisfied(self, host=None):
        """Determine if current time is """
        from datetime import datetime as dt
        return dt.now() >= self.getTimeScheduled()


class TaskCondition(Condition):

    def __init__(self, task):
        """Create a condition on a task

        :param task: Task ID or Task with Identifier
        :type task: str | pycultivator_lab.tasks.tasks.Task
        """
        super(TaskCondition, self).__init__()
        from pycultivator_lab.tasks import Task
        if isinstance(task, Task):
            task = task.getIdentification()
        if task is None:
            raise ValueError("Task ID cannot be None!")
        self._task_id = task

    def isSatisfied(self, host=None):
        """Check whether the task has been executed.

        Checks with the given host whether the task has finished.
        NOTE: If the given task ID is not registered or if the finished tasks list was emptied, this condition will
        never satisfy!

        :param host: The host wanting to execute the task. Returns False if host is not a TaskHost object.
        :type host: pycultivator_lab.tasks.host.TaskHost | None
        :return: Whether the task has been executed (according to the host).
        :rtype: bool
        """
        result = False
        from pycultivator_lab.tasks.host import TaskHost
        if isinstance(host, TaskHost):
            result = host.isTaskFinished(self._task_id)
        return result


def load_conditions():
    return [
        TimeCondition,
        TaskCondition,
    ]
