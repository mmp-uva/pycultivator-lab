
from pycultivator.core import pcException
from pycultivator.core.objects.configurable import ConfigurableObject
from pycultivator_lab.core.resources import *
from tasks import *

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = [
    "TaskHost", "TaskHostException",
]


class TaskHost(ConfigurableObject):
    """A very light-weight host"""

    _namespace = "task.host"
    _default_settings = {
        "root.dir": None,
        "config.dir": None,
        "log.file.state": False,
        "log.file.dir": None,
        "data.dir": None,
    }

    def __init__(self, settings=None, **kwargs):
        super(TaskHost, self).__init__(settings=settings, **kwargs)
        # tasks waiting to be executed
        self._unfinished_tasks = {}
        self._finished_tasks = {}
        # adds id's to it, last = youngest
        self._finished_tasks_order = []
        # max number of finished tasks before cleaning
        self._finished_tasks_size = 1000
        # resource
        self._resources = {}
        """:type: dict[str, pycultivator_lab.core.resources.ReservableResource]"""
        # whether to actively clean resources
        self._clean_resources = True

    def run(self):
        """Runs the host and any task given to it"""
        while self.countWaitingTasks() > 0:
            self._manageTasks()
        # done
    # tasks

    def countWaitingTasks(self):
        return len(self._unfinished_tasks.keys())

    def countFinishedTasks(self):
        return len(self._finished_tasks)

    def clearFinishedTasks(self):
        self._finished_tasks = {}

    def queueTask(self, task):
        """Queues a task for execution

        :type task: pycultivator_lab.tasks.Task
        :return: Whether the task was queued successfully.
        :rtype: bool
        """
        result = False
        # check if Task ID was issued before by this server or externally
        if task.hasIdentification():
            task_id = task.getIdentification()
            if not self.isTaskIDAvailable(task_id):
                # potential collision, reject!
                task.setIdentification(None)
        else:
            self.requestTaskID(task)
        # we are certain task has valid ID, now register
        if task.hasIdentification():
            task_id = task.getIdentification()
            # register task to host
            self._registerTask(task)
            # add to pool
            self._unfinished_tasks[task_id] = task
            result = True
        return result

    def requestTaskID(self, task=None):
        """Request a task ID from the server"""
        import uuid
        # generate random ID for the task
        result = str(uuid.uuid4())
        # check availability to be sure, or re-issue.
        while not self.isTaskIDAvailable(result):
            result = str(uuid.uuid4())
        if isinstance(task, Task):
            task.setIdentification(result)
        return result

    def isTaskIDAvailable(self, task_id):
        return task_id is None or self._unfinished_tasks.get(task_id) is None

    def _registerTask(self, task):
        # register task with server
        task.setParent(self)
        # reserve resources used by task
        for resource in task.resources:
            self._registerTaskResource(task, resource)

    def _registerTaskResource(self, task, resource):
        result = False
        if self.hasResource(resource):
            self.getResource(resource).reserve()
            result = True
        else:
            # create resource load job and set task condition
            pass
        return result

    def _unregisterTask(self, task):
        # remove resource reservations of by task
        for resource in task.resources:
            if self.hasResource(resource):
                self.getResource(resource).finish()

    def _manageTasks(self):
        """Iterate over tasks and check whether they are ready to be executed"""
        new_tasks = {}
        executed_count = 0
        for task_id, task in self._unfinished_tasks.items():
            if task is not None and task.isReady():
                self._runTask(task)
                executed_count += 1
            else:
                new_tasks[task_id] = task
        self._unfinished_tasks = new_tasks
        return executed_count

    def _runTask(self, task):
        """Execute the task

        :type task: pycultivator_lab.tasks.Task
        """
        result = False
        results = []
        try:
            results = task.run()
            self._processTaskResults(results)
            result = True
        except (TaskHostException, TaskException):
            self.getLog().error("Error occurred while executing tasks", exc_info=1)
        # handle task finish
        self.finishTask(task)
        return result

    def finishTask(self, task):
        # finish it's resources
        self._unregisterTask(task)
        # store at finished tasks
        self._finished_tasks[task.getIdentification()] = task
        self._finished_tasks_order.append(task.getIdentification())

    def _processTaskResults(self, results):
        import collections
        if isinstance(results, collections.Iterable):
            for result in results:
                self._processTaskResult(result)
        else:
            self._processTaskResult(results)

    def _processTaskResult(self, result):
        if isinstance(result, Task):
            # queue this task
            self.queueTask(result)
        # TODO: Add automatic measurement reporting

    def hasTask(self, task_id):
        """Whether the task has been registered with the server

        :param task_id:
        :type task_id: str | pycultivator_lab.tasks.Task
        :return:
        :rtype:
        """
        if isinstance(task_id, Task):
            task_id = task_id.getIdentification()
        return self.isTaskRegistered(task_id)

    def isTaskRegistered(self, task_id):
        """Whether the task is registered with the server"""
        return self.isTaskWaiting(task_id) or self.isTaskFinished(task_id)

    def isTaskWaiting(self, task_id):
        """Whether the task with the given ID is waiting"""
        return task_id in self._unfinished_tasks.keys()

    def collectWaitingTask(self, task_id):
        """Return the finished task (if finished)

        :rtype: pycultivator_lab.tasks.Task
        """
        result = None
        if self.isTaskWaiting(task_id):
            result = self._unfinished_tasks[task_id]
        return result

    def isTaskFinished(self, task_id):
        """Return whether the task with the given ID is finished"""
        return task_id in self._finished_tasks.keys()

    def collectFinishedTask(self, task_id):
        """Return the finished task (if finished)

        :rtype: pycultivator_lab.tasks.Task
        """
        result = None
        if self.isTaskFinished(task_id):
            result = self._finished_tasks[task_id]
        return result

    def getFinishedTaskPoolSize(self):
        return self._finished_tasks_size

    def _manageFinishedTasks(self):
        """Removes tasks if more than finished task pool size number of tasks are kept"""
        self.getLog().info("Finished Tasks list ({}) > max size ({})".format(
            self.countFinishedTasks(), self.getFinishedTaskPoolSize()
        ))
        while self.countFinishedTasks() > self.getFinishedTaskPoolSize():
            task_id = self._finished_tasks_order.pop(0)   # first is oldest
            self._finished_tasks.pop(task_id)
            self.getLog().info(
                "Removed task from finished task... ID = {}".format(
                    task_id
                )
            )

    # resources

    def hasResource(self, name):
        """Determine if the server has a resource with the given name

        :return: Whether the resource is present
        :rtype: bool
        """
        return name in self._resources.keys()

    def getResource(self, name):
        """Retrieve a resource from the server

        NOTE: Returns a Resource object (wrapping the resource), not the resource itself.

        :param name: Name of the resource requested
        :type name: str
        :return:
        :rtype: pycultivator_lab.core.resources.ReservableResource
        """
        result = None
        if self.hasResource(name):
            result = self._resources[name]
        return result

    def addResource(self, name, resource=None):
        """Register a resource with the host

        :type name: str
        :type resource: None or pycultivator_lab.core.resources.ReservableResource
        :rtype: bool
        """
        result = True
        if self.hasResource(name):
            self.getLog().error("Already registered a resource with the name '{}', ignoring".format(name))
            result = False
        if resource is not None and not isinstance(resource, ReservableResource):
            self.getLog().error("Given resource is not a reservable resource, ignoring")
        if result:
            self._resources[name] = ReservableResource()
            result = True
        return result

    def removeResource(self, name):
        """Remove a resource from the host

        :type name: str
        :rtype: bool
        """
        result = False
        if not self.hasResource(name):
            self.getLog().warning("Already registered a resource with the name '{}', ignoring".format(name))
        else:
            if self.getResource(name).in_use:
                self.getLog().warning("Cannot remove resource '{}', being used".format(name))
            else:
                self._resources.pop(name)
            result = True
        return result

    def reserveResource(self, name):
        """Increase resource usage with one

        The number of reservations made for one particular resource informs the server about whether the resource is
        still needed. When the number of reservations reaches zero, the server may remove the resource.
        """
        if not self.hasResource(name):
            raise TaskHostException("No resource with name: {}".format(name))
        self.getResource(name).reserve()

    def isResourceReserved(self, name):
        return self.isResourceInUse(name)

    def isResourceInUse(self, name):
        if not self.hasResource(name):
            raise TaskHostException("No resource with name: {}".format(name))
        return self.getResource(name).in_use

    def finishResource(self, name):
        """Decrease resource usage with one

        The number of reservations made for one particular resource informs the server about whether the resource is
        still needed. When the number of reservations reaches zero, the server may remove the resource.
        """
        if not self.hasResource(name):
            raise TaskHostException("No resource with name: {}".format(name))
        self.getResource(name).finish()

    def countResources(self):
        return len(self._resources)

    def clearResources(self):
        """Remove all resources not being used

        :return: Number of resources removed
        :rtype: int
        """
        result = 0
        new_resources = {}
        for name in self._resources.keys():
            if self.isResourceInUse(name):
                new_resources[name] = self.getResource(name)
            else:
                result += 1
        self._resources = new_resources
        return result


class TaskHostException(pcException.PCException):
    """Exception raised by task hosts"""

    def __init__(self, msg):
        super(TaskHostException, self).__init__(msg=msg)

