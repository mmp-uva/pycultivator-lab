# coding=utf-8
"""Test cases for the attributes module in pycultivator_lab.tasks"""

from pycultivator_lab.tasks.attributes import *
from pycultivator.core.tests import UvaSubjectTestCase

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TestCondition(UvaSubjectTestCase):
    """Test the Condition class"""

    _subject_cls = Attribute
    _abstract = False

