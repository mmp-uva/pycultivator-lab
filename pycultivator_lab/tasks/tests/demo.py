"""Demonstration of how to execute tasks directly as script"""


from pycultivator_lab.tasks.script import TaskMetaScript
from pycultivator_lab.tasks.examples import HelloWorldTask

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


if __name__ == "__main__":
    # all you need to start a task using a script:
    script = TaskMetaScript(HelloWorldTask)
    script.run()
