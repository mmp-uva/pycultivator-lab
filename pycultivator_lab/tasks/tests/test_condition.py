# coding=utf-8
"""Test cases for the conditions module in pycultivator_lab.tasks"""

from pycultivator_lab.tasks.conditions import *
from pycultivator.core.tests import UvaSubjectTestCase

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TestCondition(UvaSubjectTestCase):
    """Test the Condition class"""

    _subject_cls = Condition
    _abstract = False

    def test_isSatisfied(self):
        self.assertTrue(self.getSubject().isSatisfied())


class TestCustomCondition(TestCondition):

    _subject_cls = CustomCondition

    def initSubject(self, condition=True):
        """

        :param condition:
        :type condition: bool | () -> bool
        :rtype: TestCustomCondition._subject_cls
        """
        return super(TestCustomCondition, self).initSubject(condition)

    def test_isSatisfied(self):
        s = self.initSubject(condition=True)
        self.assertTrue(s.isSatisfied())
        s = self.initSubject(condition=lambda: True)
        self.assertTrue(s.isSatisfied())
        s = self.initSubject(condition=lambda: False)
        self.assertFalse(s.isSatisfied())
        v = False
        s = self.initSubject(condition=lambda: v)
        self.assertFalse(s.isSatisfied())
        v = True
        self.assertTrue(s.isSatisfied())


class TestTimeCondition(TestCondition):

    _subject_cls = TimeCondition

    def test_isSatisfied(self):
        s = self.initSubject()  # initialize to current time
        self.assertTrue(s.isSatisfied())
        from datetime import datetime as dt, timedelta as td
        s = self.initSubject(t_condition=dt.now() + td(seconds=5))
        self.assertFalse(s.isSatisfied())
        import time
        time.sleep(5)
        self.assertTrue(s.isSatisfied())
