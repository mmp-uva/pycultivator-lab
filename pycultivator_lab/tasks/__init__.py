
from __future__ import print_function
from tasks import *
from attributes import *

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = [
    "Task", "TaskException",
    "Attribute", "ListAttribute", "AttributeException"
]
_task_cache = None


def load_tasks():
    """Load predefined tasks provided by pycultivator_lab"""
    return [

    ]


def find_tasks(entries=None, force=False):
    """Search for entry points defining tasks

    Caches into local var

    :param entries: Entry points to search
    :param force: Force method to ignore cache
    :return: Dictionary of tasks with namespaced name as key
    :rtype: dict[str, callable -> pycultivator_lab.tasks.tasks.Task]
    """
    global _task_cache
    if _task_cache is None or force:
        results = _find_tasks(entries=entries)
        _task_cache = results
    return _task_cache


def _find_tasks(entries=None):
    """Search for entry points defining pre-defined tasks"""
    results = {}
    # if no possible commands are given, search them
    if entries is None:
        entries = []
        from pkg_resources import iter_entry_points
        entry_points = iter_entry_points(group='pycultivator_lab.tasks', name=None)
        for entry_point in entry_points:
            entries.append(entry_point.load())
    if len(entries) == 0:
        entries = load_tasks()
    # now iterate over entries
    import types
    from pycultivator_lab.tasks import Task
    for entry in entries:
        # check if it is a command
        if isinstance(entry, (type, types.ClassType)) and issubclass(entry, Task):
            name = entry.getFullName()
            results[name] = entry
        # if a function expect it to return a list of commands
        elif isinstance(entry, types.FunctionType):
            results.update(find_tasks(entry()))
    return results
