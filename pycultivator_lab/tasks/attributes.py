"""Attributes for tasks"""

from pycultivator.core.objects import PCObject
from pycultivator.core import pcException
from pycultivator_lab.core.validators import *

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = [
    "Attribute", "AttributeException", "ListAttribute",
    "FilePathAttribute",
    "ResourceAttribute",
]


class Attribute(PCObject):

    def __init__(
            self, type_, default=None, is_required=True, name=None, aliases=None,
            description=None, validators=None, **kwargs
    ):
        """Initialize a Task Attribute

        - Inform task users about what parameters are available in this task
        - Help task developers to quickly define variables for in the task
        - Provide a way to translate tasks into small scripts with argparse

        """
        super(Attribute, self).__init__()
        self._name = None
        self.set_name(name)
        self._type = type_
        self._default = default
        self._value = None
        self._is_required = is_required is True
        # extra information
        if aliases is None:
            aliases = []
        if not isinstance(aliases, (tuple, list, set)):
            aliases = [aliases]
        self._aliases = set(aliases)
        self._description = description
        # an validator object to help the attribute validate the values given to it
        if validators is None:
            validators = []
        if not isinstance(validators, (list, tuple)):
            validators = [validators]
        self._validators = list(validators)
        self.reset()
        # ignore kwargs

    def get_name(self):
        """Return name of attribute

        :return:
        :rtype: str
        """
        return self._name

    @property
    def name(self):
        return self.get_name()

    def has_name(self):
        return self.get_name() is not None

    def set_name(self, v):
        self._name = v

    def get_type(self):
        return self._type

    @property
    def type(self):
        return self.get_type()

    def get_default(self):
        return self._default

    @property
    def default(self):
        return self.get_default()

    def get(self):
        return self._value

    @property
    def value(self):
        return self.get()

    @value.setter
    def value(self, v):
        self.set(v)

    def is_set(self):
        return self.value is not None

    def is_required(self):
        return self._is_required

    def set_required(self, state):
        self._is_required = state is True

    def get_aliases(self):
        """Extra name arguments of script

        :rtype: set[str]
        """
        return self._aliases

    @property
    def aliases(self):
        return self.get_aliases()

    def add_alias(self, name):
        if name == self.get_name():
            raise ValueError("Alias cannot be the same as the name")
        return self._aliases.add(name)

    def get_description(self):
        return self._description

    @property
    def description(self):
        return self.get_description()

    def get_validators(self):
        """ List of all defined validators

        :return:
        :rtype: list[pycultivator_lab.core.validators.BaseValidator]
        """
        return self._validators

    @property
    def validators(self):
        return self.get_validators()

    # actions

    def reset(self):
        """Reset the value of the attribute to the default value"""
        self.set(self.get_default())

    def set(self, v):
        result = False
        if self.validate(v):
            self._value = v
            result = True
        return result

    def isSet(self):
        """Checks if the value of the attribute is other than None"""
        return self.get() is not None

    def validate(self, value):
        result = True
        for validator in self.validators:
            result = validator.test(value)
            if not result:
                self.getLog().warning(
                    "Value for {} attribute is not valid: {}, ignoring".format(
                        self.get_name(), value
                    )
                )
                break
        return result

    def copy(self, memo=None):
        """Create a deepcopy of the object

        :param memo: Extra memo for deepcopy
        :return: A deep copy of this object
        :rtype: pycultivator_lab.tasks.attributes.Attribute
        """
        import copy
        result = type(self)(
            self._type, name=self._name,
            default=copy.deepcopy(self.default, memo),
            aliases=copy.deepcopy(self.aliases, memo),
            description=copy.deepcopy(self.description, memo),
            validators=copy.deepcopy(self.validators, memo)
        )
        result.set(copy.deepcopy(self._value, memo))
        return result

    def __str__(self):
        return self.short_help()

    def to_dict(self):
        """Serialize the attribute to a dictionary

        :rtype: dict[str, str | int | float]
        """
        attribute_type = self.get_type()
        if isinstance(attribute_type, type):
            attribute_type = attribute_type.__name__
        validators = []
        for validator in self.validators:
            validators.append(validator.to_dict())
        return {
            'name': self.get_name(),
            'type': attribute_type,
            'value': self.value,
            'description': self.description,
            'validators': validators
        }

    def from_dict(self):
        pass

    def short_help(self):
        return "{required}<{type}> {name} (default={default}){value}".format(
            required="required " if self.is_required() else "",
            type=self.get_type().__name__,
            name=self.name, default=self.default,
            value=" {}".format(self.value) if self.is_set() else ""
        )

    def full_help(self):
        return "- {required} <{type}> {name}\n\tdefault={default}\n\tvalue={value}\n\n\t{help}".format(
            required="required" if self.is_required() else "",
            type=self.get_type().__name__,
            name=self.name, default=self.default,
            value=" {}".format(self.value) if self.is_set() else "",
            help=self.get_description()
        )


class AttributeException(pcException.PCException):
    """Exception raised by script attributes"""

    def __init__(self, msg):
        super(AttributeException, self).__init__(msg=msg)


class ListAttribute(Attribute):
    """A Task Attribute that is a list"""

    def __init__(self, type_, default=None, all_values=None, **kwargs):
        """Initialze a List Attribute for MetaTasks

        :param all_values: All possible values for the list, can be used by scripts
        :type all_values: None | list
        """
        if not isinstance(default, (tuple, list)):
            default = [default]
        super(ListAttribute, self).__init__(type_=type_, default=list(default), **kwargs)
        if all_values is None:
            all_values = []
        self._all_values = all_values

    def get_all_values(self):
        return self._all_values

    def set_all_values(self, v):
        self._all_values = v

    def copy(self, memo=None):
        result = super(ListAttribute, self).copy(memo=memo)
        """:type: ListAttribute"""
        result.set_all_values(self.get_all_values()[:])
        return result

    def to_dict(self):
        result = super(ListAttribute, self).to_dict()
        result["all_values"] = self.get_all_values()
        return result


class FilePathAttribute(Attribute):
    """Attribute containing a file location"""

    def __init__(self, type_, default=None, validators=None, **kwargs):
        # check if validators argument is given
        if validators is None:
            validators = []
        if PathValidator not in validators:
            validators.append(PathValidator(require_exist=True))
        super(FilePathAttribute, self).__init__(type_, default=default, validators=validators, **kwargs)


class ResourceAttribute(Attribute):
    """Attribute signalling to the task that it should try to load it into a Resource"""

    def __init__(self, type_, default=None, validators=None, **kwargs):
        super(ResourceAttribute, self).__init__(type_, default=default, validators=None, **kwargs)


class DeviceResourceAttribute(ResourceAttribute):

    def __init__(self):
        super(DeviceResourceAttribute, self).__init__()
