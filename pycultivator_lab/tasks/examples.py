
from pycultivator_lab.tasks import Task, Attribute


class HelloWorldTask(Task):

    _name = "hello_world"
    username = Attribute(str, default="anonymous")

    def _execute(self):
        msg = "{} says: Hello World!".format(self.username)
        print(msg)
        self._result = msg
        return True


class TestExceptionTask(Task):

    _name = "test_exception"

    def _execute(self):
        raise ValueError


def load_tasks():
    """A list of available examples"""
    return [
        HelloWorldTask,
        TestExceptionTask
    ]
