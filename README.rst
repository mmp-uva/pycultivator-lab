PyCultivator Laboratory Utilities
=================================

Version: August 12, 2017

----

The pyCultivator Laboratory Utilities provide classes to enable interaction with the pyCultivator devices:

- Data storage; supports the import and export of measurements to different data storage facilities.
- Server; provides one central place for interaction with devices
- Relations; support the definition of relations between instruments and devices
- Scripts; facilitate the development of front-end applications

Data Storage
------------

* Implement a standard data interface for measurements
* Help users to develop data storage bindings to their favorite data storage facility.
* Provide already implemented bindings to some data storage facilities.

Server
------

For every pyCultivator device a large number of objects need to be loaded and created by the computer. Also, it may
be necessary to share usage of resources between two or more tasks executed in parallel. The server provides a central
place to manage devices (resources) and their access.

Resources are small objects wrapped around objects that need to be shared between different threads. Their job is to
allow access to only one thread at the time, and to keep track of usage (by providing a reservation system) so that
resources can be removed when they are not used for some time.

A server will spawn one or more workers, separate threads to run the tasks in, allowing for almost parallel execution
 of tasks (look into GIL with python).

Tasks make it possible to perform actions on the pycultivator devices via the server. A task informs the server about
 any condition on execution (for example scheduling on time) and which resources should be kept around (by reserving
 them).

Conditions make it possible to set constraints on when a task is allowed to run. For example a time constrain,
specifying after what time point a task is allowed to run. But this can also be scripted conditions, creating
dependencies on other tasks. NOTE: once a task is queued it should run at one point!
